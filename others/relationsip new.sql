ALTER TABLE `merchant_credit`
	ADD CONSTRAINT `fk_merchant_credit_to_admin`
	FOREIGN KEY(`admin_id`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `merchant_credit`
	ADD CONSTRAINT `fk_merchant_credit_to_merchant`
	FOREIGN KEY(`merchant_id`)
	REFERENCES `merchant` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;
	
ALTER TABLE `merchant_credit`
	ADD CONSTRAINT `fk_merchant_credit_to_receipt`
	FOREIGN KEY(`receipt_id`)
	REFERENCES `receipt` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `member`
	ADD CONSTRAINT `fk_member_to_admin`
	FOREIGN KEY(`admin_id`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `referral`
	ADD CONSTRAINT `fk_member_id_to_member`
	FOREIGN KEY(`member_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `referral`
	ADD CONSTRAINT `fk_referral_code_to_member`
	FOREIGN KEY(`referral_code`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `referral`
	ADD CONSTRAINT `fk_top_referrer_to_member`
	FOREIGN KEY(`top_referrer`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `merchant`
	ADD CONSTRAINT `fk_merchant_to_member`
	FOREIGN KEY(`member_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;
	
ALTER TABLE `ngo`
	ADD CONSTRAINT `fk_ngo_to_member`
	FOREIGN KEY(`member_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `cashback_rate`
	ADD CONSTRAINT `fk_cashbackrate_to_merchant`
	FOREIGN KEY(`merchant_id`)
	REFERENCES `merchant` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `receipt`
	ADD CONSTRAINT `fk_receipt_to_merchant`
	FOREIGN KEY(`merchant_id`)
	REFERENCES `merchant` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;
	
ALTER TABLE `merchant`
	ADD CONSTRAINT `fk_merchant_connector_to_member`
	FOREIGN KEY(`connect_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `merchant`
	ADD CONSTRAINT `fk_merchant_to_admin`
	FOREIGN KEY(`admin_id`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;
	
ALTER TABLE `ngo`
	ADD CONSTRAINT `fk_ngo_to_admin`
	FOREIGN KEY(`admin_id`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `cashback`
	ADD CONSTRAINT `fk_cashback_to_member`
	FOREIGN KEY(`member_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;
	
ALTER TABLE `cashback`
	ADD CONSTRAINT `fk_cashback_downline_referral_code_to_member`
	FOREIGN KEY(`downline_referral_code`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `receipt`
	ADD CONSTRAINT `fk_receipt_to_member`
	FOREIGN KEY(`member_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `withdrawal`
	ADD CONSTRAINT `fk_withdrawal_to_member`
	FOREIGN KEY(`member_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `donation`
	ADD CONSTRAINT `fk_donation_to_member`
	FOREIGN KEY(`member_id`)
	REFERENCES `member` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `cashback`
	ADD CONSTRAINT `fk_receipt_to_cashback`
	FOREIGN KEY(`receipt_id`)
	REFERENCES `receipt` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `receipt`
	ADD CONSTRAINT `fk_receipt_to_admin1`
	FOREIGN KEY(`admin_1`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `receipt`
	ADD CONSTRAINT `fk_receipt_to_admin2`
	FOREIGN KEY(`admin_2`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `withdrawal`
	ADD CONSTRAINT `fk_withdrawal_to_admin`
	FOREIGN KEY(`admin_id`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `donation`
	ADD CONSTRAINT `fk_donation_to_admin`
	FOREIGN KEY(`admin_id`)
	REFERENCES `admin` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `donation`
	ADD CONSTRAINT `fk_donation_to_ngo`
	FOREIGN KEY(`ngo_id`)
	REFERENCES `ngo` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `cashier`
	ADD CONSTRAINT `fk_cashier_to_merchant`
	FOREIGN KEY(`merchant_id`)
	REFERENCES `merchant` (`id`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `receipt`
	ADD CONSTRAINT `fk_receipt_to_cashier`
	FOREIGN KEY(`cashier_id`)
	REFERENCES `cashier` (`username`)
	ON DELETE RESTRICT ON UPDATE CASCADE;

-- ALTER TABLE ngo AUTO_INCREMENT = 20000;

-- list all constraints
-- select *
-- from information_schema.table_constraints
-- where constraint_schema = 'alexmah_ist'
--
-- list all foreign key constraints
-- select *
-- from information_schema.referential_constraints
-- where constraint_schema = 'alexmah_ist'

--get all the amounts that a specific merchant need to pay to ist
--SELECT * 
--FROM  cashback 
--INNER JOIN receipt ON receipt.id = cashback.receipt_id
--WHERE receipt.merchant_id = 1009;
