<script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=[];
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=[]; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
// $(document).ready(function() {
//   ('.btn-default').click(function() {
//       $('.modal-body').empty();
//       var title = $(this).parent('a').attr("title");
//       $('.modal-title').html(title);
//       $($(this).parents('div').html()).appendTo('.modal-body');
//       $('#myModal').modal({
//           show: true
//       });
//     });
// });

</script>
<body onLoad="MM_preloadImages('images/fb_footer2.png','images/yt_footer2.png')"><div class="clear"></div>
<footer class="indexfooter">

    <div class="coverup">
        <!-- <div class="footer-inner"> -->
 <!--<div class="col-md-4 text-left text-foot">-->
   <div class="col-md-5 col-xs-12 col-sm-5 pl5">
   <div class="stay">
   <p class="stayp">Stay Connected/ Keep Update</p>
   <form id="indexForm" role="form" method="post" action="">
    <div class="textarea1">
    <div class="input-group whitebox1">
      <input type="email" name="email" class="form-control textarea1 signup1" placeholder="Sign up now" >
      <span class="input-group-btn span1">
        <button class="btn btn-default button1"  type="submit" name="submit"><span class="glyphicon glyphicon-menu-right icon1" aria-hidden="true"></span></button>
      </span>
    </div><!-- /input-group -->
  </div><!-- /.textarea1 -->
  <!-- <textarea class="textarea1" placeholder="Sign up now"></textarea>-->
</form>
   </div>
   <div class="social">
   <p class="follow">Follow US</p>
<a href="https://www.facebook.com/iSpendtribute/" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5','','images/fb_footer2.png',1)"  class="sociala"><img src="images/fb_footer.png" alt="" width="89" height="89" id="Image5"  class="socialpng">   </a>

<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5bb','','images/insta_footer2.png',0)" class="sociala"><img src="images/insta_footer.png" alt="" width="89" height="89" id="Image5bb" class="socialpng"></a>

<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image5yt','','images/yt_footer2.png',1)" class="sociala"><img src="images/yt_footer.png" alt="" width="89" height="89" id="Image5yt" class="socialpng"></a>
   </div>
   </div>
    <div class="col-md-3 col-xs-12 col-sm-3">
    <div class="div1">
    <p class="titlep">LEARN MORE</p>
    <a href="#" class="contentp">How It Works</a><br>
    <a href="#"  class="contentp">Success Stories</a><br>
    <a href="#"  class="contentp">About iST</a>
    </div>
    <div class="div2">
    <p class="titlep">CUSTOMER SERVICE</p>
    <a href="#"  class="contentp">Help & FAQs</a><br>
    <a href="#"  class="contentp">News & Press</a><br>
    <a href="#"  class="contentp">Terms of Service</a><br>
    <a href="#"  class="contentp">Privacy Policy</a><br>
    <a href="#"  class="contentp">Site Map</a>
    </div>
    </div>
     <div class="col-md-4 col-xs-12 col-sm-4 pr5">
     <img src="images/mail_head.png" class="logo"><br>
     <p class="titlep2">Compassionate Crowdfunding</p>
     <p class="normaltext textfont">YouCaring is dedicated to compassionate crowdfunding, providing free and easy online fundraising and support for humanitarian causes.</p>
     </div>
    </div>
    <!-- </div> -->
</footer>
<style>
    .textfont{
        font-family: Arial !important;
    }
.clear{
	clear:both;}
.coverup{
	padding-top:40px;
	background-color:#58595b;
	min-height:300px;
	color:white;}
.pl5{
	padding-left:5%;}
.pr5{
	padding-right:5%;}
.logo{
	width:60%;}
.titlep2{
	padding-top:25px;
	padding-bottom:10px;
	font-size:22px;}
.contentp{
	color:#9f76b4;
    font-family: Arial !important;
}
.contentp:hover{
	color:#dda3fb;
	text-decoration:none;}
.titlep{
	margin-bottom:0;
	font-size:16px;}
.div2{
	margin-top:25px;}
.stayp{
	font-size:19px;}
.follow{
	font-size:17px;}
.textarea1{
	width:69%;
	border-radius:10px;

	}
.textarea1::-webkit-input-placeholder{
	padding-top:10px;
	padding-left:10px;
	color:#a2a2a2;}
.textarea1::-moz-placeholder{padding-top:10px;
padding-left:10px;color:#a2a2a2;}
.textarea1::-ms-input-placeholder{padding-top:10px;
padding-left:10px;color:#a2a2a2;}
.signup1{
	border-top-left-radius:10px !Important;
	border-bottom-left-radius:10px !Important;
	border-right:0px !important;
	min-height:40px !important;


	}
.button1{
	min-height:40px;
	border-left:0px;
	border-top-right-radius:10px !Important;
	border-bottom-right-radius:10px !Important;}
.icon1{color:#a2a2a2 !important;
}
.icon1:hover{
	background-color:white !important;}
span.icon1:hover{
	background-color:white !important;}
.button1:hover{background-color:white !important;}
.icon1:active{
	background-color:white !important;}
span.icon1:active{
	background-color:white !important;}
.button1:active{background-color:white !important;}
.social{
	margin-top:25px;

	}
img.socialpng{
	width:10%;
	margin-right:5px;
	height:auto;}
body{
	margin-bottom:0 !important;}
@media all and (max-width: 1025px){
.textarea1{
	width:74%;}

	}
@media all and (max-width: 953px){
.textarea1{
	width:79%;}

	}
@media all and (max-width: 841px){
.textarea1{
	width:90%;}

	}
@media all and (max-width: 767px){

.pl5{
	text-align:center;
	padding-left:0% !important;
	padding-right:0% !important;}
.textarea1{
	text-align:center;}
.whitebox1{
	margin-left:25%;
	width:60%;

	}
.div1{
	text-align:center;
	margin-top:3%;}
.follow{
	font-size:19px;}
.contentp{
	font-size:16px;}
.div2{
text-align:center;
	}
.coverup{
	min-height:800px;}
.pr5{
	padding:0 22%;
	text-align:center;
	margin-top:5%;

	}
.stay{
	text-align:center;}
.social{
	text-align:center;}
.textarea1::-webkit-input-placeholder{

	padding-left:15px;
	}
.textarea1::-moz-placeholder{
padding-left:15px;}
.textarea1::-ms-input-placeholder{
padding-left:15px;}

}
/*.modal{
  margin-top: 60px;
}
.charity {
  color: #fff;
  background-color: #f291bb;
}*/
</style>
