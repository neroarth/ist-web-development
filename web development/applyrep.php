<?php
session_start();
require_once "php-files/usefulFunction.php";
if (isLogin()) {

} else {
    header("Location: login");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <title>IST-Profile</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="main-style.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!-- [if lt IE 9]> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="assets/js/ie8-responsive-file-warning.js"></script>
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale = 1.0,
maximum-scale=1.0, user-scalable=no"/>
</head>
<style>
    .footer {
        border-top: 1px solid #d3d3d3;
    }

    .modal-header {
        background: #9c77b4;
        color: #fff;
    }

    select {
        border: 0;
        outline: 0;
        font-family: 'MyWebFont', 'vagrounded-regular', sans-serif;
    }

    .select {
        font-family: 'MyWebFont', 'vagrounded-regular', sans-serif;
    }
</style>

<body>
<?php include 'navProfile.php'; ?>
<div class="container overwrite-container">
    <div class="row">
        <h1 class="rep-h1">Details of apply as Business Rep</h1>
        <p></p>
        <form action="profile" class="form-horizontal" method="post" enctype="multipart/form-data" id="applyRepForm">
            <a class="apply-it" id="ot1">
                <input class="apply-div" id="applyRep" name="applyRep" type="submit" value="Apply">
            </a>
        </form>

        <a class="cancel-it" id="ot2">
            <div class="cancel-div">Cancel</div>
        </a>
    </div>
</div>
<!--Began another section -->
<script>
    document.getElementById("ot2").onclick = function () {

        window.location = "profile.php";
    };
</script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="jquery-2.2.0.min.js"></script>
<?php include 'foot.php'; ?>
</body>

</html>
