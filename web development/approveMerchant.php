<?php
	session_start();
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		ob_start();
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		$complete = true;
		
		$merchantId = $_POST['merchantList'];
		
		$remark = rewrite($_POST["remark"]);
		$remark = stripSpaces($remark);
		if(strlen($remark) > 300){
			$remarkError = "Remark should not be more than 300 characters";
		    $complete = false;
		}
		
		$sql = "UPDATE merchant SET approval_status = ?, remark = ?, admin_id = ? WHERE id = ?";
		
		if (isset($_POST['approve'])) {
			$approvalStatus = "approved";
		}else if(isset($_POST['reject'])) {
			$approvalStatus = "rejected";
		}
		else{
			header("Location: adminMerchantDetails");
			exit();
		}
		
		if($complete){
			$stmt = $conn->prepare($sql);
			$stmt->bind_param("sssi", $approvalStatus, $remark, $_SESSION['admin-id'], $merchantId);
	
			$stmt->execute();
			$stmt->close();
			header("Location: adminMerchantDetails");
		}else{
			header("Location: adminMerchantDetails?remarkError=$remarkError&remark=$remark");
		}
		
		
		
		$conn->close();
		ob_end_clean();
	}
?>