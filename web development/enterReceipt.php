<?php
session_start();
ob_start();
require_once "php-files/usefulFunction.php";
require_once "php-files/conDb.php";
if (!isCashierLogin()) {
    header("Location: login.php");
    $conn->close();
    exit();
}

if (isset($_GET["msg"])) {
    if (strcmp($_GET['msg'], "success") == 0) {
        $msg = "Receipt's details recorded successfully!";
    } else if (strcmp($_GET['msg'], "fail") == 0) {
        $msg = "Sorry, failed to record the receipt\'s details. Please try again!";
    } else if (strcmp($_GET['msg'], "not-exist") == 0) {
        $msg = "Sorry, no such member exist!";
    } else if (strcmp($_GET['msg'], "wrong-pin") == 0) {
        $msg = "Incorrect cashier pin number!";
    } else if (strcmp($_GET['msg'], "wrong-member-pin") == 0) {
        $msg = "Incorrect member pin number!";
    } else if (strcmp($_GET['msg'], "success") == 0) {
        $msg = "Receipt saved successfully!";
    }
}

ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link href="images/favicon.png" rel="icon" type="image/png"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">

    <title>Enter Receipt</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php include 'nav.php' ?>
<body class="body-color">
<div class="minpink">
    <div class="container cancelpaddingtop">
        <h1 class="paddingtop25 p15" style="color:white;">Enter Receipt Details</h1>
        <div class="row">
            <form action="enterReceiptConfirmation" class="form-horizontal" method="post" enctype="multipart/form-data"
                  id="receiptForm">
                <div class="col-md-9 nop">
                    <div class="form-group">
                        <div class="col-md-3">
                            <input type="number" step="0.01" min="0"
                                   oninvalid="setCustomValidity('Please do not enter any amount less than 0');"
                                   class="form-control form-controls text-line line1 transparent01" oninput="setCustomValidity('');"
                                   id="amount-spent" name="amount-spent" placeholder="Amount Spent"
                                   value="<?php echo $_GET["amount-spent"]; ?>" required>
                            <span class="error"><?php echo $_GET["amount-spent-error"]; ?></span>
                        </div>
                    </div>
                    <div class="form-group c1">
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-controls text-line transparent01" id="receipt-number" style="color:white !important;"
                                   name="receipt-number" placeholder="Enter the receipt's number"
                                   value="<?php echo $_GET["receipt-number"]; ?>">
                            <span class="error"><?php echo $_GET["receipt-number-error"]; ?></span>
                        </div>
                    </div>

                    <div class="form-group c1">
                        <div class="col-sm-4">
                            <input type="text" class="form-control form-controls text-line transparent01" id="receipt-remark"
                                   name="receipt-remark" placeholder="Enter the receipt's remark"
                                   value="<?php echo $_GET["receipt-remark"]; ?>">
                            <span class="error"><?php echo $_GET["receipt-remark-error"]; ?></span>
                        </div>
                    </div>

                    <div class="form-group c1">
                        <div class="col-sm-4">
                            <span class="form-controls" id="basic-addon1">+60</span>
                            <input type="text" class="form-controls text-line transparent01" id="member-phone-no"
                    style=" color:white !important;"                name="member-phone-no"
                                   placeholder="Enter Member's Phone Number"
                                   value="<?php echo $_GET["member-phone-no"]; ?>" required>
                            <span class="error"><?php echo $_GET["member-phone-no-error"]; ?></span>
                        </div>
                    </div>

                    <div class="form-group c1">
                        <div class="col-sm-4">
                            <input type="password"
                               style=" color:white !important;"     oninvalid="setCustomValidity('Please do not enter a valid 6-digit pin number');"
                                   class="form-control form-controls text-line line1 transparent01" oninput="setCustomValidity('');"
                                   id="member-pin-no" name="member-pin-no" placeholder="Enter Member Pin Number">
                        </div>
                    </div>

                    <div class="form-group c1">
                        <div class="col-sm-4">
                            <input type="checkbox" id="pay-with-credit" name="pay-with-credit" value="pay-with-credit" style=" vertical-align:top !important;    margin-right: 5px;"><span style="color:white;">Pay with credit</span>
                            <span class="error"><?php echo $_GET["credit-error"]; ?></span>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-default btn-min-register">Confirm</button>
                    <div><span class="error"><?php echo $msg; ?></span></div>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.container -->
<div class="block"></div>
<?php include 'foot.php' ?>
<!--The progress bar section-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
<style>
input{
	color:white !important;}
autofill{color:white !important;
}
</style>
</body>
</html>
