<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (isset($_POST['skip'])) {
			ob_start();
			header("Location: ngoRegistrationPart3");
			ob_end_clean();
			exit();
		}
		
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		for($i=0;$i<count($_FILES["uploadNgoSsn"]["name"]);$i++){
			$ok = true;

			$imagePath = "uploads/ngos/".$_SESSION["ngo-id"]."/ssn/";
		
			$fileName = basename($_FILES["uploadNgoSsn"]["name"][$i]);
			
			//$fullPath = $imagePath . $fileName;
			$fullPath = $imagePath . "ssn." . pathinfo($fileName,PATHINFO_EXTENSION);
			
			$fileType = pathinfo($fullPath,PATHINFO_EXTENSION);
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])){
				$check = getimagesize($_FILES["uploadNgoSsn"]["tmp_name"][$i]);
				if($check !== false){
					$ok = true;
				} else {
					$msg = " The chosen file is not an image!\n";
					//header("Location: index?&msg=$msg");
					$ok = false;
				}
			}
			
			if($fileType != "jpg" && $fileType != "jpeg" && $fileType != "png"
			&& $fileType != "JPG" && $fileType != "JPEG" && $fileType != "PNG"
			&& $fileType != "gif" && $fileType != "GIF"){
					$msg = "Only jpg, jpeg, gif and png file types are allowed\n";
					//header("Location: index?&msg=$msg");
					$ok = false;
			}
			
			//10mb = 10 * 1024 * 1024 bytes
			if($_FILES["uploadNgoSsn"]["size"][$i] > 10485760){
					$msg = " Uploaded picture cannot exceed 10mb\n";
					//header("Location: index?&msg=$msg");
					$ok = false;
			}
	
			if($ok == false){
				//$msg = $msg . "Sorry some error occured and your file was not uploaded.";
				
				//header("Location: index?&msg=$msg");
			}
			else{
				if(!file_exists("$imagePath")){
					mkdir("$imagePath",0755,true);
				}
					
				if(file_exists($fullPath)){
					if(unlink($fullPath)==1){
						$replaced = true;
					}
				}
				if(move_uploaded_file($_FILES["uploadNgoSsn"]["tmp_name"][$i], $fullPath)){
					//if($replaced == false){
					//}
					//else{
					//}
					$stmt = $conn->prepare("UPDATE ngo SET ssn_link = ? WHERE id = ?");
					$stmt->bind_param("si", $fullPath, $_SESSION["ngo-id"]);
		
					$stmt->execute();
					$stmt->close();
					
					$msg = "Your ngo ssn has been uploaded successfully!";
					header("Location: ngoRegistrationPart3");
				}
				else{
						$msg = "Sorry, there was an error uploading your file.";
				}
			}
			echo "<div>$msg</div>";
		}
		$conn->close();
	}
?>