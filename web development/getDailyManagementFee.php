<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";

        $day = rewrite($_POST['day']);
        $month = rewrite($_POST['month']);
        $year = rewrite($_POST['year']);
        if(isAdminLogin()){
            $merchantId = rewrite($_POST['merchantId']);
        }else if(isMerchantLogin()){
            $merchantId = $_SESSION["merchant-id"];
        }else{
            $merchantId = 0;
        }

        $rows = getDailyManagementFee($conn, $merchantId, $day, $month, $year);

        if($rows == null){
            //duno why it really returns null but not going inside here also
            echo 'No fee is needed for today';
        }else{
            $tableData = "";
            foreach ($rows as &$value) {
                $tableData .= "RM" . $value['management-fee-amount'];
                $tempData = $value['management-fee-amount'];
            }
            if(strcmp($tempData, "") == 0){
                echo 'No fee is needed for today';
            }else{
                echo $tableData;
            }
        }

		$conn->close();
	}
?>