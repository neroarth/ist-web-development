<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";

		$perpage = $_POST['perpage'];

        if(isset($_POST["currentPage"])){
            $page = intval($_POST["currentPage"]);
        }
        else {
            $page = 1;
        }

        //last row of record
        $end = $perpage * $page;
        //first row of record
        $offset = $end - $perpage;

        $where1 = "receipt.image_url";
        $whereId = 0;

        $where2 = "";
        $sortBy = "ASC";

        if(strcmp($_POST['sortBy'], "Newest") == 0){
            $sortBy = "DESC";
        }else if(strcmp($_POST['sortBy'], "Oldest") == 0){
            $sortBy = "ASC";
        }

        if(strcmp($_POST['where1'], "merchant") == 0){
            $where1 = "receipt.merchant_id";
            if(isAdminLogin()){
                $whereId = $_POST['merchantId'];
            }else if(isMerchantLogin()){
                $whereId = $_SESSION["merchant-id"];
            }else{
                $whereId = 0;
            }
        }else if(strcmp($_POST['where1'], "member") == 0){
            $where1 = "receipt.member_id";
            if(isLogin()){
                $whereId = $_SESSION["id"];
            }
        }else if(strcmp($_POST['where1'],"full") == 0 && rewrite($_POST['receiptId']) != null){
            $where1 = "receipt.id";
            $whereId = rewrite($_POST['receiptId']);
            $perpage = 1;
            $offset = 0;
        }

        $day = rewrite($_POST['day']);
        $month = rewrite($_POST['month']);
        $year = rewrite($_POST['year']);

        $rows = getCashbackReport($conn, $whereId, $offset, $perpage, $where1, $where2, $sortBy, $day, $month, $year);

        if($rows == null){
            echo '<tr>
                       <td>Sorry, no records found</td>
                  </tr>';
        }else{
            $tableData = "";
            if(strcmp($_POST['where1'], "merchant") == 0){
                foreach ($rows as &$value) {
                    $tableData .= '<tr onclick="showReceiptDetails(' . $value['id'] . ')">';
                    $tableData .= '<td>' . $value['timestamp'] . '</td>';
                    $tableData .= '<td> Cashback for ' . $value['member-name'] . '</td>';
                    $tableData .= '<td> Created by ' . $value['cashier-name'] . '</td>';
                    $tableData .= '<td>' . $value['cashback-amount'] . '</td>';
                    $tableData .= '<td>' . $value['merchant-approval-status'] . '</td>';
                    if(!isAdminLogin() && !strcmp($value['merchant-approval-status'], "approved") == 0){
                        $tableData .= '<td><input type="checkbox" name="approveCB[]"  class="checkbox-overwrite"  value="' . $value['id'] . '">Approve</td>';
                    }

                    $tableData .= '</tr>';
                }
            }else if(strcmp($_POST['where1'], "member") == 0){
                foreach ($rows as &$value) {
                    $tableData .= '<tr onclick="showReceiptDetails(' . $value['id'] . ')">';
                    $tableData .= '<td>' . $value['timestamp'] . '</td>';
                    $tableData .= '<td> Cashback for ' . $value['shop-name'] . '</td>';
                    $tableData .= '<td>' . $value['cashback-amount'] . '</td>';
                    $tableData .= '<td>' . $value['merchant-approval-status'] . '</td>';
                    $tableData .= '</tr>';
                }
            }else if(strcmp($_POST['where1'], "full") == 0){
                foreach ($rows as &$value) {
                    $tableData .= '<p>Date & Time: ' . $value['timestamp'] . '</p>';
                    $tableData .= '<p>Member Name: ' . $value['member-name'] . '</p>';
                    $tableData .= '<p>Shop Name: ' . $value['shop-name'] . '</p>';
                    $tableData .= '<p>Receipt Number: ' . $value['receipt-number'] . '</p>';
                    $tableData .= '<p>Receipt Remark: ' . $value['receipt-remark'] . '</p>';
                    $tableData .= '<p>Amount Spent: ' . $value['amount-spent'] . '</p>';
                    $tableData .= '<p>Cashback Amount: ' . $value['cashback-amount'] . '</p>';
                    if($value['pay-with-credit'] == 1){
                        $tableData .= '<p>Is paid with credits: YES</p>';
                    }else{
                        $tableData .= '<p>Is paid with credits: NO</p>';
                    }
                    $tableData .= '<p>Cashier Name: ' . $value['cashier-name'] . '</p>';
                    $tableData .= '<p>Merchant Approval Status: ' . $value['merchant-approval-status'] . '</p>';
                }
            }
            echo $tableData;
        }

		$conn->close();
	}
?>