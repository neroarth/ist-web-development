<?php
	session_start();
	ob_start();
	require_once "php-files/usefulFunction.php";
	require_once "php-files/conDb.php";

	if(!isLogin()){
		header("Location: login.php");
		exit();
	}
	
	$sql = "SELECT full_name FROM member WHERE id = ?";

	if ($stmt = $conn->prepare($sql)) {

		$stmt->bind_param("i", $_SESSION['id']);

		$stmt->execute();

		$stmt->bind_result($rsFullName);

		$stmt->fetch();

		$stmt->close();
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (isset($_POST['skip'])) {
			header("Location: verification.php");
			exit();
		}
		
		$complete = true;

		$bankType = rewrite($_POST['bank-type']);

		$accountHolderName = rewrite($_POST['account-holder-name']);
		$accountHolderName = stripSpaces($accountHolderName);
		if (strlen($accountHolderName) <= 0){
			$accountHolderNameError = "Account holder name can't be empty";
			$complete = false;
		}
		else if(strlen($accountHolderName) > 100){
			$accountHolderNameError = "Account holder name can't be more than 100 characters";
			$complete = false;
		}

		$accountNumber = rewrite($_POST['account-number']);
		$accountNumber = stripAllWhiteSpaces($accountNumber);

		if (strlen($accountNumber) <= 0){
			$accountNumberError = "Account number can't be empty";
			$complete = false;
		}
		else if(strlen($accountNumber) >= 18){
			$accountNumberError = "Invalid account number length";
		    $complete = false;
		}

		if($complete){
			$stmt = $conn->prepare("UPDATE member SET bank_type = ?, bank_account_number = ?, account_holder_name = ? WHERE id = ?");
			$stmt->bind_param("sssi", $bankType, $accountNumber, $accountHolderName, $_SESSION['id']);

			$stmt->execute();
			$stmt->close();

			header("Location: verification.php");
		}

	}

	ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<link href="images/favicon.png" rel="icon" type="image/png"/>
		<meta name="description" content="ISpendTribute">
		<meta name="author" content="Spending tribute">

		<title>Registration Part 3</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
		<script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	h1{
	color:#fff;
	padding-top:50px;
	}
	.register-text{
		color:#9c77b4;
	}
	@media (max-width: 736px) {
	.footer {
		height:18%;
	}

}
 .caption{
	 padding-top: 10px;
 }
.block{
	width:100%;
	height:20%;
	border:30px solid  #f291bb;
	padding-bottom:20px;
}
option{
	border:0;
}
#bank-type {
	width:200px;

}
	</style>
  </head>
  <?php include 'nav.php' ?>
  <body class="body-color">
    <div class="container cancelpaddingtop">
        <h1 class="paddingtop25">Create Your Account</h1> <img src="images/heart3.jpg" class="heartimg"><br>
          <form id="registerFormPart3" class="form-horizontal" role="form" method="post" action="">

					<div class="form-group">
							<div class="col-sm-4">
							<select id="bank-type" name="bank-type">
								<?php
									$rows = getListOfBanks($conn);
									foreach ($rows as &$value) {
										echo '<option value="'.$value['name'].'">'.$value['name'].'</option>';
									}
									$conn->close();
								?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-4">
							<input type="text" class="form-control form-controls text-line" id="account-holder-name" name ="account-holder-name" placeholder="Account holder name"
								   value="<?php
												if($_POST["account-holder-name"] != null){
													echo $_POST["account-holder-name"];
												}else{
													echo $rsFullName;
												}
										  ?>" required>
							<span class = "error"><?php echo $accountHolderNameError;?></span>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-4">
						  <input type="text" class="form-control form-controls text-line" id="account-number" name ="account-number" placeholder="Account number"
								 value="<?php echo $_POST["account-number"];?>" >
						  <span class = "error"><?php echo $accountNumberError;?></span>
						</div>
					</div>

				<div class="form-group caption">
					<div class="col-sm-4  register-text">
							<button type="submit" class="btn btn-default btn-min-register">Next</button>
							<input id = "skip" name="skip" type="submit" class="btn btn-default btn-min-register skiplink2" value="Skip" />
					</div>
				</div>
          </form>
    </div><!-- /.container -->
	<div class="block"></div>
	<?php include 'foot.php' ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
