<?php
    require_once "php-files/usefulFunction.php";
    require_once "php-files/conDb.php";

    if(isset($_GET['id'])){
        $id = $_GET['id'];
        $rows = getMerchantDetails($conn, $id);

        foreach ($rows as &$value) {
            $logoLink = $value['logo-link'];
            $shopName = $value['shop-name'];
            $description = $value['description'];
            $operatingHours = $value['operating-hours'];
            $email = $value['email'];
            $phoneNumber = $value['phone-number'];
            $address = $value['lot-number'] . ' ' . $value['street-name'] . ' ' . $value['postcode'] . ' ' . $value['city'] . ' ' . $value['state'];
            $website = $value['website'];
            $socialMedia = $value['social-media'];
            $category = $value['category'];
        }
    }

    $conn->close();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>iSpendtribute</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link href="main-style.css" rel="stylesheet">
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
   
</head>

<body>
    <?php include 'navProfile.php'; ?>
    
    <div class="bigdiv">
    <div class="left-div">
    <div class="shop-image">
        <?php
            if($logoLink == null){
                echo '<img class="shop-img" src="images/fromagerie.jpg">';
            }else{
                echo '<img class="shop-img" src="' . $logoLink .'">';
            }
        ?>

    </div></div>
    <div class="right-div">
        <h1 class="shopname" id="shopName"><b><?php echo $shopName;?></b></h1>
        <p class="description1"><?php echo $description;?></p>
        <p class="description"><b>Operating hours: </b><?php echo $operatingHours;?></p>
        <p class="description"><b>Email: </b><?php echo $email;?></p>
        <p class="description"><b>Contact: </b><?php echo $phoneNumber;?></p>
        <p class="description"><b>Address: </b><?php echo $address;?></p>
        <p class="description"><b>Website: </b><a href="<?php echo $website;?>" class="shop-name"><?php echo $website;?></a></p>
        <p class="description"><b>Facebook Page: </b><a href="<?php echo $socialMedia;?>" class="shop-name">Follow us</a></p>
        <p class="description"><b>Category: </b><?php echo $category;?></p>
    </div>
    
    </div>
  
    <?php include 'foot.php';?>

</body>

</html>

