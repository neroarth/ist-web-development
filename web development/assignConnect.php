<?php
	session_start();
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		ob_start();
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		$complete = true;
		
		$merchantId = $_POST['merchantList'];
		$repId = $_POST['repList'];
		
		$sql = "UPDATE merchant SET connect_id = ?, connect_admin_id = ? WHERE id = ?";
		
		if($complete){
			$stmt = $conn->prepare($sql);
			$stmt->bind_param("isi", $repId, $_SESSION['admin-id'], $merchantId);
	
			$stmt->execute();
			
			header("Location: adminAssignConnect.php");
		}else{
			header("Location: adminAssignConnect.php?success=fail");
		}
		
		$conn->close();
		ob_end_clean();
	}
?>