<?php
	session_start();
    require_once "php-files/usefulFunction.php";
	if ($_SERVER["REQUEST_METHOD"] == "POST" && isAdminLogin()) {
		require_once "php-files/conDb.php";

        $referralData = null;
        $memberId = rewrite($_POST['memberId']);

        $rows = getTopReferralTree($conn, $memberId);
        if($rows != null){
            foreach ($rows as &$value) {
                $referralData .= '</br>';
                $referralData .= '<p>' . $value['id'] . '</p>';
                $referralData .= '<p>' . $value['member-name'] . '</p>';
                $referralData .= '<p>' . $value['referral-code'] . '</p>';
                $referralData .= '</br>';
            }
        }

        $referralData .= "<p>bottom:</p>";

        $rows = getBottomReferralTree($conn, $memberId);

        if($rows != null){
            foreach ($rows as &$value) {
                $referralData .= '</br>';
                $referralData .= '<p>' . $value['id'] . '</p>';
                $referralData .= '</br>';
            }
        }

		$conn->close();

        if($referralData == null){
            echo '<tr>
                       <td>Sorry, no records found</td>
                  </tr>';
        }else{
            echo $referralData;
        }
	}
?>