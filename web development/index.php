<?php
session_start();
require_once "php-files/usefulFunction.php";
require_once "php-files/conDb.php";
if (isset($_GET["logout"])) {
    logout();
}

if (isset($_POST["submit"])) {
    // $name = $_POST['name'];
    $email = $_POST['email'];
    // $number = $_POST['number'];
    // $message =$_POST['message'];
    $to = "willteh.ist@gmail.com";
    $subject = "Newsletter";
    $message =
        // "From: ".$name."\n".
        " You have a new subscriber from this email: " . $email . "\n" .
        // "Phone number: ". $number."\n".
        // "Feedback: ". $message."\n";
        // $message = wordwrap($message,100);
        // $number = preg_replace( '/[^0-9]/', '',$number);
        $email = filter_var($email, FILTER_SANITIZE_SPECIAL_CHARS);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        echo '<script type="text/javascript">'
        , 'alert("Invalid email");',
        'window.location = "index.php";'
        , '</script>';
    } else if (mail("$to", "$subject", "$message")) {
        echo '<script type="text/javascript">'
        , 'alert("Email sent successfully!");',
        'window.location = "index.php";'
        , '</script>';
    } else {
        echo '<script type="text/javascript">',
        'window.location = "index.php";'
        , '</script>';
    }
}
$whereTextPartSql = "";
$orderByPartSql = " LIMIT 0, 5 ";
$rows = getAllMerchants($conn, $whereTextPartSql, $orderByPartSql);

$whereTextNgoPartSql = "";
$orderByPartNgoSql = " LIMIT 0, 3 ";
$ngoRows = getAllNgos($conn, $whereTextNgoPartSql, $orderByPartNgoSql);

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <title>IST-Home</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!-- [if lt IE 9]> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="assets/js/ie8-responsive-file-warning.js"></script>
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- [if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif] -->
    <!-- <script>
//	  $('#login').click(function(e) {
//
//        e.preventDefault();
//	  });

//	  function changeNav1Active() {
//		console.log("1");
//		clearActive();
//        $("#navItem1").addClass("active");
//      }
//
//	  function changeNav2Active() {
//		console.log("2");
//		clearActive();
//        $("#navItem2").addClass("active");
//      }
//
//	  function changeNav3Active() {
//		console.log("3");
//		clearActive();
//		$("#navItem3").addClass("active");
//      }
//	  function changeNav4Active() {
//		console.log("4");
//		clearActive();
//		$("#navItem4").addClass("active");
//      }
//	  function clearActive() {
//		$(".navItem1").removeClass('active');
//      }
//
</script> -->
    <style>
        .how-it-work {
            background-color: #fff;
        }

        .how-it-work h1 {
            color: black;
        }

        .how-it-work p {
            color: #9c77b4;
        }

        .margin {
            margin: 0 auto;
        }

        .how-it-work {
            border-bottom: 1px solid #f291bb;
        }

        .society {
            border-bottom: 1px solid #9c77b4;
            padding-bottom: 20px;
        }

        .course {
            padding-bottom: 20px;
            border-bottom: 1px solid #f291bb;
        }

        /*card layout*/
        .card {
            padding-top: 20px;
            margin: 10px 0 20px 0;
            width: 200px;
            background-color: rgba(214, 224, 226, 0.2);
            border-top-width: 0;
            border-bottom-width: 2px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            border: 1px solid #f291bb;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;

        }

        .card .card-heading {
            padding: 0 20px;
            margin: 0;
        }

        .card .card-heading.simple {
            font-size: 20px;
            font-weight: 300;
            color: #777;
            border-bottom: 1px solid #e5e5e5;
        }

        .card .card-heading.image img {
            display: inline-block;
            width: 46px;
            height: 46px;
            margin-right: 15px;
            vertical-align: top;
            border: 0;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
        }

        .card .card-heading.image .card-heading-header {
            display: inline-block;
            vertical-align: top;
        }

        .card .card-heading.image .card-heading-header h3 {
            margin: 0;
            font-size: 14px;
            line-height: 16px;
            color: #262626;
        }

        .card .card-heading.image .card-heading-header span {
            font-size: 12px;
            color: #999999;
        }

        .card .card-body {
            padding: 0 30px;
            margin-top: 20px;
        }

        .card .card-media {
            padding: 0 20px;
            margin: 0 -14px;
        }

        .card .card-media img {
            max-width: 100%;
            max-height: 100%;
        }

        .card .card-actions {
            min-height: 30px;
            padding: 0 20px 20px 20px;
            margin: 20px 0 0 0;
        }

        .card .card-comments {
            padding: 20px;
            margin: 0;
            background-color: #f8f8f8;
        }

        .card .card-comments .comments-collapse-toggle {
            padding: 0;
            margin: 0 20px 12px 20px;
        }

        .card .card-comments .comments-collapse-toggle a,
        .card .card-comments .comments-collapse-toggle span {
            padding-right: 5px;
            overflow: hidden;
            font-size: 12px;
            color: #999;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .card-comments .media-heading {
            font-size: 13px;
            font-weight: bold;
        }

        .card.people {
            position: relative;
            display: inline-block;
            width: 170px;
            height: 300px;
            padding-top: 0;
            margin-left: 20px;
            overflow: hidden;
            vertical-align: top;
        }

        .card.people:first-child {
            margin-left: 0;
        }

        .card.people .card-top {
            position: absolute;
            top: 0;
            left: 0;
            display: inline-block;
            width: 170px;
            height: 150px;
            background-color: #ffffff;
        }

        .card.people .card-top.green {
            background-color: #53a93f;
        }

        .card.people .card-top.blue {
            background-color: #427fed;
        }

        .card.people .card-info {
            position: absolute;
            top: 150px;
            display: inline-block;
            width: 100%;
            height: 101px;
            overflow: hidden;
            background: #ffffff;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .card.people .card-info .title {
            display: block;
            margin: 8px 14px 0 14px;
            overflow: hidden;
            font-size: 16px;
            font-weight: bold;
            line-height: 18px;
            color: #404040;
        }

        .card.people .card-info .desc {
            display: block;
            margin: 8px 14px 0 14px;
            overflow: hidden;
            font-size: 12px;
            line-height: 16px;
            color: #737373;
            text-overflow: ellipsis;
        }

        .card.people .card-bottom {
            position: absolute;
            bottom: 0;
            left: 0;
            display: inline-block;
            width: 100%;
            padding: 10px 20px;
            line-height: 29px;
            text-align: center;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .card.hovercard {
            position: relative;
            padding-top: 0;
            overflow: hidden;
            text-align: center;
            background-color: #FFF;
            margin-left: 30px;
        }

        .card.hovercard .cardheader {
            background-size: cover;
            height: 100px;

        }

        .card.hovercard .avatar {
            position: relative;
            bottom: 50px;
            margin-bottom: -50px;
        }

        .card.hovercard .avatar img {
            width: 100px;
            height: 100px;
            max-width: 100px;
            max-height: 100px;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            border: 5px solid rgba(255, 255, 255, 0.5);
        }

        .card.hovercard .info {
            padding: 4px 8px 10px;
        }

        .card.hovercard .info .title {
            margin-bottom: 4px;
            font-size: 24px;
            line-height: 1;
            color: #262626;
            vertical-align: middle;
        }

        .card.hovercard .info .desc {
            overflow: hidden;
            font-size: 12px;
            line-height: 20px;
            text-overflow: ellipsis;
        }

        .card.hovercard .bottom {
            padding: 0 20px;
            margin-bottom: 18px;
        }

        @media all and (max-width: 1230px) {
            .card.hovercard {
                margin: auto;
            }
        }

        @media all and (max-width: 1092px) {
            .card.hovercard {
                margin: auto;
            }
        }

        @media all and (max-width: 980px) {
            .card.hovercard {
                margin: auto;
            }
        }

        @media all and (max-width: 882px) {
            .card.hovercard {
                margin: auto;
            }
        }

        @media all and (max-width: 808px) {
            .card.hovercard {
                margin: auto;
            }
        }

        /*.btn{ border-radius:20px; width:auto; height:40px; line-height:2px;  }*/

    </style>
</head>

<body>
<nav class="nav navbar-default  navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img class="img-responsive indexlogo" alt="Ist" src="images/brand.png">
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <!--target-active-->
            <ul class="nav navbar-nav navbar-right text nav-margin">
                <li><a href="index.php">How we help</a></li>
                <!--id="navItem1" onclick="changeNav1Active()" class="active navItem"-->
                <li><a href="#shop">Choose Us</a></li>
                <li><a href="charity.php">Marketing Kit</a></li>
                <li>
                    <?php
                    if (isLogin() || isAdminLogin() || isCashierLogin()) {
                        echo '<div class="btn-nav logina"><a class="btn btn-small navbar-btn pink" href="index.php?logout=true">Logout</a></div>';

                        echo '</li>';
                        echo '<div class="btn-nav col-xs-1  logina"><li><a class="btn btn-small navbar-btn pink-two" href="profile.php"><span style="color:#ffffff">Your Profile</span></a></li></div>';
                    } else {
                        echo '<div class="btn-nav logina"><a class="btn btn-small navbar-btn pink" href="login.php">Log In</a></div>';

                        echo '</li>';
                        echo '<div class="btn-nav col-xs-1 logina2"><li><a class="btn btn-small navbar-btn pink-two" href="memberRegistration.php"><span style="color:#ffffff">Register</span></a></li></div>';
                    }
                    ?>

            </ul>
        </div>
    </div>
    <!--/.nav-collapse -->
</nav>
<!-- Carousel -->
<div id="carousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
    </ol>
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img src="./images/header.jpg" alt="First slide">
            <!-- Static Header -->

            <!-- /header-text -->
        </div>
        <div class="item">
            <img src="./images/header_2.jpg" alt="Second slide">
            <!-- Static Header -->

            <!-- /header-text -->
        </div>
        <div class="item">
            <img src="./images/header_3.jpg" alt="Third slide">

            <!-- /header-text -->
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#carousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
</div>
<!-- /carousel -->
<!-- mobile textbox -->
<!-- add carousel function to this two textbox -->
<!-- <div class="item active"> -->
<!-- Static Header -->
<!-- <div class="mobile_box mobile_show">
                            <div class="text-center">
                                    <h2>
                                        <strong>Donate As You Spend</strong>
                                    </h2>
                                            <a class="btn btn-default btn-min-block" href="#">Join Now</a>
                            </div>
                    </div> -->
<!-- /header-text -->
<!-- </div> -->
<!-- <div class="item"> -->
<!-- Static Header -->
<!-- <div class="mobile_box mobile_show">
                            <div class="text-center">
                                     <h2>
                                        <strong>Donate As You Spend</strong>
                                    </h2>
                                    <div class="text-center">
                                            <a class="btn btn-default btn-min-block" href="#">Join Now</a></div>
                            </div>
                    </div> -->
<!-- /header-text -->
<!-- </div> -->
<!-- <div class="container-fluid   welcome text-center  caption">
    <h1 class="caption">Welcome to iSpendTribute</h1>
    <p> Student..?Technician..?Doctor..?Business owner..?</p>
    <p> No matter who you are </p>
    <h3> The world needs you !</h3>
</div> -->
<!--how it works-->
<div class="container text-center" id="howitworks">
    <h1 class="mth1">How iST works</h1>
    <div class="row row-mobile">
        <div class="col-md-2 col-md-offset-1">
            <div class="img-responsive img-rounded"><img alt="Strategic Location" src="./images/work_1.png"></div>
            <div class="caption text-center">
                <a href="memberRegistration.php"><span class="underline"> Join us!!!</span></a>
                <p>It’s easy to get started, register for FREE and start Spendtributing</p>
            </div>
        </div>
        <div class="col-md-2 img-responsive">
            <img src="./images/arrow_03.png" class="arrow"></img>
        </div>
        <div class="col-md-2">
            <div class="img-responsive img-rounded"><img alt="Paperless" src="./images/work_2.png">
            </div>
            <div class="caption">
                <a href="#"><span class="underline"> Save</span></a>
                <p>Shop at iSpendtribute partnered merchants and start saving $$</p>
            </div>
        </div>
        <div class="col-md-2  img-responsive work">
            <!-- <img src="./images/arrow-02.png" class="arrow"> -->
            <img src="./images/arrow_03.png" class="arrow"></img>
        </div>
        <div class="col-md-2">
            <div class="img-responsive img-rounded"><img alt="Instant Service" src="./images/work_3.png"></div>
            <div class="caption">
                <a href="#"><span class="underline">Contribute</span></a>
                <p>Help out when you cash out – it doesn’t cost you a cent extra!</p>
            </div>
        </div>
    </div>
</div>
<div class="heartimgpink mtop">
    <img src="images/pinkheart-line.jpg" class="heartline">
</div>
<div class="container text-center">
    <h1 class="m60">Change society for the better</h1>
    <div class="row center-block">
        <?php
        //        <div class="col-sm-4 m6">
        //               <img src="./images/dummy.png" class="img-responsive margin" style="width:auto;" alt="Image">
        //         </div>
        foreach ($ngoRows as &$value) {
            $id = $value['id'];
            $logoLink = $value['logo-link'];
            $name = $value['name'];
            $description = $value['description'];
            $operatingHours = $value['operating-hours'];
            $email = $value['email'];
            $phoneNumber = $value['phone-number'];
            $lotNumber = $value['lot-number'];
            $streetName = $value['street-name'];
            $postcode = $value['postcode'];
            $city = $value['city'];
            $state = $value['state'];
            $country = $value['country'];
            $website = $value['website'];
            $socialMedia = $value['social-media'];
            $category = $value['category'];
            $latitude = $value['latitude'];
            $longitude = $value['longitude'];
            $dateJoined = $value['date-joined'];

            echo '<div class="col-sm-4 m6">';
            if ($logoLink == null) {
                echo '<img src="./images/dummy.png" class="img-responsive margin" style="width:auto;" alt="' . $name . '">';
            } else {
                echo '<img src="' . $logoLink . '" class="img-responsive margin" style="width:auto;" alt="' . $name . '">';
            }
            echo '<div class = "caption  caption-div">';
            echo "<p class = 'charity-p-index'>$name</p>";
            echo '</div>';
            echo '</div>';
        }
        ?>
        <!--        <div class="col-sm-4 m6">-->
        <!--            <img src="./images/dummy.png" class="img-responsive margin" style="width:auto;" alt="Image">-->
        <!--        </div>-->
        <!--                <div class="col-sm-4 m6">-->
        <!--                    <img src="./images/dummy.png" class="img-responsive margin" style="width:auto;" alt="Image">-->
        <!--                </div>-->
    </div>
</div>
<div class="heartimgpink mt60">
    <img src="images/purpleheart-line.jpg" class="heartline">
</div>
<div class="container text-center">
    <h1 class="m60">Select a course</h1>
    <div class="row center-block">
        <?php
        //            <div class="col-sm-2" >
        //              <img src="./images/dummy.png" class="img-responsive margin" style="width:auto;" alt="Image">
        //            </div>
        foreach ($rows as &$value) {
            $id = $value['id'];
            $logoLink = $value['logo-link'];
            $shopName = $value['shop-name'];
            $description = $value['description'];
            $operatingHours = $value['operating-hours'];
            $email = $value['email'];
            $phoneNumber = $value['phone-number'];
            $lotNumber = $value['lot-number'];
            $streetName = $value['street-name'];
            $postcode = $value['postcode'];
            $city = $value['city'];
            $state = $value['state'];
            $country = $value['country'];
            $website = $value['website'];
            $socialMedia = $value['social-media'];
            $category = $value['category'];
            $latitude = $value['latitude'];
            $longitude = $value['longitude'];
            $dateJoined = $value['date-joined'];

            echo '<div class="col-sm-2"  style="margin-left:2.5%;">';
            if ($logoLink == null) {
                echo '<img src="./images/dummy.png" class="img-responsive margin image-index" style="width:145px; height:145px;" alt="' . $shopName . '">';
            } else {
                echo ' <img src="' . $logoLink . '" class="img-responsive margin" style="width:145px; height:145px;"  alt="' . $shopName . '">';
            }
            echo '<div class = "caption caption-div">';
            echo "<p class = 'charity-p-index'>$shopName</p>";
            echo '</div>';
            echo '</div>';
        }
        ?>
    </div>
</div>

<!--shops-->
<!-- <div class="container text-center shop caption" id="shop">
    <h1>Partners</h1>
    <p class="text-muted">They care for you </p>
    <p class="text-muted"> Start Spendtributing here</p>
    <a href="#" class="shop-contribute">Browse all partners</a>
    <div class="row">
            <div class="col-sm-3 col-xs-6">
                <div class="img-rounded"><img alt="lebanon" src="./images/lebanon_restaurant.jpg"></div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="fromagerie" src="./images/fromagerie.jpg"></div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="le_perigold" src="./images/le_perigord.jpg"></div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="blackdoor" src="./images/blackdoor.jpg"></div>
            </div>
            <div class="collapse" id="demo">
            <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="jc" src="./images/jc_logo.png"></div>
            </div>
            <div class="col-sm-3 col-xs-6" >
                <div class="img-responsive img-rounded"><img alt="cinnamon" src="./images/cinnamon.png"></div>
            </div>
            <div class="col-sm-3 col-xs-6" >
                <div class="img-responsive img-rounded"><img alt="hana_cafe" src="./images/hana_cafe.png"></div>
            </div>
            <div class="col-sm-3 col-xs-6">
                  <div class="img-responsive img-rounded text-center"><img alt="fur_kids" src="./images/fur_kids_logo.jpg"></div>
            </div>
             <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="Strategic Location" src="./images/rounded.png"></div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="Paperless" src="./images/rounded.png"></div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="Instant Service" src="./images/rounded.png"></div>
            </div>
            <div class="col-sm-3 col-xs-6">
                <div class="img-responsive img-rounded"><img alt="Instant Service" src="./images/rounded.png"></div>
            </div>
        </div>
<div class="caption">
<button class="btn center-block purple" type="button" data-toggle="collapse" data-target="#demo">See all</a>
</div>
</div>
</div> -->
<!-- <div class="text-center charity caption" id="charity">
			<h1>Social Projects</h1>
			<div>
				<p>They need your help!</p>
				<p>find your favourite cause here </p>
			</div>
      <div class="container">
			<div class="row">
			<a href="#" class="shop-charity">browse all project</a>
    </div>
    <div class="col-sm-3 col-xs-6">
      <div class="img-responsive img-rounded text-center"><img alt="fur_kids" src="./images/fur_kids_logo.jpg"></div>
    </div>
				<div class="col-sm-6 col-md-offset-3 form group caption">
					<button class="btn center-block pink" type="submit">See all</button>
				</div>
      </div>
  </div> -->
<!--faq-->
<!-- <div class="container caption" id="faq">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<h1 class="faq-color text-center">VIQ</h1>
					<p class="text-center text-muted">No worries, we got your questions covered ;) </p>
					<div class="panel-group" id="accordion">
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
                  	General Question
               <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"></a>
			    </h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse">
								<div class="panel-body">
									Any registed user, who presents a work, which is genuine and appealing, can post it on <strong>PrepBootstrap</strong>.
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
                	Memberships
				    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"></a>
                </h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse">
								<div class="panel-body">
									The steps involved in this process are really simple. All you need to do is:
									<ul>
										<li>Register an account</li>
										<li>Activate your account</li>
										<li>Go to the <strong>Themes</strong> section and upload your theme</li>
										<li>The next step is the approval step, which usually takes about 72 hours.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
                  	Charity Organizations
					<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive"></a>
                </h4>
							</div>
							<div id="collapseFive" class="panel-collapse collapse">
								<div class="panel-body">
									Here, at <strong>PrepBootstrap</strong>, we offer a great, 70% rate for each seller, regardless of any restrictions, such as volume, date of entry, etc.
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
									Merchant
					   <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
					   </a>
                </h4>
							</div>
							<div id="collapseSix" class="panel-collapse collapse">
								<div class="panel-body">
									There are a number of reasons why you should join us:
									<ul>
										<li>A great 70% flat rate for your items.</li>
										<li>Fast response/approval times. Many sites take weeks to process a theme or template. And if it gets rejected, there is another iteration. We have aliminated this, and made the process very fast. It only takes up to 72 hours for a template/theme
											to get reviewed.</li>
										<li>We are not an exclusive marketplace. This means that you can sell your items on <strong>PrepBootstrap</strong>, as well as on any other marketplate, and thus increase your earning potential.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
                  	Payment System
										<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight"></a>
                </h4>
							</div>
							<div id="collapseEight" class="panel-collapse collapse">
								<div class="panel-body">
									The best way to transfer funds is via Paypal. This secure platform ensures timely payments and a secure environment.
								</div>
							</div>
						</div>
						<div class="panel">
							<div class="panel-heading">
								<h4 class="panel-title">
                  Collaboration
					<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
					</a>
                </h4>
							</div>
							<div id="collapseSeven" class="panel-collapse collapse">
								<div class="panel-body">
									Each item in <strong>PrepBootstrap</strong> is maintained to its latest version. This ensures its smooth operation.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    </div> -->
<div class="heartimgpink mt60">
    <img src="images/pinkheart-line.jpg" class="heartline">
</div>
<div class="container feature">
    <div class="row">
        <h1 class=" text-center m60">Featured Hot Deal</h1>
        <div class="col-lg-3 col-sm-6">
            <div class="card hovercard margin card1234">
                <div class="cardheader">
                </div>
                <div class="avatar">
                    <img alt="" src="./images/rounded.png">
                </div>
                <div class="info">
                    <div class="desc">Passionate designer</div>
                </div>
                <div class="bottom">
                    <a class="btn pink-two" href="charity.php">
                        REVEAL DEAL
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 t6">
            <div class="card hovercard margin card1234">
                <div class="cardheader">
                </div>
                <div class="avatar">
                    <img alt="" src="./images/rounded.png">
                </div>
                <div class="info">
                    <div class="desc">Passionate designer</div>
                </div>
                <div class="bottom">
                    <a class="btn pink-two" href="charity.php">
                        REVEAL DEAL
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 t6">
            <div class="card hovercard card123">
                <div class="cardheader">
                </div>
                <div class="avatar">
                    <img alt="" src="./images/rounded.png">
                </div>
                <div class="info">
                    <div class="desc">Passionate designer</div>

                </div>
                <div class="bottom">
                    <a class="btn pink-two" href="charity.php">
                        REVEAL DEAL
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="card hovercard card123">
                <div class="cardheader">
                </div>
                <div class="avatar">
                    <img alt="" src="./images/rounded.png">
                </div>
                <div class="info">
                    <div class="desc">Promotion detail 30%</div>
                </div>
                <div class="bottom">
                    <a class="btn pink-two" href="charity.php">
                        REVEAL DEAL
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<a class="btn pink-two center-block custom1" href="#" style="margin-bottom:20px;">
    Show More
</a>

<!-- <div class="container-fluid text-center charity" id="contact">
    <h2>Contact</h2>
    <form id="indexForm" role="form" method="post" action="">
        <div class="row">
            <div class="col-sm-6 col-md-offset-3">
                <div class="col-sm-6 col-md-offset-3 form-group">
                    <input class="form-control" id="name" name="name" placeholder="Name" required="" type="text" />
                </div>
                <div class="col-sm-6  col-md-offset-3 form-group">
                    <input class="form-control" id="email" name="email" placeholder="Email" required="" type="email" />
                </div>
                <div class="col-sm-6 col-md-offset-3 form-group">
                    <input class="form-control" id="phone number" name="number" placeholder="Phone Number" required="" type="phone number" />
                </div>
                <div class=" col-sm-6 col-md-offset-3 form-group">
                    <textarea class="form-control" id="exampleTextarea" name="message" placeholder="Message" rows="3"></textarea>
                </div>
                <div class="col-sm-6 col-md-offset-3 form-group btmsmall">
                    <button class="btn center-block pinktwo" type="submit" name="submit">Send</button>
                </div>
            </div>
        </div>
    </form>
<<<<<<< HEAD
</div>-->


<?php include 'indexfooter.php' ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
    function checkCaptcha() {
        var googleResponse = jQuery('#g-recaptcha-response').val();
        if (!googleResponse) {
            $('<p style="color:red !important" class="error-captcha"><span class="glyphicon glyphicon-remove " ></span> Please fill up the captcha.</p>" ').insertAfter("#html_element");
            return false;
        } else {

            return true;
        }
    }
</script>
<script>
    window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')
</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
<style>
    .heartline {
        width: 100%;
    }

    .mtop {
        margin-top: 55px;
    }

    .mth1 {
        margin-bottom: 35px;
        margin-top: 0;
    }

    .m60 {
        margin-bottom: 60px;
        margin-top: 0;
    }

    .mt60 {
        margin-top: 60px;
    }

    .custom1 {
        margin-top: 5% !important;
        margin-bottom: 5% !important;
    }

    .mtsmall {
        margin-top: 3%;

    }

    .btmsmall {
        margin-bottom: 6%;
    }

    @media all and (max-width: 1199px) {
        .card123 {
            margin-top: 10% !important;

        }
    }

    @media all and (max-width: 974px) {
        img.indexlogo {
            height: 40px !important;
            margin-top: 2px;

        }

    }

    @media (max-width: 988px) {
        .navbar-header {
            float: none;
        }

        .navbar-left, .navbar-right {
            float: none !important;
        }

        .navbar-toggle {
            display: block;
        }

        .navbar-collapse {
            border-top: 1px solid transparent;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.1);
        }

        .navbar-fixed-top {
            top: 0;
            border-width: 0 0 1px;
        }

        .navbar-collapse.collapse {
            display: none !important;
        }

        .navbar-nav {
            float: none !important;
            margin-top: 7.5px;
        }

        .navbar-nav > li {
            float: none;
        }

        .navbar-nav > li > a {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        .collapse.in {
            display: block !important;
        }

        .logina {
            padding-left: 10px;
        }

        .logina2 {
            margin-left: -5px;
        }
    }

    @media (max-width: 617px) {
        .custom1 {
            margin-top: 7% !important;
            margin-bottom: 7% !important;
        }
    }

    @media (max-width: 767px) {
        .m6 {
            margin-top: 6%;
        }

        .t6 {
            margin-top: 10% !important;
        }
    }

    @media (max-width: 307px) {
        img.indexlogo {
            height: 30px !important;
            margin-top: 7px !important;
        }
    }
</style>
</body>

</html>
