<?php
	session_start();
	ob_start();
	require_once "php-files/conDb.php";
	require_once "php-files/usefulFunction.php";

	if(!isMerchantLogin()){
		header("Location: index.php");
		exit();
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$complete = true;

	/*********************************************************************USERNAME***************************************************************/
			$username = rewrite($_POST["username"]);
			$username = stripAllWhiteSpaces($username);
			if (strlen($username) == 0){
			   $usernameError = "Your username should not be empty!";
			   $complete = false;
			}
			else if(strlen($username) > 30){
			   $usernameError = "Your username can't be more than 30 characters";
			   $complete = false;
		   }else{
				$sql = "SELECT name FROM cashier WHERE username = ?";

				if ($stmt = $conn->prepare($sql)) {

					/* bind parameters for the ? variable */
					$stmt->bind_param("s", $username);

					/* execute query */
					$stmt->execute();

					/* bind result variables */
					$stmt->bind_result($isUsernameExist);

					/* fetch value */
					$stmt->fetch();

					/* close statement */
					$stmt->close();
				}

				if($isUsernameExist != null){
					$usernameError = "Username already exists!";
					$complete = false;
				}
		   }

	/*********************************************************************PASSWORD***************************************************************/
			$password = $_POST["password"];
			if (!preg_match("/^[a-zA-Z0-9]+$/",$password)) {
				  $passwordError = "Only letters and numbers allowed (no spaces too)";
				  $complete = false;
			}
			else if(strlen($password) < 8){
				  $passwordError = "Password length needs to be at least 8";
				  $complete = false;
			}
			else{
				$password = hash('sha256',$password);
				//adds a random salt with random number from 0 to 100
				$salt = substr(sha1(mt_rand()), 0, 100);
				$finalPassword = hash('sha256', $salt.$password);
			}

	/*********************************************************************FULL NAME***************************************************************/
			$fullName = rewrite($_POST["full-name"]);
			$fullName = stripSpaces($fullName);
			if (strlen($fullName) == 0){
			   $fullNameError = "Your name should not be empty!";
			   $complete = false;
			}
			else if(strlen($fullName) > 100){
			   $fullNameError = "Your name can't be more than 100 characters";
			   $complete = false;
		   }
	/*********************************************************************ENDs HERE***************************************************************/

			if($complete){
                $pinNo = mt_rand(100000,999999);

				$stmt = $conn->prepare("INSERT INTO cashier (username, password, salt, name, merchant_id, pin_no, pin_salt) 
                                              VALUES (?, ?, ?, ?, ?, ?, ?)");
				$stmt->bind_param("ssssiss", $username, $finalPassword, $salt, $fullName, $_SESSION['merchant-id'], $pinNo, $pinSalt);
				$stmt->execute();

				//$_SESSION["cashier-id"]=$username;

				header("Location: index.php");

				$stmt->close();
			}
	}

	ob_end_clean();
	$conn->close();

	//if($complete && $phoneNoError == null && $emailError == null && $referralCodeError == null){
	//	echo '<script language="javascript">';
	//	echo 'alert("Account registered successfully")';
	//	echo '</script>';
	//}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<link href="images/favicon.png" rel="icon" type="image/png"/>
		<meta name="description" content="ISpendTribute">
		<meta name="author" content="Spending tribute">

		<title>Cashier Registration</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
		<script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	h1{
	color:#fff;
	padding-top:50px;
	}
	.register-text{
		color:#9c77b4;
	}
	@media (max-width: 736px) {
	.footer {
		height:18%;
	}

}
 .caption{
	 padding-top: 10px;
 }
.block{
	width:100%;
	height:20%;
	border:30px solid  #f291bb;
	padding-bottom:20px;
}
	</style>
  </head>
  <?php include 'nav.php' ?>
  <body class="body-color">
    <div class="container cancelpaddingtop">
        <h1 class="paddingtop25">Create a Cashier Account</h1>
          <form id="cashierRegistrationForm" class="form-horizontal" role="form" method="post" action="">

			<div class="form-group">
                <div class="col-sm-4">
                  <input type="text" class="form-control form-controls text-line" id="username" name ="username" placeholder="Enter your username"
						 value="<?php echo $_POST["username"];?>" required>
                  <span class = "error"><?php echo $usernameError;?></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-4">
                  <input type="password" pattern=".{8,}" oninvalid="setCustomValidity('Password length needs to be at least 8');"
						 class="form-control form-controls text-line" oninput="setCustomValidity('');"
						 id="password" name ="password" placeholder="Enter password" required>
                  <span class = "error"><?php echo $passwordError;?></span>
                </div>
            </div>

			<div class="form-group">
                <div class="col-sm-4">
                  <input type="text" class="form-control form-controls text-line" id="full-name" name ="full-name" placeholder="Enter your full name"
						 value="<?php echo $_POST["full-name"];?>" required>
                  <span class = "error"><?php echo $fullNameError;?></span>
                </div>
            </div>

            <div class="form-group caption">
				<div class="col-sm-4  register-text">
					<button type="submit" class="btn btn-default btn-min-register">Register as Cashier</button>
				</div>
            </div>
          </form>
    </div><!-- /.container -->
	<div class="block"></div>
	<?php include 'foot.php' ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
