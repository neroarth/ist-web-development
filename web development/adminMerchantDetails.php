<?php
session_start();
ob_start();
require_once "php-files/usefulFunction.php";
require_once "php-files/conDb.php";

if (!isAdminLogin()) {
    header("Location: index.php");
    exit();
}

ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link href="images/favicon.png" rel="icon" type="image/png"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">

    <title>Merchant Details</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        h1 {
            color: #fff;
            padding-top: 50px;
        }

        .register-text {
            color: #9c77b4;
        }

        @media (max-width: 736px) {
            .footer {
                height: 18%;
            }

        }

        .caption {
            padding-top: 10px;
        }

        .block {
            width: 100%;
            height: 20%;
            border: 30px solid #f291bb;
            padding-bottom: 20px;
        }

        option {
            border: 0;
        }

        #bank-type {
            width: 200px;

        }

        #auto_load_div > div {
            width: 100%;
            max-width: 320px;
        }
    </style>
</head>
<?php include 'nav.php' ?>
<body class="body-color">
<div class="container" style="padding-top:0px;">
    <h1 class="text-center" style="padding-bottom:20px; padding-top:20px;">Approve Merchant Here</h1>
    <div class="form-wrap" style="padding-top:0px;">
        <form action="approveMerchant" class="form-horizontal" method="post" enctype="multipart/form-data"
              id="merchantListForm" style="padding-top:15px;">
            <div class="form-group formdiv" style="text-align:left;">

                <h2 class="text-center" style="margin-bottom:0; margin-top:0px;">Merchant Info</h2>
                <div style="text-align:center; padding-top:10px;">
                    <select id="merchantList" name="merchantList" onChange="getMerchantDetails(this);">
                        <?php
                        $rows = getListOfMerchants($conn, "SELECT id, shop_name FROM merchant WHERE approval_status = ?", "pending");
                        foreach ($rows as &$value) {
                            echo '<option value="' . $value['id'] . '">' . $value['shop_name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <table border="1" cellspacing="0" cellpadding="0" class="merchanttable">
                <tr>
                    <td width="50%" valign="top"><p>SSN Link:</p></td>
                    <td width="50%" valign="top"><p id="ssn-link">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Logo Link:</p></td>
                    <td width="50%" valign="top"><p id="logo-link">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Shop name:</p></td>
                    <td width="50%" valign="top"><p id="shop-name">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Description:</p></td>
                    <td width="50%" valign="top"><p id="description">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Operation:</p></td>
                    <td width="50%" valign="top"><p id="operating-hours">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Email:</p></td>
                    <td width="50%" valign="top"><p id="email">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Phone Number:</p></td>
                    <td width="50%" valign="top"><p id="phone-number">0</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Lot Number:</p></td>
                    <td width="50%" valign="top"><p id="lot-number">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Street Name:</p></td>
                    <td width="50%" valign="top"><p id="street-name">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Postcode:</p></td>
                    <td width="50%" valign="top"><p id="postcode">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>City:</p></td>
                    <td width="50%" valign="top"><p id="city">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>State:</p></td>
                    <td width="50%" valign="top"><p id="state">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Country:</p></td>
                    <td width="50%" valign="top"><p id="country">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Website:</p></td>
                    <td width="50%" valign="top"><p id="website">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Social Media:</p></td>
                    <td width="50%" valign="top"><p id="social-media">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Date Joined:</p></td>
                    <td width="50%" valign="top"><p id="date-joined">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Bank Type:</p></td>
                    <td width="50%" valign="top"><p id="bank-type">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Bank Account Number:</p></td>
                    <td width="50%" valign="top"><p id="bank-account-number">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Account Holder Name:</p></td>
                    <td width="50%" valign="top"><p id="account-holder-name">&nbsp;</p></td>
                </tr>
            </table>

            <div class="form-group formdiv">

                <textarea class="form-control" rows="5" id="remark" name="remark"
                          placeholder="Remark"><?php echo $_GET['remark']; ?></textarea>
                <span class="error"><?php echo $_GET['remarkError']; ?></span>
            </div>

            <input id="approve" name="approve" type="submit" class="btn btn-default btn-min-register" value="Approve"/>
            <input id="reject" name="reject" type="submit" class="btn btn-default btn-min-register" value="Reject"/>
        </form>
    </div><!--merchant div ends here-->

    <h1 class="text-center" style="padding-bottom:20px; padding-top:20px;">Approve NGO Here</h1>
    <div class="form-wrap" style="padding-top:0px;">
        <form action="approveNgo" class="form-horizontal" method="post" enctype="multipart/form-data"
              id="ngoListForm" style="padding-top:15px;">
            <div class="form-group formdiv" style="text-align:left;">

                <h2 class="text-center" style="margin-bottom:0; margin-top:0px;">NGO Info</h2>
                <div style="text-align:center; padding-top:10px;">
                    <select id="ngoList" name="ngoList" onChange="getNgoDetails(this);">
                        <?php
                            $rows = getListOfNgo($conn, "SELECT id, name FROM ngo WHERE approval_status = ?", "pending");
                            foreach ($rows as &$value) {
                                echo '<option value="' . $value['id'] . '">' . $value['name'] . '</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            <table border="1" cellspacing="0" cellpadding="0" class="merchanttable">
                <tr>
                    <td width="50%" valign="top"><p>SSN Link:</p></td>
                    <td width="50%" valign="top"><p id="ngo-ssn-link">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Logo Link:</p></td>
                    <td width="50%" valign="top"><p id="ngo-logo-link">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Shop name:</p></td>
                    <td width="50%" valign="top"><p id="ngo-shop-name">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Description:</p></td>
                    <td width="50%" valign="top"><p id="ngo-description">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Operation:</p></td>
                    <td width="50%" valign="top"><p id="ngo-operating-hours">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Email:</p></td>
                    <td width="50%" valign="top"><p id="ngo-email">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Phone Number:</p></td>
                    <td width="50%" valign="top"><p id="ngo-phone-number">0</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Lot Number:</p></td>
                    <td width="50%" valign="top"><p id="ngo-lot-number">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Street Name:</p></td>
                    <td width="50%" valign="top"><p id="ngo-street-name">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Postcode:</p></td>
                    <td width="50%" valign="top"><p id="ngo-postcode">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>City:</p></td>
                    <td width="50%" valign="top"><p id="ngo-city">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>State:</p></td>
                    <td width="50%" valign="top"><p id="ngo-state">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Country:</p></td>
                    <td width="50%" valign="top"><p id="ngo-country">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Website:</p></td>
                    <td width="50%" valign="top"><p id="ngo-website">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Social Media:</p></td>
                    <td width="50%" valign="top"><p id="ngo-social-media">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Date Joined:</p></td>
                    <td width="50%" valign="top"><p id="ngo-date-joined">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Bank Type:</p></td>
                    <td width="50%" valign="top"><p id="ngo-bank-type">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Bank Account Number:</p></td>
                    <td width="50%" valign="top"><p id="ngo-bank-account-number">&nbsp;</p></td>
                </tr>
                <tr>
                    <td width="50%" valign="top"><p>Account Holder Name:</p></td>
                    <td width="50%" valign="top"><p id="ngo-account-holder-name">&nbsp;</p></td>
                </tr>
            </table>

            <div class="form-group formdiv">

                <textarea class="form-control" rows="5" id="remarkNgo" name="remarkNgo"
                          placeholder="Remark"><?php echo $_GET['remarkNgo']; ?></textarea>
                <span class="error"><?php echo $_GET['remarkNgoError']; ?></span>
            </div>

            <input id="approveNgo" name="approveNgo" type="submit" class="btn btn-default btn-min-register" value="Approve"/>
            <input id="rejectNgo" name="rejectNgo" type="submit" class="btn btn-default btn-min-register" value="Reject"/>
        </form>
    </div><!--ngo div ends here-->

    <!--business rep div starts here-->
    <h1 class="text-center" style="padding-top:20px; padding-bottom:20px;">Approve Business Rep here</h1>
    <div class="form-wrap">
        <form action="approveBusinessRep" class="form-horizontal" method="post" enctype="multipart/form-data"
              id="repListForm">
            <div class="form-group formdiv" style="text-align:left;">

                <b>Business Rep </b> <select id="repList" name="repList" onChange="getRepDetails(this);">
                    <?php
                    $rows = getListOfMembers($conn, "SELECT id, full_name FROM member WHERE biz_rep_status = ?", "pending");
                    foreach ($rows as &$value) {
                        echo '<option value="' . $value['id'] . '">' . $value['full_name'] . '</option>';
                    }
                    $conn->close();
                    ?>
                </select>

            </div>
            <table border="1" cellspacing="0" cellpadding="0" class="merchanttable">
                <tr>
                    <td width="192" valign="top"><p>Full Name:</p></td>
                    <td width="50%" valign="top"><p id="rep-full-name">Kit</p></td>
                </tr>
                <tr>
                    <td width="192" valign="top"><p>Representative Email:</p></td>
                    <td width="50%" valign="top"><p id="rep-email">yungkit14@gmail.com</p></td>
                </tr>
                <tr>
                    <td width="192" valign="top"><p>Representative Phone Number:</p></td>
                    <td width="50%" valign="top"><p id="rep-phone-no">164637931</p></td>
                </tr>
            </table>
            <!--Full name:<div id="rep-full-name"></div>
            Representative Email :<div id="rep-email"></div>
            Representative Phone number:<div id="rep-phone-no"></div>-->

            <div class="form-group formdiv">
                <textarea class="form-control" rows="5" id="remarkRep" name="remarkRep"
                          placeholder="Remark"><?php echo $_GET['remarkRep']; ?></textarea>
                <span class="error"><?php echo $_GET['remarkRepError']; ?></span>
            </div>

            <input id="approveRep" name="approveRep" type="submit" class="btn btn-default btn-min-register"
                   value="Approve"/>
            <input id="rejectRep" name="rejectRep" type="submit" class="btn btn-default btn-min-register"
                   value="Reject"/>
        </form>
    </div>
</div><!-- /.container -->
<div class="block"></div>
<?php include 'foot.php' ?>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script type="text/javascript">
    function getMerchantDetails() {
        var merchantList = $('#merchantList').val();
        $.ajax({
            type: 'post',
            url: "listMerchant.php",
            data: {merchantList: merchantList},
            cache: false,
            success: function (data) {
                $("#ssn-link").html(data["ssn-link"]);
                $("#logo-link").html(data["logo-link"]);
                $("#shop-name").html(data["shop-name"]);
                $("#description").html(data["description"]);
                $("#operating-hours").html(data["operating-hours"]);
                $("#email").html(data["email"]);
                $("#phone-number").html(data["phone-number"]);
                $("#lot-number").html(data["lot-number"]);
                $("#street-name").html(data["street-name"]);
                $("#postcode").html(data["postcode"]);
                $("#city").html(data["city"]);
                $("#state").html(data["state"]);
                $("#country").html(data["country"]);
                $("#website").html(data["website"]);
                $("#social-media").html(data["social-media"]);
                $("#date-joined").html(data["date-joined"]);
                $("#bank-type").html(data["bank-type"]);
                $("#bank-account-number").html(data["bank-account-number"]);
                $("#account-holder-name").html(data["account-holder-name"]);
            }
        });
    }

    function getNgoDetails() {
        var ngoList = $('#ngoList').val();
        $.ajax({
            type: 'post',
            url: "listNgo.php",
            data: {ngoList: ngoList},
            cache: false,
            success: function (data) {
                $("#ngo-ssn-link").html(data["ssn-link"]);
                $("#ngo-logo-link").html(data["logo-link"]);
                $("#ngo-shop-name").html(data["name"]);
                $("#ngo-description").html(data["description"]);
                $("#ngo-operating-hours").html(data["operating-hours"]);
                $("#ngo-email").html(data["email"]);
                $("#ngo-phone-number").html(data["phone-number"]);
                $("#ngo-lot-number").html(data["lot-number"]);
                $("#ngo-street-name").html(data["street-name"]);
                $("#ngo-postcode").html(data["postcode"]);
                $("#ngo-city").html(data["city"]);
                $("#ngo-state").html(data["state"]);
                $("#ngo-country").html(data["country"]);
                $("#ngo-website").html(data["website"]);
                $("#ngo-social-media").html(data["social-media"]);
                $("#ngo-date-joined").html(data["date-joined"]);
                $("#ngo-bank-type").html(data["bank-type"]);
                $("#ngo-bank-account-number").html(data["bank-account-number"]);
                $("#ngo-account-holder-name").html(data["account-holder-name"]);
            }
        });
    }

    function getRepDetails() {
        var repList = $('#repList').val();
        $.ajax({
            type: 'post',
            url: "listMember.php",
            data: {memberId: repList},
            cache: false,
            success: function (data) {
                $("#rep-full-name").html(data["full-name"]);
                $("#rep-email").html(data["email"]);
                $("#rep-phone-no").html(data["phone-number"]);
            }
        });
    }

    $(document).ready(function () {
        getMerchantDetails(); //Call auto_load() function when DOM is Ready
        getNgoDetails();
        getRepDetails();
    });
</script>

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
