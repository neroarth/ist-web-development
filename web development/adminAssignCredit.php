<?php
	session_start();
	ob_start();
	require_once "php-files/usefulFunction.php";
	require_once "php-files/conDb.php";

	if(!isAdminLogin()){
		header("Location: adminLogin");
		exit();
	}

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
	    $complete = true;
        $merchantId = rewrite($_POST['merchantList']);
        $merchantCredit = rewrite($_POST['merchant-credit']);
        $type = "add";

        if ($merchantCredit >= 0 && $merchantCredit < 100000000000 && validateTwoDecimals($merchantCredit)) {

        } else {
            $numAfterDecimalPoints = getNumberAfterDecimalPoint($merchantCredit);
            if ($numAfterDecimalPoints[1] != 0 && $numAfterDecimalPoints[2] != 0) {
                $conn->close();
                $complete = false;
                $creditError = "Please do not enter any amount less than 0";
                exit();
            }
        }

        if($complete){
            $sql = "INSERT INTO merchant_credit (amount, type, merchant_id, admin_id) VALUES (? ,? ,? ,?)";

            if($stmt = $conn->prepare($sql)){

                $stmt->bind_param("dsis", $merchantCredit, $type, $merchantId, $_SESSION['admin-id']);

                $stmt->execute();

                $stmt->close();

                echo '<script type="text/javascript">'
                , 'alert("Credit added successfully");'
                , '</script>';
            }
        }
    }

	ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<link href="images/favicon.png" rel="icon" type="image/png"/>
		<meta name="description" content="ISpendTribute">
		<meta name="author" content="Spending tribute">

		<title>Assign Credits</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
		<script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	h1{
	color:#fff;
	padding-top:50px;
	}
	.register-text{
		color:#9c77b4;
	}
	@media (max-width: 736px) {
	.footer {
		height:18%;
	}

}
 .caption{
	 padding-top: 10px;
 }
.block{
	width:100%;
	height:20%;
	border:30px solid  #f291bb;
	padding-bottom:20px;
}
option{
	border:0;
}
#bank-type {
	width:200px;

}

#auto_load_div>div{width:100%;max-width:320px;}
	</style>
  </head>
  <?php include 'nav.php' ?>
  <body class="body-color"><div class="bodymin">
    <div class="container" style="padding-top:0px;">
        <h1 class="text-center" style="padding-top:20px; padding-bottom:20px;">Assign Credit to a Merchant</h1>
		<div class="form-wrap">
			<h2 style="margin-bottom:0; margin-top:0px;">Merchant Info</h2>
			<form action=""  class="form-horizontal" method="post" enctype="multipart/form-data" id="assignCreditForm">
				<div class="form-group">
					
						<select id="merchantList" name="merchantList" onChange="getMerchantDetails(this);">
							<?php
								$rows = getListOfMerchants($conn,"SELECT id, shop_name FROM merchant WHERE approval_status = ? ", "approved");
								foreach ($rows as &$value) {
									echo '<option value="'.$value['id'].'">'.$value['shop_name'].'</option>';
								}
                                $conn->close();
							?>
						</select>

				</div>
                <table border="1" cellspacing="0" cellpadding="0" class="merchanttable">
					  <tr>
					    <td width="50%" valign="top"><p>Shop name:</p></td>
					    <td width="50%" valign="top"><p id="shop-name"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Description:</p></td>
					    <td width="50%" valign="top"><p id="description"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Operation:</p></td>
					    <td width="50%" valign="top"><p id="operating-hours"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Email:</p></td>
					    <td width="50%" valign="top"><p id="email">&nbsp;</p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Phone Number:</p></td>
					    <td width="50%" valign="top"><p id="phone-number"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Lot Number:</p></td>
					    <td width="50%" valign="top"><p id="lot-number"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Street Name:</p></td>
					    <td width="50%" valign="top"><p id="street-name"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Postcode:</p></td>
					    <td width="50%" valign="top"><p id="postcode"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>City:</p></td>
					    <td width="50%" valign="top"><p id="city"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>State:</p></td>
					    <td width="50%" valign="top"><p id="state"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Country:</p></td>
					    <td width="50%" valign="top"><p id="country"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Website:</p></td>
					    <td width="50%" valign="top"><p id="website"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Social Media:</p></td>
					    <td width="50%" valign="top"><p id="social-media"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Date Joined:</p></td>
					    <td width="50%" valign="top"><p id="date-joined"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Bank Type:</p></td>
					    <td width="50%" valign="top"><p id="bank-type"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Bank Account Number:</p></td>
					    <td width="50%" valign="top"><p id="bank-account-number"></p></td>
				      </tr>
					  <tr>
					    <td width="50%" valign="top"><p>Account Holder Name:</p></td>
					    <td width="50%" valign="top"><p id="account-holder-name"></p></td>
				      </tr>
				  </table>

                <input type="number" step="0.01" min="0"
                       oninvalid="setCustomValidity('Please do not enter any amount less than 0');"
                       oninput="setCustomValidity('')"
                       class="contributecredit" id="merchant-credit" name="merchant-credit">
                <span class = "error"><?php echo $creditError;?></span>
				
				<input id = "assign" name="assign" type="submit" class="btn btn-default btn-min-register" value="Assign Credit" />
			</form>
	   </div>
    </div><!-- /.container -->
	<div class="block"></div></div>
	<?php include 'foot.php' ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	
	<script type="text/javascript">
		function getMerchantDetails(){
			var merchantList = $('#merchantList').val();
			$.ajax({
				type:'post',
				url: "listMerchant.php",
				data : { merchantList: merchantList },
				cache: false,
				success: function(data){
				   $("#shop-name").html(data["shop-name"]);
				   $("#description").html(data["description"]);
				   $("#operating-hours").html(data["operating-hours"]);
				   $("#email").html(data["email"]);
				   $("#phone-number").html(data["phone-number"]);
				   $("#lot-number").html(data["lot-number"]);
				   $("#street-name").html(data["street-name"]);
				   $("#postcode").html(data["postcode"]);
				   $("#city").html(data["city"]);
				   $("#state").html(data["state"]);
				   $("#country").html(data["country"]);
				   $("#website").html(data["website"]);
				   $("#social-media").html(data["social-media"]);
				   $("#date-joined").html(data["date-joined"]);
				   $("#bank-type").html(data["bank-type"]);
				   $("#bank-account-number").html(data["bank-account-number"]);
				   $("#account-holder-name").html(data["account-holder-name"]);
				}
			  });
		}
		
		$(document).ready(function(){
			getMerchantDetails(); //Call auto_load() function when DOM is Ready
		});
	</script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
