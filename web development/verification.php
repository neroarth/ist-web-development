<?php
	session_start();
	ob_start();
	require_once "php-files/usefulFunction.php";
	require_once "php-files/conDb.php";
	
	if(!isLogin()){
		header("Location: login.php");
		exit();
	}
	
	$sql = "SELECT verification_status, email, email_verification_code FROM member WHERE id = ?";

	if ($stmt = $conn->prepare($sql)) {

		/* bind parameters for the ? variable */
		$stmt->bind_param("i", $_SESSION['id']);

		/* execute query */
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result($isVerified, $rsEmail, $rsVerificationCode);

		/* fetch value */
		$stmt->fetch();

		/* close statement */
		$stmt->close();
	}
		
	if($isVerified == 1){
		header("Location: profile.php");
		exit();
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (isset($_POST['resendCode'])) {
		    $to = $rsEmail;
			$subject = "Your ISpendTribute Account Verification Code";
			$htmlContent ="Hi, Thank You for registering with us!
							<br/>Below is your account verification code
							<br/>Verification Code: "."$rsVerificationCode".
							"<br/>Please go to this <a href = \"http://ispendtribute.com/login.php\">link</a> to enter your verification code<br/>";
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;  charset=iso-8859-1" . "\r\n";
			$headers .= 'From:ISpendTribute<willteh.ist@gmail.com>' . "\r\n";
			//header("Location: login.php");
			// Send email
			if(mail($to,$subject,$htmlContent,$headers)):
				$emailMsg=  "The verification code has been sent to $rsEmail successfully!";
			else:
				$emailMsg = 'Failed to send email, please click the button again. Sorry.';
			endif;
		}
		else{
			$verificationCode = rewrite($_POST["verification-code"]);
			$verificationCode = stripAllWhiteSpaces($verificationCode);
			
			if(strcmp($verificationCode,$rsVerificationCode) != 0){
				$verificationCodeError = "Wrong verification code! Please try again";
			}
			else{
				$stmt = $conn->prepare("UPDATE member SET verification_status = ? WHERE id = ?");
				$verificationStatus = 1;
				$stmt->bind_param("ii", $verificationStatus, $_SESSION['id']);
	
				$stmt->execute();
				$stmt->close();
				
				header("Location: profile.php");
			}
		}
	}
	
	ob_end_clean();
	$conn->close();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<link href="images/favicon.png" rel="icon" type="image/png"/>
	<meta name="description" content="ISpendTribute">
	<meta name="author" content="Spending tribute">

	<title>Account Verification</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
	<script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	h1{
	color:#fff;
	padding-top:50px;
	}
	.register-text{
		color:#9c77b4;
	}
	@media (max-width: 736px) {
	.footer {
		height:18%;
	}

}
 .caption{
	 padding-top: 10px;
 }
.block{
	width:100%;
	height:20%;
	border:30px solid  #f291bb;
	padding-bottom:20px;
}
	</style>
  </head>
  <?php include 'nav.php' ?>
  <body class="body-color">
    <div class="container cancelpaddingtop">
        <h1 class="paddingtop25">Verify Your Account</h1>
        <form id="verificationForm" class="form-horizontal" role="form" method="post" action="" >
            <div class="form-group">
                <div class="col-sm-4">
				  <span>Your verification code is sent to this email <?php echo $rsEmail;?> (Please check your junk mail if not found in inbox)</span>
                  <input type="text" class="form-control form-controls text-line" id="verification-code" name ="verification-code" placeholder="Enter your verification code">
                  <span class = "error"><?php echo $verificationCodeError;?></span>
				  <span class = "error"><?php echo $emailMsg;?></span>
                </div>
            </div>

            <div class="form-group caption">
				<div class="col-sm-4  register-text width20">
					<button type="submit" class="btn btn-default btn-min-register">Verify</button>
				</div>
				
				<div class="col-sm-4  register-text">
					<button type="submit" class="btn btn-default btn-min-register" id="resendCode" name="resendCode">Resend Code</button>
				</div>
            </div>
        </form>
    </div><!-- /.container -->
	<div class="block"></div>
	<?php include 'foot.php' ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
