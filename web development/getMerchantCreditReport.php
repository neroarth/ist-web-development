<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";

		$perpage = $_POST['perpage'];

        if(isset($_POST["currentPage"])){
            $page = intval($_POST["currentPage"]);
        }
        else {
            $page = 1;
        }

        //last row of record
        $end = $perpage * $page;
        //first row of record
        $offset = $end - $perpage;
        $no = $offset + 1;

        $rows = getMerchantCreditReport($conn, $offset, $perpage);

        if($rows == null){
            echo '<tr>
                       <td>Sorry, no records found</td>
                  </tr>';
        }else{
            $tableData = "";
            foreach ($rows as &$value) {
                $addTypeRows = getAddTypeCredit($conn,$value['id']);
                $subTypeRows = getSubTypeCredit($conn,$value['id']);
                $addTypeValue = 0;
                $subTypeValue = 0;
                foreach ($addTypeRows as &$addTypeRowValue) {
                    if($addTypeRowValue['add-type'] == null){
                        $addTypeValue = 0;
                    }else{
                        $addTypeValue = $addTypeRowValue['add-type'];
                    }
                }

                foreach ($subTypeRows as &$subTypeRowValue) {
                    if($subTypeRowValue['sub-type'] == null){
                        $subTypeValue = 0;
                    }else{
                        $subTypeValue = $subTypeRowValue['sub-type'];
                    }
                }

                $currentCreditValue = $addTypeValue - $subTypeValue;

                $tableData .= '<tr>';
                $tableData .= '<td>' . $no . '</td>';
                $tableData .= '<td>' . $value['id'] . '</td>';
                $tableData .= '<td>' . $value['shop-name'] . '</td>';
                $tableData .= '<td>' . $addTypeValue . '</td>';
                $tableData .= '<td>' . $subTypeValue . '</td>';
                if($currentCreditValue <= 100){
                    $tableData .= '<td style="color:#de0c07;">' . $currentCreditValue . '</td>';
                }else if($currentCreditValue > 100 && $currentCreditValue <= 500){
                    $tableData .= '<td style="color:#ffd233;">' . $currentCreditValue . '</td>';
                }else{
                    $tableData .= '<td style="color:#2ab574;">' . $currentCreditValue . '</td>';
                }
                $tableData .= '</tr>';

                $no++;
            }
            echo $tableData;
        }

		$conn->close();
	}
?>