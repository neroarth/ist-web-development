<?php
session_start();
ob_start();
require_once "php-files/usefulFunction.php";
require_once "php-files/conDb.php";
if (!isCashierLogin()) {
    header("Location: login.php");
    $conn->close();
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require_once "php-files/usefulFunction.php";
    require_once "php-files/conDb.php";

    $complete = true;

    $amountSpent = rewrite($_POST['amount-spent']);

    $receiptNumber = rewrite($_POST["receipt-number"]);
    $receiptNumber = stripAllWhiteSpaces($receiptNumber);

    $receiptRemark = rewrite($_POST["receipt-remark"]);
    $receiptRemark = stripSpaces($receiptRemark);

    $memberPhoneNo = rewrite($_POST["member-phone-no"]);
    $memberPhoneNo = stripAllWhiteSpaces($memberPhoneNo);

    $memberPinNo = rewrite($_POST["member-pin-no"]);
    $memberPinNo = stripAllWhiteSpaces($memberPinNo);

    if ($amountSpent >= 0 && $amountSpent < 100000000000 && validateTwoDecimals($amountSpent)) {

    } else {
        $numAfterDecimalPoints = getNumberAfterDecimalPoint($amountSpent);
        if ($numAfterDecimalPoints[1] != 0 && $numAfterDecimalPoints[2] != 0) {
//            $amountSpentError = "Please do not enter amount that is less than 0";
//            $complete = false;
            $conn->close();
            header("Location: enterReceipt.php?amount-spent-error=Please do not enter amount that is less than 0&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
            exit();
        }
    }

    if (strlen($receiptNumber) > 100) {
//        $receiptNumberError = "Length of receipt number can't be more than 100 characters";
//        $complete = false;
        $conn->close();
        header("Location: enterReceipt.php?receipt-number-error=Length of receipt number cannot be more than 100 characters&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
        exit();
    }

    if (strlen($receiptRemark) > 300) {
//        $receiptRemarkError = "Length of remark can't be more than 300 characters";
//        $complete = false;
        $conn->close();
        header("Location: enterReceipt.php?receipt-remark-error=Length of remark cannot be more than 300 characters&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
        exit();
    }

    $sql = "SELECT id, full_name, email  FROM member WHERE phone_no = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $memberPhoneNo);

        $stmt->execute();

        $stmt->bind_result($memberId, $memberName, $memberEmail);

        $stmt->fetch();

        $stmt->close();
    }


    if ($memberName == null) {
        $conn->close();
        header("Location: enterReceipt.php?msg=not-exist&amount-spent=$amountSpent&receipt-number=$receiptNumber&receipt-remark=$receiptRemark");
        exit();
    }

    /*****************GET MEMBER PIN NUMBER********************/
    $sql = "SELECT  pin_no FROM member WHERE id = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $memberId);

        $stmt->execute();

        $stmt->bind_result($rsMemberPinNo);

        $stmt->fetch();

        $stmt->close();

    }

    /*****************GET MERCHANT ID & PIN NUMBER FROM CASHIER********************/
    $sql = "SELECT  merchant_id, pin_no FROM cashier WHERE username = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("s", $_SESSION['cashier-id']);

        $stmt->execute();

        $stmt->bind_result($merchantId,$rsPinNo);

        $stmt->fetch();

        $stmt->close();
    }

    /*****************GET CASHBACK RATE FROM MERCHANT BASED ON THE ID GET PREVIOUSLY********************/
    $sql = "SELECT rate FROM `cashback_rate` WHERE merchant_id = ? ORDER BY id DESC LIMIT 1";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $merchantId);

        $stmt->execute();

        $stmt->bind_result($cashbackRate);

        $stmt->fetch();

        $stmt->close();
    }

    /*****************CALCULATING CASHBACK AMOUNTS************************/
    $totalAmount = $amountSpent * ($cashbackRate / 100);

    if ($_POST['pay-with-credit'] == 'pay-with-credit'){
        if(!strcmp($memberPinNo, $rsMemberPinNo) == 0){
            $conn->close();
            header("Location: enterReceipt.php?msg=wrong-member-pin&amount-spent=$amountSpent&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
            exit();
        }else{
            if(!checkEnoughCredit($conn,"member",$memberId,$amountSpent)){
                $conn->close();
                header("Location: enterReceipt.php?credit-error=not enough spend credit&amount-spent=$amountSpent&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
                exit();
            }
        }
    }

    $isPayWithCredit = 0;
    if(strcmp($_POST['pay-with-credit-confirm'], "checked") == 0){
        $memberPinNoConfirm = rewrite($_POST["member-pin-no-confirm"]);
        $memberPinNoConfirm = stripAllWhiteSpaces($memberPinNoConfirm);
        if(!strcmp($memberPinNoConfirm, $rsMemberPinNo) == 0){
            $conn->close();
            header("Location: enterReceipt.php?msg=wrong-member-pin&amount-spent=$amountSpent&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
            exit();
        }else{
            if(checkEnoughCredit($conn,"member",$memberId,$amountSpent)){
                $previousCashbackAmounts = getMemberCashbackAmount($memberId, $conn);

                $currentCashbackAmount = ($previousCashbackAmounts['current-cashback-amount'] - $amountSpent);

                /**************************UPDATE MEMBER CURRENT CASHBACK AMOUNT*****************************/
                $sql = 'UPDATE member SET current_cashback_amount = ? WHERE id = ?';

                if ($stmt = $conn->prepare($sql)) {

                    $stmt->bind_param("di", $currentCashbackAmount, $memberId);

                    $stmt->execute();

                    $stmt->close();
                }
                $isPayWithCredit = 1;
            }else{
                $conn->close();
                header("Location: enterReceipt.php?credit-error=not enough spend credit&amount-spent=$amountSpent&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
                exit();
            }
        }
    }

    if (isset($_POST['pinConfirmBtn'])) {

        $pinNo = $_POST['pin-no'];

        if($pinNo != null && !strcmp($pinNo, $rsPinNo) == 0){
            $conn->close();
            header("Location: enterReceipt.php?msg=wrong-pin&amount-spent=$amountSpent&receipt-number=$receiptNumber&receipt-remark=$receiptRemark&member-phone-no=$memberPhoneNo");
            exit();
        }

        /**************************THEN INSERT DATA INTO RECEIPT TABLE******************************/
        $sql = "INSERT INTO receipt (receipt_number, receipt_remark, amount_spent, cashback_amount, pay_with_credit, member_id, merchant_id, cashier_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("ssddiiis", $receiptNumber, $receiptRemark, $amountSpent, $totalAmount, $isPayWithCredit, $memberId, $merchantId, $_SESSION['cashier-id']);

            $stmt->execute();

            $receiptId = $stmt->insert_id;

            $stmt->close();

            $conn->close();
            header("Location: enterReceipt.php?msg=success");
        }
    }

}


ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link href="images/favicon.png" rel="icon" type="image/png"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">

    <title>Receipt Confirmation</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php include 'nav.php' ?>
<body class="body-color">
<div class="minpink">
    <div class="container cancelpaddingtop">
        <h1 class="paddingtop25 p15 g1" style="color:white;">Receipt & Member Details Confirmation</h1>
        <div class="row">
            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="receiptForm">
                <div class="col-md-9 nop">
                    <div class="form-group">
                        <div class="col-md-3 w2">
                            <span class="form-controls" id="basic-addon1">Amount Spent: </span>
                            <input type="number" step="0.01" min="0"
                                   oninvalid="setCustomValidity('Please do not enter any amount less than 0');"
                                   class="form-controls line1 white mustbewhite" oninput="setCustomValidity('');"
                                   id="amount-spent" name="amount-spent" placeholder="Amount Spent"
                                   value="<?php echo $amountSpent; ?>" readonly required>
                        </div>
                    </div>
                </div>

                <div class="col-md-9 nop">
                    <div class="form-group">
                        <div class="col-md-3 w2">
                            <span class="form-controls" id="basic-addon1">Cashback Amount: </span>
                            <input type="number"
                                   class="form-controls line1 white mustbewhite"
                                   value="<?php echo $totalAmount; ?>" readonly>
                        </div>
                    </div>
                </div>

                <div class="form-group c1">
                    <div class="col-sm-4 w2">
                        <span class="form-controls" id="basic-addon1">Receipt Number: </span>
                        <input type="text" class="form-controls  white mustbewhite" id="receipt-number"
                               name="receipt-number" placeholder="Enter the receipt's number"
                               value="<?php echo $receiptNumber; ?>" readonly>
                    </div>
                </div>

                <div class="form-group c1">
                    <div class="col-sm-4 w2">
                        <span class="form-controls" id="basic-addon1">Receipt's Remark :</span>
                        <input type="text" class="form-controls mustbewhite width100" id="receipt-remark"
                               name="receipt-remark" placeholder="Enter the receipt's remark"
                               value="<?php echo $receiptRemark; ?>" readonly>
                    </div>
                </div>

                <input type="text" class="form-controls mustbewhite" id="pay-with-credit-confirm"
                       name="pay-with-credit-confirm"
                       value="<?php if($_POST['pay-with-credit'] == 'pay-with-credit'){ echo 'checked';} ?>" readonly hidden>

                <input type="text" class="form-controls mustbewhite" id="member-pin-no-confirm"
                       name="member-pin-no-confirm"
                       value="<?php echo $_POST['member-pin-no']; ?>" readonly hidden>

                <div class="form-group c1">
                    <div class="col-sm-4 w2">
                        <span class="form-controls" id="basic-addon1">Member's Phone Number: </span>
                        <input type="text" class=" form-controls white mustbewhite" id="member-phone-no" name="member-phone-no"
                               placeholder="Member's Phone Number"
                               value="<?php echo $memberPhoneNo; ?>" readonly required>
                    </div>
                </div>

                <div class="form-group c1">
                    <div class="col-sm-4 w2">
                        <span class="form-controls" id="basic-addon1">Member's Name  :</span>
                        <input type="text" class="form-controls white mustbewhite width100" id="member-name" name="member-name"
                               placeholder="Member's Name"
                               value="<?php echo $memberName; ?>" readonly>
                    </div>
                </div>

                <div class="form-group c1">
                    <div class="col-sm-4 w2">
                        <span class="form-controls" id="basic-addon1">Member's Email :</span>
                        <input type="text" class="form-controls white mustbewhite width100" id="member-email" name="member-email"
                               placeholder="Member's Email"
                               value="<?php echo $memberEmail; ?>" readonly>
                    </div>
                </div>

                <button name="receiptConfirmBtn" id="receiptConfirmBtn" class="btn btn-default btn-min-register"
                        data-toggle="modal" data-target="#pinConfirmation">Confirm</button>
                <span class="error"><?php echo $_GET["pin-no-error"]; ?></span>





                    <form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="approveForm">
                      
                        <div class="modal fade" id="pinConfirmation" tabindex="-1" role="dialog" aria-labelledby="title" style="border-radius:0 !important;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="title">Please enter your 6-digit pin number</h4>
                                    </div>
                                    <div class="modal-content"  style="border-radius:0 !important;"><div class="modal-body"><p class="security-p">Security</p>
                                        <input type="number" min="100000" max = "999999" style="border-bottom: 1px solid #9c77b4; color:#9c77b4 !important;"
                                               oninvalid="setCustomValidity('Please do not enter a valid 6-digit pin number');"
                                               class="form-control form-controls text-line line1 pline" oninput="setCustomValidity('');"
                                               id="pin-no" name="pin-no" placeholder="Pin Number"
                                               required>
                                      
                        <p><button type="submit" name="pinConfirmBtn" id="pinConfirmBtn"
                                                class="btn btn-default btn-min-register confirm2-overwrite">Confirm</button></p>               
                                          
                                   </div></form> </div>
                                  
                                </div>
                            </div></div>
                        </div>
    </div><!-- /.container -->
    <div class="block"></div>
</div>
<?php include 'foot.php' ?>

<!--The progress bar section-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
<style>
textarea.purple-line{
	color:#9c77b4 !important;
	
}
.purple-line::placeholder{
	color:#9c77b4 !important;
	
	}
::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color:#9c77b4 !important;
}
::-moz-placeholder { /* Firefox 19+ */
  color:#9c77b4 !important;
}
:-ms-input-placeholder { /* IE 10+ */
  color:#9c77b4 !important;
}
:-moz-placeholder { /* Firefox 18- */
  color:#9c77b4 !important;
}
.modal{
	top:70px !important;}
.modal.in .modal-dialog{
	    background-color: #9f76b4;
    color: white;
}

</style>
</body>
</html>
