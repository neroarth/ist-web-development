<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		$id = $_POST['merchantList'];
		
		$sql = "SELECT  ssn_link, logo_link, shop_name, description, operating_hours,
						email, phone_number, lot_number,
						street_name, postcode, city,
						state, country, website,
						social_media, date_joined + Interval 13 Hour 'date_joined', bank_type,
						bank_account_number, account_holder_name 
						FROM merchant WHERE id = ?";
		
		if ($stmt = $conn->prepare($sql)) {
	
		  $stmt->bind_param("i", $id);
	
		  $stmt->execute();
	
		  $stmt->bind_result($ssnLink, $logoLink, $shopName,$description, $operatingHours,
							 $email, $phoneNumber, $lotNumber,
							 $streetName, $postcode, $city,
							 $state, $country, $website,
							 $socialMedia, $dateJoined, $bankType,
							 $bankAccountNumber, $accountHolderName);
	
		  $stmt->fetch();
	
		  $stmt->close();
		}
		
		$merchantDetails = array(
			'id' => $id,
            'ssn-link' => $ssnLink,
            'logo-link' => $logoLink,
			'shop-name' => $shopName,
			'description' => $description,
			'operating-hours' => $operatingHours,
			'email' => $email,
			'phone-number' => $phoneNumber,
			'lot-number' => $lotNumber,
			'street-name' => $streetName,
			'postcode' => $postcode,
			'city' => $city,
			'state' => $state,
			'country' => $country,
			'website' => $website,
			'social-media' => $socialMedia,
			'date-joined' => $dateJoined,
			'bank-type' => $bankType,
			'bank-account-number' => $bankAccountNumber,
			'account-holder-name' => $accountHolderName
		);
		header('Content-Type: application/json');
	    echo json_encode($merchantDetails);
		
		$conn->close();
	}
?>