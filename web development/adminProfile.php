<?php
session_start();
require_once "php-files/usefulFunction.php";
if (isAdminLogin()) {
    require_once "php-files/conDb.php";
    $adminId = $_SESSION["admin-id"];

    $sql = "SELECT name, level 
            FROM admin 
            WHERE username = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("s", $adminId);

        $stmt->execute();

        $stmt->bind_result($rsName, $rsLevel);

        $stmt->fetch();

        $stmt->close();
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["change-pin-no-btn"])) {
            $newPinNo = rewrite($_POST['new-pin-no']);
            if (isCorrectPinFormat($newPinNo)) {
                if ($stmt = $conn->prepare("UPDATE admin SET pin_no = ? WHERE username = ?")) {
                    $stmt->bind_param("is", $newPinNo, $adminId);
                    $stmt->execute();
                    $stmt->close();
                    echo '<script type="text/javascript">'
                    , 'alert("New pin number updated successfully.");'
                    , '</script>';
                }
            } else {
                echo '<script type="text/javascript">'
                , 'alert("Please create a pin number with 6 number digits");'
                , '</script>';
            }
        }
    }
} else {
    header("Location: adminLogin");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <title>MyAdmin</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="main-style.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!-- [if lt IE 9]> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="assets/js/ie8-responsive-file-warning.js"></script>
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale = 1.0,
maximum-scale=1.0, user-scalable=no"/>
</head>
<style>
    .footer {
        border-top: 1px solid #d3d3d3;
    }

    .modal-header {
        background: #9c77b4;
        color: #fff;
    }

    select {
        border: 0;
        outline: 0;

    }

</style>

<body>
<?php include 'navProfile.php'; ?>
<div class="container overwrite-container">
    <div class="row">
        <div class="col-lg-3 col-md-4 centerise">
            <div class="text-center marginleftminus cancelmarginleft2">

                <h4><?php echo $rsName; ?></h4>
                <div class="text-center">
                    <p class="text-muted">Access Level:
                        <?php echo $rsLevel; ?>
                    </p>
                </div>
            </div>
        </div>
        <!-- end col -->
        <div class="col-md-9 col-lg-5 text-center m10 cancelmarginleft2 addmleft">
            <div class="col-md-4 col-lg-4 col-md-push-2 col-xs-4 col-xs-push-1 text-center width-size cashback-size over-write-purple">
                <a href="charity.php">
                    <button class="btn purple select buttomwidth w3 same1" data-target="#cashback">Get Cashback
                    </button>
                </a>
                <div class="modal fade" id="cashback" tabindex="-1" role="dialog" aria-labelledby="title">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a href="charity.php">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                </a>
                                <h4 class="modal-title" id="title">Increase donation rate from cashback</h4>
                            </div>
                            <div class="modal-body">
                                <p>Coming soon</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="receiptDetailModal" tabindex="-1" role="dialog" aria-labelledby="title">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="title reg1">Receipt Detail</h4>
                            </div>
                            <div class="modal-body" id="receiptDetailDiv" name="receiptDetailDiv">

                            </div>
                        </div>
                    </div>
                </div>
                <p style="font-size:1.5em;" class="select normallineheight minheight sc-text"> Management Fee </p>
                <div class="group-value">
                    <span class="text-price-two select resizeRM cash-text sc" id="management-fee-result"></span>
                    <!-- <button type="button" class="btn btn-circle editcircle cash-text ca-button ex0" data-toggle="modal"
                             data-target="#contribution">
                         <span class="glyphicon glyphicon-plus ca-button ex1"></span>
                     </button>-->
                </div>
            </div>
            <div class="verticalLine col-md-2 col-xs-1 col-xs-push-2 specialline overwrite-vertical"></div>
            <div class="col-md-4 col-md-push-3 col-xs-4 col-xs-push-3 cashback-size sm-margin overwrite-sm">
                <button class="btn pinkthree select buttomwidth pink-overwrite w3 same1" data-toggle="modal"
                        data-target="#contribute">
                    Contribute
                </button>
                <div class="modal fade" id="contribute" tabindex="-1" role="dialog" aria-labelledby="title">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="title">Modal title</h4>
                            </div>
                            <div class="modal-body">
                                coming soon
                            </div>
                        </div>
                    </div>
                </div>
                <p style="font-size:1.5em;" class="select normallineheight minheight"> Contribution Account</p>
                <div class="w100">
                    <span class="text-price text-price-three select resizeRM pinkfont mm ca overwrite-cc">CC <?php echo $rsCurrentCharityAmount; ?></span>
                    <!--   <button type="button" class="btn btn-circle editcircle overwrite-cc-button ex0" data-toggle="modal"
                              data-target="#contribution">
                          <span class="glyphicon glyphicon-plus ex1"></span>
                      </button>--></div>

                <!-- Modal -->
                <div class="modal fade" id="contribution" tabindex="-1" role="dialog"
                     aria-labelledby="contributionLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="contributionLabel">Contribution</h4>
                            </div>
                            <div class="modal-body">
                                Coming soon
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again1 v1 rf1"
                 style="z-index: 1;">
                <a href="adminMerchantDetails" class="btn purple-merchant select purplemerchantadmin reg1">Approve Role</a>
            </div>


            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again3 v2admin  rf2"
                 style="z-index: 0;">
                <a href="adminAssignConnect" class="btn purple-merchant select purplemerchantadmin mobilengo reg1">Assign
                    Connect to Merchant</a>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again1 v3admin rf3"
                 style="z-index: 1;">
                <a href="adminViewReferral" class="btn purple-merchant select purplemerchantadmin reg1">View Referral
                    List</a>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again3 v4admin rf4"
                 style="z-index: 0;">
                <a href="adminAssignCredit" class="btn purple-merchant select purplemerchantadmin mobilengo reg1">Assign
                    Credit to Merchant</a>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-login overwrite-again4 v5admin rf5">
                <button class="btn purple select purplemerchantadmin reg1" data-toggle="modal"
                        data-target="#changePinModal">Change PIN Number
                </button>
            </div>

            <div class="modal fade" id="changePinModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Change your pin number</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data"
                                  id="change-pin-form">
                                <input type="password" class="form-controls" id="new-pin-no"
                                       name="new-pin-no" placeholder="Enter new pin number">
                                <input id="change-pin-no-btn" name="change-pin-no-btn" type="submit"
                                       class="btn btn-default btn-min-register reg1"
                                       value="Submit"
                                       style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-8 col-lg-9 col-xs-12 divsize d40 tf1">
            <h1 class="trans">Merchant's Transaction History</h1>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:#2ab574;" class="select"> &#x25cf; Approved</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p class="select"> &#x25cf; Pending</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:red" class="select"> &#x25cf; Rejected</p>
            </div>
            <div class="sortby sortbyadmin">
                <div style="display:inline-block;">
                    <p class="select admin-text">Sort by</p>
                </div>
                <div class="col-md-5 col-lg-2 col-md-push-8  col-xs-3 col-xs-push-9 latest" name="sort-by-cb"
                     id="sort-by-cb">
                    <select>
                        <option>Newest</option>
                        <option>Oldest</option>
                    </select>
                </div>

                <div class="col-md-5 col-lg-2 col-md-push-8  col-xs-3 col-xs-push-9">
                    <input type="date" name="reportDate" id="reportDate">
                    <select name="merchant-id-cb"
                            id="merchant-id-cb">
                        <?php
                        $rows = getListOfMerchants($conn, "SELECT id, shop_name FROM merchant WHERE approval_status = ?", "approved");
                        foreach ($rows as &$value) {
                            if (strcmp($_GET['merchantId'], $value['id']) == 0) {
                                echo '<option value="' . $value['id'] . '" selected>' . $value['shop_name'] . '</option>';
                            } else {
                                echo '<option value="' . $value['id'] . '">' . $value['shop_name'] . '</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <!--            start transaction history-->
            <div>
                <div>
                    <div>
                        <form action="" class="form-horizontal" method="post" enctype="multipart/form-data"
                              id="approveForm">

                            <div class="modal fade" id="pinConfirmation" tabindex="-1" role="dialog"
                                 aria-labelledby="title"
                                 style="border-radius:0 !important;">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="title">Please enter your 6-digit pin number</h4>
                                    </div>
                                    <div class="modal-content" style="border-radius:0 !important;">
                                        <div class="modal-body"><p class="security-p">Security</p>
                                            <input type="number" min="100000" max="999999"
                                                   style="border-bottom: 1px solid #9c77b4; color:#9c77b4 !important;"
                                                   oninvalid="setCustomValidity('Please do not enter a valid 6-digit pin number');"
                                                   class="form-control form-controls text-line line1 pline"
                                                   oninput="setCustomValidity('');"
                                                   id="pin-no" name="pin-no" placeholder="Pin Number"
                                                   required>
                                            <p>
                                                <button type="submit" name="pinConfirmBtn" id="pinConfirmBtn"
                                                        class="btn btn-default btn-min-register confirm2-overwrite">
                                                    Confirm
                                                </button>
                                            </p>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <table class="table">
                                <tbody id="search-results">

                                </tbody>
                            </table>
                        </form>
                    </div>


                    <table class="table" id="page-table" name="page-table">
                        <tr>
                            <td id="page-results">

                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--            end transaction history-->

            <!--            start credit history-->
            <div>
                <div>
                    <div>
                        <h1 class="trans">All Merchant's Credit History</h1>
                        <form action="" class="form-horizontal" method="post" enctype="multipart/form-data"
                              id="merchantCreditForm">
                            <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Merchant ID</th>
                                    <th>Shop Name</th>
                                    <th>Credit Given</th>
                                    <th>Credit Used</th>
                                    <th>Current Credit</th>
                                </tr>
                                </thead>
                                <tbody id="merchant-credit-results">

                                </tbody>
                            </table>
                            </div>
                        </form>
                    </div>

                    <table class="table" id="credit-page-table" name="credit-page-table">
                        <tr>
                            <td id="credit-page-results">
                                <?php
                                $conn->close();
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- end credit history-->
        </div>
    </div>
</div>
<!--Began another section -->
<script src="dist/js/bootstrap.min.js"></script>
<script type="text/javascript">

    Date.prototype.toDateInputValue = (function () {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0, 10);
    });

    jQuery(document).ready(function () {
        if (getUrlParameter('date') != null) {
            document.getElementById("reportDate").defaultValue = getUrlParameter('date');
            //yyyy-MM-dd
//        document.getElementById("reportDate").defaultValue = "2014-02-09";
        } else {
            $('#reportDate').val(new Date().toDateInputValue());
        }

        getMerchantReport();
        getPageResults();
        getManagementFee();
        getMerchantCreditReport();
        getCreditPageResults();
        $("page-table").hide();
        $("credit-page-table").hide();
    });

    $("#sort-by-cb").change(function () {
        getMerchantReport();
        getPageResults();
        getManagementFee();
        return false;
    });

    $("#merchant-id-cb").change(function () {
        getMerchantReport();
        getPageResults();
        getManagementFee();
        return false;
    });

    $("#reportDate").change(function () {
        getMerchantReport();
        getPageResults();
        getManagementFee();
        return false;
    });

    function showReceiptDetails(receiptId) {
        var where1 = "full";
        $.post('getCashbackReport', {
            receiptId: receiptId,
            where1: where1
        }, function (data) {
            $("#receiptDetailDiv").html(data);
        });

        $('#receiptDetailModal').modal('show');
    }

    <?php //from here https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js ?>
    var getUrlParameter = function getUrlParameter(param) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === param) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var getMerchantReport = function getMerchantReport() {
        var sortBy = $('#sort-by-cb').find(":selected").text();
        var perpage = 10;
        var currentPage = null;
        var where1 = "merchant";
        //get id instead of text
        var merchantId = $('#merchant-id-cb').find(":selected").val();

        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (getUrlParameter('page') !== null) {
            currentPage = getUrlParameter('page');
        } else {
            currentPage = 1;
        }

        $.post('getCashbackReport', {
            sortBy: sortBy,
            perpage: perpage,
            currentPage: currentPage,
            where1: where1,
            day: day,
            month: month,
            year: year,
            merchantId: merchantId
        }, function (data) {
            $("#search-results").html(data);
        });

        $("page-table").show();
    };

    var getPageResults = function getPageResults() {
        var sortBy = $('#sort-by-cb').find(":selected").text();
        var perpage = 10;
        var currentPage = null;
        var where1 = "merchant";
        //get id instead of text
        var merchantId = $('#merchant-id-cb').find(":selected").val();

        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (getUrlParameter('page') !== null) {
            currentPage = getUrlParameter('page');
        }

        $.post('getPageResults', {
            sortBy: sortBy,
            perpage: perpage,
            currentPage: currentPage,
            where1: where1,
            day: day,
            month: month,
            year: year,
            merchantId: merchantId
        }, function (data) {
            $("#page-results").html(data);
        });
    };

    var getMerchantCreditReport = function getMerchantCreditReport() {
        var perpage = 10;
        var currentPage = null;
        //get id instead of text
        var merchantId = $('#merchant-id-cb').find(":selected").val();

        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (getUrlParameter('creditPage') !== null) {
            currentPage = getUrlParameter('creditPage');
        } else {
            currentPage = 1;
        }

        $.post('getMerchantCreditReport', {
            perpage: perpage,
            currentPage: currentPage
        }, function (data) {
            $("#merchant-credit-results").html(data);
        });

        $("credit-page-table").show();
    };

    var getCreditPageResults = function getCreditPageResults() {
        var perpage = 10;
        var currentPage = null;

        if (getUrlParameter('creditPage') !== null) {
            currentPage = getUrlParameter('creditPage');
        }

        $.post('getCreditPageResults', {
            perpage: perpage,
            currentPage: currentPage
        }, function (data) {
            $("#credit-page-results").html(data);
        });
    };

    var getManagementFee = function getManagementFee() {
        var merchantId = $('#merchant-id-cb').find(":selected").val();
        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        $.post('getDailyManagementFee', {
            day: day,
            month: month,
            year: year,
            merchantId: merchantId
        }, function (data) {
            $("#management-fee-result").html(data);
        });
    };
</script>
<?php include 'foot.php'; ?>
</body>

</html>
