<?php
function rewrite($info)
{
    $info = stripslashes($info);
    $info = trim($info);
    $info = htmlspecialchars($info, ENT_QUOTES, 'ISO-8859-1', true);
    return $info;
}

function stripSpaces($info)
{
    return preg_replace('/\s+/', ' ', $info);
}

function stripAllWhiteSpaces($info)
{
    return preg_replace('/\s+/', '', $info);
}

function logout()
{
    session_unset();
    session_destroy();
    header("Location: ../index.php");
}

function isLogin()
{
    if (isset($_SESSION['id']) && $_SESSION['id'] != null && $_SESSION['id'] > 1000) {
        return true;
    } else {
        return false;
    }
}

function isAdminLogin()
{
    if (isset($_SESSION['admin-id']) && $_SESSION['admin-id'] != null) {
        return true;
    } else {
        return false;
    }
}

function isMerchantLogin()
{
    if (isset($_SESSION['merchant-id']) && $_SESSION['merchant-id'] != null) {
        return true;
    } else {
        return false;
    }
}

function isCashierLogin()
{
    if (isset($_SESSION['cashier-id']) && $_SESSION['cashier-id'] != null) {
        return true;
    } else {
        return false;
    }
}

function isNgoLogin(){
    if(isset($_SESSION["ngo-id"]) && $_SESSION["ngo-id"] != null){
        return true;
    }else{
        return false;
    }
}

function getListOfBanks($conn)
{
    $bankSql = "SELECT name FROM bank";

    if ($bankStmt = $conn->prepare($bankSql)) {
        /* execute query */
        $bankStmt->execute();

        $bankStmt->bind_result($bankList);

        $rows = array();

        while ($bankStmt->fetch()) {
            $row = array(
                'name' => $bankList
            );
            $rows[] = $row;
        }

        $bankStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getListOfCategories($conn)
{
    $categorySql = "SELECT name FROM category";

    if ($categoryStmt = $conn->prepare($categorySql)) {
        /* execute query */
        $categoryStmt->execute();

        $categoryStmt->bind_result($categoryList);

        $rows = array();

        while ($categoryStmt->fetch()) {
            $row = array(
                'name' => $categoryList
            );
            $rows[] = $row;
        }

        $categoryStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function checkEnoughCredit($conn, $table, $id, $amountSpent){
    //this is to validate whether the user is login to a merchant that he registered, not other user's merchant
    $checkEnoughCreditSql = "SELECT current_cashback_amount   
                             FROM $table   
                             WHERE id = ?";

    $isEnough = false;

    if ($checkEnoughCreditStmt = $conn->prepare($checkEnoughCreditSql)) {

        $checkEnoughCreditStmt->bind_param("i", $id);

        /* execute query */
        $checkEnoughCreditStmt->execute();

        $checkEnoughCreditStmt->bind_result($currentCashbackAmount);

        while ($checkEnoughCreditStmt->fetch()) {
            if(!$isEnough && $amountSpent <= $currentCashbackAmount){
                $isEnough = true;
            }
        }
        $checkEnoughCreditStmt->close();

        return $isEnough;
    } else {
        print_r($conn->error);
        return false;
    }
}

function checkEnoughContributionCredit($conn, $id, $donationAmount){
    //this is to validate whether the user is login to a merchant that he registered, not other user's merchant
    $checkEnoughContributionCreditSql = "SELECT current_charity_amount   
                             FROM member   
                             WHERE id = ?";

    $isEnough = false;

    if ($checkEnoughContributionCreditStmt = $conn->prepare($checkEnoughContributionCreditSql)) {

        $checkEnoughContributionCreditStmt->bind_param("i", $id);

        /* execute query */
        $checkEnoughContributionCreditStmt->execute();

        $checkEnoughContributionCreditStmt->bind_result($currentCharityAmount);

        while ($checkEnoughContributionCreditStmt->fetch()) {
            if(!$isEnough && $donationAmount <= $currentCharityAmount){
                $isEnough = true;
            }
        }
        $checkEnoughContributionCreditStmt->close();

        return $isEnough;
    } else {
        print_r($conn->error);
        return false;
    }
}

function isCorrectPinFormat($pinNo){
    //to check is 6 digit number bo(wont have characters, only numbers)
    if(preg_match('@\d{6}@', $pinNo) && strlen($pinNo) == 6){
        return true;
    }else{
        return false;
    }
}

function getDailyManagementFee($conn, $merchantId, $day, $month, $year)
{
    if($day > 0){
        $managementFeeSql = "  SELECT SUM(cashback.amount) 
                                    FROM receipt  
                                    INNER JOIN cashback ON cashback.receipt_id = receipt.id 
                                    WHERE receipt.merchant_id = ? 
                                    AND cashback.type = 'management fee'
                                    AND receipt.timestamp 
                                    BETWEEN DATE_SUB('$year-$month-$day 00:00:00', INTERVAL 13 HOUR) 
                                    AND DATE_SUB('$year-$month-$day 23:59:59', INTERVAL 13 HOUR)";
    }else{
        $managementFeeSql = "  SELECT SUM(cashback.amount) 
                                    FROM receipt  
                                    INNER JOIN cashback ON cashback.receipt_id = receipt.id 
                                    WHERE receipt.merchant_id = ? 
                                    AND cashback.type = 'management fee'";
    }

    if ($managementFeeStmt = $conn->prepare($managementFeeSql)) {

        $managementFeeStmt->bind_param("i", $merchantId);

        /* execute query */
        $managementFeeStmt->execute();

        $managementFeeStmt->bind_result($managementFeeAmount);

        $rows = array();

        while ($managementFeeStmt->fetch()) {

            $row = array(
                'management-fee-amount' => $managementFeeAmount
            );
            $rows[] = $row;
        }

        $managementFeeStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getCashbackReport($conn, $whereId, $offset, $perpage, $where1, $where2, $sortBy, $day, $month, $year)
{
    if($day > 0){
        $merchantCashbackReportSql = "  SELECT receipt.id, receipt.timestamp + Interval 13 Hour 'receipt.timestamp', receipt.merchant_approval_status, 
                                           receipt.cashback_amount, cashier.name, member.full_name, merchant.shop_name, 
                                           receipt.receipt_number, receipt.receipt_remark, 
                                           receipt.amount_spent, receipt.pay_with_credit 
                                    FROM receipt  
                                    INNER JOIN merchant ON receipt.merchant_id = merchant.id 
                                    INNER JOIN cashier ON receipt.cashier_id = cashier.username 
                                    INNER JOIN member ON receipt.member_id = member.id 
                                    WHERE $where1 = ? $where2
                                    AND receipt.timestamp 
                                    BETWEEN DATE_SUB('$year-$month-$day 00:00:00', INTERVAL 13 HOUR) 
                                    AND DATE_SUB('$year-$month-$day 23:59:59', INTERVAL 13 HOUR)
                                    ORDER BY receipt.timestamp 
                                    $sortBy LIMIT ?, ?";
    }else{
        $merchantCashbackReportSql = "  SELECT receipt.id, receipt.timestamp + Interval 13 Hour 'receipt.timestamp', receipt.merchant_approval_status, 
                                           receipt.cashback_amount, cashier.name, member.full_name, merchant.shop_name, 
                                           receipt.receipt_number, receipt.receipt_remark, 
                                           receipt.amount_spent, receipt.pay_with_credit 
                                    FROM receipt  
                                    INNER JOIN merchant ON receipt.merchant_id = merchant.id 
                                    INNER JOIN cashier ON receipt.cashier_id = cashier.username 
                                    INNER JOIN member ON receipt.member_id = member.id 
                                    WHERE $where1 = ? $where2
                                    ORDER BY receipt.timestamp 
                                    $sortBy LIMIT ?, ?";
    }

    if ($merchantCashbackReportStmt = $conn->prepare($merchantCashbackReportSql)) {

        $merchantCashbackReportStmt->bind_param("iii", $whereId, $offset, $perpage);

        /* execute query */
        $merchantCashbackReportStmt->execute();

        $merchantCashbackReportStmt->bind_result($id, $timestamp, $merchantApprovalStatus,
                                                 $cashbackAmount, $cashierName, $memberName, $shopName,
                                                 $receiptNumber, $receiptRemark, $amountSpent, $payWithCredit);

        $rows = array();

        while ($merchantCashbackReportStmt->fetch()) {

            $row = array(
                'id' => $id,
                'timestamp' => $timestamp,
                'merchant-approval-status' => $merchantApprovalStatus,
                'cashback-amount' => $cashbackAmount,
                'cashier-name' => $cashierName,
                'member-name' => $memberName,
                'shop-name' => $shopName,
                'receipt-number' => $receiptNumber,
                'receipt-remark' => $receiptRemark,
                'amount-spent' => $amountSpent,
                'pay-with-credit' => $payWithCredit
            );
            $rows[] = $row;
        }

        $merchantCashbackReportStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getMerchantCreditReport($conn, $offset, $perpage)
{

    $merchantCreditReportSql = "    SELECT  merchant.id, merchant.shop_name
                                    FROM merchant_credit 
									INNER JOIN merchant ON merchant_credit.merchant_id = merchant.id
									GROUP BY merchant.id
                                    LIMIT ?, ?";

    if ($merchantCreditReportStmt = $conn->prepare($merchantCreditReportSql)) {

        $merchantCreditReportStmt->bind_param("ii", $offset, $perpage);

        /* execute query */
        $merchantCreditReportStmt->execute();

        $merchantCreditReportStmt->bind_result($merchantId, $shopName);

        $rows = array();

        while ($merchantCreditReportStmt->fetch()) {
            $row = array(
                'id' => $merchantId,
                'shop-name' => $shopName
            );
            $rows[] = $row;
        }

        $merchantCreditReportStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getAddTypeCredit($conn, $merchantId){
    $addType = 0;

    $addTypeCreditSql = "    SELECT SUM(amount) 
                             FROM `merchant_credit` 
                             WHERE type = 'add'
                             AND merchant_id = ?";

    if ($addTypeCreditStmt = $conn->prepare($addTypeCreditSql)) {

        $addTypeCreditStmt->bind_param("i", $merchantId);

        /* execute query */
        $addTypeCreditStmt->execute();

        $addTypeCreditStmt->bind_result($addType);

        $rows = array();

        while ($addTypeCreditStmt->fetch()) {
            $row = array(
                'add-type' => $addType
            );
            $rows[] = $row;
        }

        $addTypeCreditStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getSubTypeCredit($conn, $merchantId){
    $subType = 0;

    $subTypeCreditSql = "    SELECT SUM(amount) 
                                    FROM `merchant_credit` 
                                    WHERE type = 'sub'
                                    AND merchant_id = ?";

    if ($subTypeCreditStmt = $conn->prepare($subTypeCreditSql)) {

        $subTypeCreditStmt->bind_param("i", $merchantId);

        /* execute query */
        $subTypeCreditStmt->execute();

        $subTypeCreditStmt->bind_result($subType);

        $rows = array();

        while ($subTypeCreditStmt->fetch()) {
            $row = array(
                'sub-type' => $subType
            );
            $rows[] = $row;
        }

        $subTypeCreditStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getCreditRowCount($conn){

    $rowCountSql = "SELECT  COUNT(*)  
                    FROM  merchant_credit 
                    GROUP BY merchant_id ";
    $totalRow = 0;

    if ($rowCountStmt = $conn->prepare($rowCountSql)) {

//        $rowCountStmt->bind_param("i", $id);

        /* execute query */
        $rowCountStmt->execute();

        $rowCountStmt->bind_result($count);

        $rows = array();

        while ($rowCountStmt->fetch()) {
            $totalRow++;
            $row = array(
                'count' => $totalRow
            );
            $rows[] = $row;
        }

        $rowCountStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getBottomReferralTree($conn, $memberId)
{
    $referralTreeSql = "  SELECT member.id, member.full_name, referral.member_id 
                          FROM member  
                          INNER JOIN referral ON referral.member_id = member.id 
                          WHERE referral.referral_code = ? ";

    if ($referralTreeStmt = $conn->prepare($referralTreeSql)) {

        $referralTreeStmt->bind_param("i", $memberId);

        /* execute query */
        $referralTreeStmt->execute();

        $referralTreeStmt->bind_result($id, $memberName, $referralCode);

        $rows = array();

        while ($referralTreeStmt->fetch()) {

            $row = array(
                'id' => $id,
                'member-name' => $memberName,
                'referral-code' => $referralCode
            );
            $rows[] = $row;
        }

        $referralTreeStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getTopReferralTree($conn, $memberId)
{
    $finalRows = array();
    $levelCount = 0;
    $tempMemberId = $memberId;

    $getLevelSql = "SELECT level, referral_code, top_referrer    
                    FROM referral  
                    WHERE member_id = ?";

    if ($getLevelStmt = $conn->prepare($getLevelSql)) {

        $getLevelStmt->bind_param("i", $memberId);

        /* execute query */
        $getLevelStmt->execute();

        $getLevelStmt->bind_result($levelCount, $tempMemberId, $topReferrerId);

        $getLevelStmt->fetch();

        $getLevelStmt->close();
    } else {
        print_r($conn->error);
        return null;
    }

    $getReferralDetailSql = "SELECT member.id, member.full_name, referral.referral_code 
                           FROM member  
                           INNER JOIN referral ON referral.member_id = member.id 
                           WHERE member.id = ? ";

    for ($x = $levelCount; $x > 1; $x--) {
        if ($getReferralDetailStmt = $conn->prepare($getReferralDetailSql)) {

            $getReferralDetailStmt->bind_param("i", $tempMemberId);

            /* execute query */
            $getReferralDetailStmt->execute();

            $getReferralDetailStmt->bind_result($id, $memberName, $referralCode);

            $getReferralDetailStmt->fetch();

            $tempMemberId = $referralCode;
            $row = array(
                'id' => $id,
                'member-name' => $memberName,
                'referral-code' => $referralCode
            );
            $finalRows[] = $row;

            $getReferralDetailStmt->close();
        } else {
            print_r($conn->error);
            return null;
        }
    }

    $getTopReferrerSql = "SELECT id, full_name  
                           FROM member  
                           WHERE id = ? ";

    if ($getTopReferrerStmt = $conn->prepare($getTopReferrerSql)) {

        $getTopReferrerStmt->bind_param("i", $topReferrerId);

        /* execute query */
        $getTopReferrerStmt->execute();

        $getTopReferrerStmt->bind_result($id, $memberName);

        $getTopReferrerStmt->fetch();

        $nullReferralCode = 0;
        $row = array(
            'id' => $id,
            'member-name' => $memberName,
            'referral-code' => $nullReferralCode
        );
        $finalRows[] = $row;

        $getTopReferrerStmt->close();
    } else {
        print_r($conn->error);
        return null;
    }

//    while(!$isDone){
//        $getReferralSql = "SELECT member.id, member.full_name, referral.referral_code
//                           FROM member
//                           INNER JOIN referral ON referral.member_id = member.id
//                           WHERE member.id = ? ";
//
//        if ($getReferralStmt = $conn->prepare($getReferralSql)) {
//
//            $getReferralStmt->bind_param("i", $tempMemberId);
//
//            /* execute query */
//            $getReferralStmt->execute();
//
//            $getReferralStmt->bind_result($rsId, $rsMemberName, $rsReferralCode);
//
//            if($rsReferralCode == null){
//                $newRow = array(
//                    'id' => $rsId,
//                    'member-name' => $rsMemberName,
//                    'referral-code' => $rsReferralCode
//                );
//                $finalRows[] = $newRow;
//                $isDone = true;
//            }else{
//                $tempMemberId = $rsReferralCode;
//                $newRow = array(
//                    'id' => $rsId,
//                    'member-name' => $rsMemberName,
//                    'referral-code' => $rsReferralCode
//                );
//                $finalRows[] = $newRow;
//            }
//
//            $getReferralStmt->close();
//        } else{
//            $isDone = true;
//            print_r($conn->error);
//            return null;
//        }
//    }

    return $finalRows;
}

function getDonationReport($conn, $ngoId, $offset, $perpage, $sortBy)
{
    $donationSql = "  SELECT donation.id, donation.date_of_donation + Interval 13 Hour 'donation.date_of_donation', 
                                           donation.bank_in_status, 
                                           donation.amount, member.full_name   
                                    FROM donation  
                                    INNER JOIN ngo ON donation.ngo_id = ngo.id 
                                    INNER JOIN member ON donation.member_id = member.id 
                                    WHERE ngo_id = ? 
                                    ORDER BY donation.date_of_donation 
                                    $sortBy LIMIT ?, ?";

    if ($donationStmt = $conn->prepare($donationSql)) {

        $donationStmt->bind_param("iii", $ngoId, $offset, $perpage);

        /* execute query */
        $donationStmt->execute();

        $donationStmt->bind_result($id, $timestamp, $bankInStatus, $amount, $memberName);

        $rows = array();

        while ($donationStmt->fetch()) {

            $row = array(
                'id' => $id,
                'timestamp' => $timestamp,
                'bank-in-status' => $bankInStatus,
                'amount' => $amount,
                'member-name' => $memberName
            );
            $rows[] = $row;
        }

        $donationStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getReceiptDetail($conn, $receiptId){
    $receiptDetailSql = "SELECT  amount_spent, member_id, pay_with_credit, cashback_amount  
                        FROM receipt  
                        WHERE id = ?";

    if ($receiptDetailStmt = $conn->prepare($receiptDetailSql)) {

        $receiptDetailStmt->bind_param("i", $receiptId);

        /* execute query */
        $receiptDetailStmt->execute();

        $receiptDetailStmt->bind_result($amountSpent, $memberId, $payWithCredit, $cashbackAmount);

        $rows = array();

        while ($receiptDetailStmt->fetch()) {
            $row = array(
                'amount-spent' => $amountSpent,
                'member-id' => $memberId,
                'pay-with-credit' => $payWithCredit,
                'cashback-amount' => $cashbackAmount
            );
            $rows[] = $row;
        }

        $receiptDetailStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getRowCount($conn, $id, $table, $whereColumn, $day, $month, $year){

    if($day > 0){
        $rowCountSql = "SELECT  COUNT(*)  
                    FROM  $table 
                    WHERE $whereColumn = ? 
                    AND timestamp  
                    BETWEEN DATE_SUB('$year-$month-$day 00:00:00', INTERVAL 13 HOUR) 
                    AND DATE_SUB('$year-$month-$day 23:59:59', INTERVAL 13 HOUR)";
    }else{
        $rowCountSql = "SELECT  COUNT(*)  
                    FROM  $table 
                    WHERE $whereColumn = ? ";
    }
    
    if ($rowCountStmt = $conn->prepare($rowCountSql)) {

        $rowCountStmt->bind_param("i", $id);

        /* execute query */
        $rowCountStmt->execute();

        $rowCountStmt->bind_result($count);

        $rows = array();

        while ($rowCountStmt->fetch()) {
            $row = array(
                'count' => $count
            );
            $rows[] = $row;
        }

        $rowCountStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getUserRegisteredMerchant($conn, $memberId){
    $userRegisteredMerchantSql = "SELECT id, shop_name  
                                  FROM merchant  
                                  WHERE member_id = ?";

    if ($userRegisteredMerchantStmt = $conn->prepare($userRegisteredMerchantSql)) {

        $userRegisteredMerchantStmt->bind_param("i", $memberId);

        /* execute query */
        $userRegisteredMerchantStmt->execute();

        $userRegisteredMerchantStmt->bind_result($id,$shopName);

        $rows = array();

        while ($userRegisteredMerchantStmt->fetch()) {
            $row = array(
                'id' => $id,
                'shop-name' => $shopName
            );
            $rows[] = $row;
        }

        $userRegisteredMerchantStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getUserRegisteredNgo($conn, $memberId){
    $userRegisteredNgoSql = "SELECT id, name   
                                  FROM ngo  
                                  WHERE member_id = ?";

    if ($userRegisteredNgoStmt = $conn->prepare($userRegisteredNgoSql)) {

        $userRegisteredNgoStmt->bind_param("i", $memberId);

        /* execute query */
        $userRegisteredNgoStmt->execute();

        $userRegisteredNgoStmt->bind_result($id,$name);

        $rows = array();

        while ($userRegisteredNgoStmt->fetch()) {
            $row = array(
                'id' => $id,
                'name' => $name
            );
            $rows[] = $row;
        }

        $userRegisteredNgoStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
        return null;
    }
}

function getListOfMerchants($conn, $merchantSql, $query)
{
    //$merchantSql = "SELECT id, shop_name FROM merchant WHERE ? = ?";

    if ($merchantStmt = $conn->prepare($merchantSql)) {

        $merchantStmt->bind_param("s", $query);

        /* execute query */
        $merchantStmt->execute();

        $merchantStmt->bind_result($id, $shopName);

        $rows = array();

        while ($merchantStmt->fetch()) {
            $row = array(
                'id' => $id,
                'shop_name' => $shopName
            );
            $rows[] = $row;
        }

        $merchantStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getCashierList($conn, $merchantId){
    $getCashierSql = "SELECT username, name FROM cashier WHERE merchant_id = ? && active_status = 1 ";

    if ($getCashierStmt = $conn->prepare($getCashierSql)) {

        $getCashierStmt->bind_param("i", $merchantId);

        /* execute query */
        $getCashierStmt->execute();

        $getCashierStmt->bind_result($username, $name);

        $rows = array();

        while ($getCashierStmt->fetch()) {
            $row = array(
                'username' => $username,
                'name' => $name
            );
            $rows[] = $row;
        }

        $getCashierStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getMerchantDetails($conn, $id)
{
    $merchantSql = "SELECT logo_link, shop_name, description,
                           operating_hours, email, phone_number, 
                           lot_number, street_name, postcode, 
                           city, state, country, website, social_media, category, 
                           latitude, longitude, date_joined
                    FROM merchant 
                    WHERE id = ?";

    if ($merchantStmt = $conn->prepare($merchantSql)) {

        $merchantStmt->bind_param("i", $id);

        /* execute query */
        $merchantStmt->execute();

        $merchantStmt->bind_result($logoLink, $shopName, $description, $operatingHours, $email,
            $phoneNumber, $lotNumber, $streetName, $postcode, $city, $state,
            $country, $website, $socialMedia, $category, $latitude, $longitude, $dateJoined);

        $rows = array();

        while ($merchantStmt->fetch()) {
            $row = array(
                'logo-link' => $logoLink,
                'shop-name' => $shopName,
                'description' => $description,
                'operating-hours' => $operatingHours,
                'email' => $email,
                'phone-number' => $phoneNumber,
                'lot-number' => $lotNumber,
                'street-name' => $streetName,
                'postcode' => $postcode,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'website' => $website,
                'social-media' => $socialMedia,
                'category' => $category,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'date-joined' => $dateJoined
            );
            $rows[] = $row;
        }

        $merchantStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getNgoDetails($conn, $id)
{
    $ngoSql = "SELECT logo_link, name, description,
                           operating_hours, email, phone_number, 
                           lot_number, street_name, postcode, 
                           city, state, country, website, social_media, category, 
                           latitude, longitude, date_joined
                    FROM ngo 
                    WHERE id = ?";

    if ($ngoStmt = $conn->prepare($ngoSql)) {

        $ngoStmt->bind_param("i", $id);

        /* execute query */
        $ngoStmt->execute();

        $ngoStmt->bind_result($logoLink, $name, $description, $operatingHours, $email,
            $phoneNumber, $lotNumber, $streetName, $postcode, $city, $state,
            $country, $website, $socialMedia, $category, $latitude, $longitude, $dateJoined);

        $rows = array();

        while ($ngoStmt->fetch()) {
            $row = array(
                'logo-link' => $logoLink,
                'name' => $name,
                'description' => $description,
                'operating-hours' => $operatingHours,
                'email' => $email,
                'phone-number' => $phoneNumber,
                'lot-number' => $lotNumber,
                'street-name' => $streetName,
                'postcode' => $postcode,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'website' => $website,
                'social-media' => $socialMedia,
                'category' => $category,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'date-joined' => $dateJoined
            );
            $rows[] = $row;
        }

        $ngoStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getAllMerchants($conn, $where, $orderBy)
{

    $merchantSql = "SELECT merchant.id, merchant.logo_link, merchant.shop_name, merchant.description, 
                           merchant.operating_hours, merchant.email, merchant.phone_number, merchant.lot_number,
                           merchant.street_name, merchant.postcode, merchant.city, merchant.state, merchant.country, 
                           merchant.website, merchant.social_media, merchant.category, merchant.latitude, 
                           merchant.longitude, merchant.date_joined  
                    FROM merchant 
                    INNER JOIN cashback_rate ON cashback_rate.merchant_id = merchant.id 
                    WHERE approval_status = 'approved' $where 
                    GROUP BY cashback_rate.merchant_id  
                    $orderBy ";

    if ($merchantStmt = $conn->prepare($merchantSql)) {

//        $merchantStmt->bind_param("s", $query);

        /* execute query */
        $merchantStmt->execute();

        $merchantStmt->bind_result($id, $logoLink, $shopName, $description, $operatingHours, $email,
            $phoneNumber, $lotNumber, $streetName, $postcode, $city, $state,
            $country, $website, $socialMedia, $category, $latitude, $longitude, $dateJoined);

        $rows = array();

        while ($merchantStmt->fetch()) {
            $row = array(
                'id' => $id,
                'logo-link' => $logoLink,
                'shop-name' => $shopName,
                'description' => $description,
                'operating-hours' => $operatingHours,
                'email' => $email,
                'phone-number' => $phoneNumber,
                'lot-number' => $lotNumber,
                'street-name' => $streetName,
                'postcode' => $postcode,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'website' => $website,
                'social-media' => $socialMedia,
                'category' => $category,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'date-joined' => $dateJoined
            );
            $rows[] = $row;
        }

        $merchantStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getAllNgos($conn, $where, $orderBy)
{
    $ngoSql = "SELECT id, logo_link, name, description, operating_hours, email, phone_number, lot_number,
                           street_name, postcode, city, state, country, website, social_media, category, latitude, longitude,
                           date_joined 
                    FROM ngo  
                    WHERE approval_status = 'approved' $where 
                    $orderBy ";

    if ($ngoStmt = $conn->prepare($ngoSql)) {

//        $merchantStmt->bind_param("s", $query);

        /* execute query */
        $ngoStmt->execute();

        $ngoStmt->bind_result($id, $logoLink, $name, $description, $operatingHours, $email,
            $phoneNumber, $lotNumber, $streetName, $postcode, $city, $state,
            $country, $website, $socialMedia, $category, $latitude, $longitude, $dateJoined);

        $rows = array();

        while ($ngoStmt->fetch()) {
            $row = array(
                'id' => $id,
                'logo-link' => $logoLink,
                'name' => $name,
                'description' => $description,
                'operating-hours' => $operatingHours,
                'email' => $email,
                'phone-number' => $phoneNumber,
                'lot-number' => $lotNumber,
                'street-name' => $streetName,
                'postcode' => $postcode,
                'city' => $city,
                'state' => $state,
                'country' => $country,
                'website' => $website,
                'social-media' => $socialMedia,
                'category' => $category,
                'latitude' => $latitude,
                'longitude' => $longitude,
                'date-joined' => $dateJoined
            );
            $rows[] = $row;
        }

        $ngoStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getListOfNgo($conn, $ngoSql, $query)
{
    //$merchantSql = "SELECT id, shop_name FROM merchant WHERE ? = ?";

    if ($ngoStmt = $conn->prepare($ngoSql)) {

        $ngoStmt->bind_param("s", $query);

        /* execute query */
        $ngoStmt->execute();

        $ngoStmt->bind_result($id, $name);

        $rows = array();

        while ($ngoStmt->fetch()) {
            $row = array(
                'id' => $id,
                'name' => $name
            );
            $rows[] = $row;
        }

        $ngoStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function getListOfMembers($conn, $repSql, $query)
{
    //$repSql = "SELECT id, full_name FROM member WHERE ? = ?";

    if ($repStmt = $conn->prepare($repSql)) {

        $repStmt->bind_param("s", $query);

        /* execute query */
        $repStmt->execute();

        $repStmt->bind_result($id, $fullName);

        $rows = array();

        while ($repStmt->fetch()) {
            $row = array(
                'id' => $id,
                'full_name' => $fullName
            );
            $rows[] = $row;
        }

        $repStmt->close();

        return $rows;
    } else {
        print_r($conn->error);
    }
}

function validateReallyThisMerchant($conn, $memberId, $merchantId){
    //this is to validate whether the user is login to a merchant that he registered, not other user's merchant
    $validateMerchantSql = "SELECT id   
                                  FROM merchant  
                                  WHERE member_id = ?";

    $isFound = false;

    if ($validateMerchantStmt = $conn->prepare($validateMerchantSql)) {

        $validateMerchantStmt->bind_param("i", $memberId);

        /* execute query */
        $validateMerchantStmt->execute();

        $validateMerchantStmt->bind_result($rsMerchantId);

        while ($validateMerchantStmt->fetch()) {
            if(!$isFound && strcmp($merchantId, $rsMerchantId) == 0){
                $isFound = true;
            }
        }
        $validateMerchantStmt->close();

        return $isFound;
    } else {
        print_r($conn->error);
        return false;
    }
}

function validateReallyThisNgo($conn, $memberId, $ngoId){
    //this is to validate whether the user is login to a merchant that he registered, not other user's merchant
    $validateNgoSql = "SELECT id   
                                  FROM ngo  
                                  WHERE member_id = ?";

    $isFound = false;

    if ($validateNgoStmt = $conn->prepare($validateNgoSql)) {

        $validateNgoStmt->bind_param("i", $memberId);

        /* execute query */
        $validateNgoStmt->execute();

        $validateNgoStmt->bind_result($rsNgoId);

        while ($validateNgoStmt->fetch()) {
            if(!$isFound && strcmp($ngoId, $rsNgoId) == 0){
                $isFound = true;
            }
        }
        $validateNgoStmt->close();

        return $isFound;
    } else {
        print_r($conn->error);
        return false;
    }
}

function calculateCashbacks($conn, $where, $id){
    //this is to validate whether the user is login to a merchant that he registered, not other user's merchant
    $calculateCashbacksSql = "SELECT SUM(amount_spent), SUM(cashback_amount)   
                              FROM receipt  
                              WHERE $where = ? && merchant_approval_status = 'approved'";

    if ($calculateCashbacksStmt = $conn->prepare($calculateCashbacksSql)) {

        $calculateCashbacksStmt->bind_param("i", $id);

        /* execute query */
        $calculateCashbacksStmt->execute();

        $calculateCashbacksStmt->bind_result($totalAmountSpent, $totalCashbackAmount);

        $rows = array();

        while ($calculateCashbacksStmt->fetch()) {
            $row = array(
                'total-amount-spent' => $totalAmountSpent,
                'total-cashback-amount' => $totalCashbackAmount
            );
            $rows[] = $row;
        }
        $calculateCashbacksStmt->close();

        return $rows;
    } else {
        return ($conn->error);
    }
}

function validateTwoDecimals($number)
{
    if (preg_match('/^[0-9]+\.[0-9]{2}$/', $number) || preg_match('/^[0-9]+\.[0-9]{1}$/', $number))
        return true;
    else
        return false;
}

function getNumberAfterDecimalPoint($number)
{
    $num = explode('.', $number);
    return $num;
}

function updateReferralCashback($originalMemberId, $memberId, $noofTime, $amount, $receiptId, $conn)
{
    $threeAmountCharity = $amount * (3 / 100);
    $twoAmount = $amount * (2 / 100);
    $fourAmount = $amount * (4 / 100);
    $eightAmount = $amount * (8 / 100);

    if ($memberId != null) {
        $sql = "SELECT referral_code FROM `referral` WHERE member_id = ?";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("i", $memberId);

            $stmt->execute();

            $stmt->bind_result($referralCode);

            $stmt->fetch();

            $stmt->close();

        }
    }

    /************************GET TOTAL AND CURRENT CASHBACK AMOUNT FROM IST*************************/
    $sql = 'SELECT current_cashback_amount, total_cashback_amount, id FROM `member` WHERE category = "IST"';

    if ($stmt = $conn->prepare($sql)) {

        $stmt->execute();

        $stmt->bind_result($currentISTCashbackAmount, $totalISTCashbackAmount, $istId);

        $stmt->fetch();

        $stmt->close();

    }

    if ($referralCode == null) {
        /************************GET TOTAL AND CURRENT CASHBACK AMOUNT FROM MM*************************/
        $sql = 'SELECT current_cashback_amount, total_cashback_amount, current_charity_amount, total_charity_amount, id FROM `member` WHERE category = "MM"';

        if ($stmt = $conn->prepare($sql)) {

            $stmt->execute();

            $stmt->bind_result($currentMMCashbackAmount, $totalMMCashbackAmount, $currentMMCharityAmount, $totalMMCharityAmount, $mmId);

            $stmt->fetch();

            $stmt->close();

        }

        if ($noofTime == 1) {
            //3% into MM
            //2%, 4%, 8% into IST
            $totalMMAmount = $threeAmountCharity;
            $totalISTAmount = $twoAmount + $fourAmount + $eightAmount;
//            $MMType = "mm charity 4";
            $MMType = "mm charity 1";
            $ISTType = "ist cashback 3";
        } else if ($noofTime == 2) {
            //2% into MM
            //4%, 8% into IST
            $totalMMAmount = $twoAmount;
            $totalISTAmount = $fourAmount + $eightAmount;
//            $MMType = "mm cashback 3";
            $MMType = "mm charity 2";
            $ISTType = "ist cashback 2";
        } else if ($noofTime == 3) {
            //4% into MM
            //8% into IST
            $totalMMAmount = $fourAmount;
            $totalISTAmount = $eightAmount;
//            $MMType = "mm cashback 2";
            $MMType = "mm charity 3";
            $ISTType = "ist cashback 1";
        } else if ($noofTime == 4) {
            //8% into MM
            //0% into IST
            $totalMMAmount = $eightAmount;
//            $MMType = "mm cashback 1";
            $MMType = "mm charity 4";
        }

        /*****************************INSERT INTO CASHBACK***********************************/
        /**************************MASTER MEMBER**********************************/
        $sql = "INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?, ?)";
        $finalMMCurrentCashback = $currentMMCashbackAmount + $totalMMAmount;
        $finalMMTotalCashback = $totalMMCashbackAmount + $totalMMAmount;

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("isdii", $originalMemberId, $MMType, $totalMMAmount, $mmId, $receiptId);

            $stmt->execute();

            $stmt->close();
        }

        /**************************IST**********************************/
        $sql = "INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?, ?)";
        $finalISTCurrentCashback = $currentISTCashbackAmount + $totalISTAmount;
        $finalISTTotalCashback = $totalISTCashbackAmount + $totalISTAmount;

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("isdii", $originalMemberId, $ISTType, $totalISTAmount, $istId, $receiptId);

            $stmt->execute();

            $stmt->close();
        }
        /*************************************INSERT INTO CASHBACK END*********************************************************/
        /*****************************UPDATE INTO MEMBER FOR MM's UPDATED CASHBACK AMOUNT***********************************/
        if ($noofTime == 1) {
            $finalMMCurrentCharity = $currentMMCharityAmount + $totalMMAmount;
            $finalMMTotalCharity = $totalMMCharityAmount + $totalMMAmount;

            $sql = 'UPDATE member SET current_charity_amount = ?, total_charity_amount = ? WHERE category = "MM"';
            if ($stmt = $conn->prepare($sql)) {

                $stmt->bind_param("dd", $finalMMCurrentCharity, $finalMMTotalCharity);

                $stmt->execute();

                $stmt->close();
            }
        } else {
            $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE category = "MM"';
            if ($stmt = $conn->prepare($sql)) {

                $stmt->bind_param("dd", $finalMMCurrentCashback, $finalMMTotalCashback);

                $stmt->execute();

                $stmt->close();
            }
        }

        //because at the 4th round IST wont be getting anything
        if ($noofTime < 4) {
            $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE category = "IST"';
            if ($stmt = $conn->prepare($sql)) {

                $stmt->bind_param("dd", $finalISTCurrentCashback, $finalISTTotalCashback);

                $stmt->execute();

                $stmt->close();
            }
        }
        return "complete";
    }//if referralCode == null ENDs
    else {
        /************************GET TOTAL AND CURRENT CASHBACK AMOUNT FROM MEMBER*************************/
        $sql = 'SELECT current_charity_amount, total_charity_amount, current_cashback_amount, total_cashback_amount, category FROM `member` WHERE id = ?';

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("i", $referralCode);

            $stmt->execute();

            $stmt->bind_result($currentMemberCharityAmount, $totalMemberCharityAmount, $currentMemberCashbackAmount, $totalMemberCashbackAmount, $category);

            $stmt->fetch();

            $stmt->close();

        }

        $sqlCashback = "INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?, ?)";
        $sqlMember = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE id = ?';
        $sqlMemberCharity = 'UPDATE member SET current_charity_amount = ?, total_charity_amount = ? WHERE id = ?';
        if ($noofTime == 1) {
            //3% to referrals (first upline) Contribution only
            /*****************************INSERT INTO CASHBACK***********************************/
            $inspireAmount = $threeAmountCharity;
            $finalCurrentMemberCharity = $currentMemberCharityAmount + $inspireAmount;
            $finalTotalMemberCharity = $totalMemberCharityAmount + $inspireAmount;
            $type = "inspire charity 1";

            if ($stmt = $conn->prepare($sqlCashback)) {

                $stmt->bind_param("isdii", $originalMemberId, $type, $inspireAmount, $referralCode, $receiptId);

                $stmt->execute();

                $stmt->close();
            }
            /*****************************UPDATE INTO MEMBER FOR UPDATED CASHBACK AMOUNT***********************************/
            if ($stmt = $conn->prepare($sqlMemberCharity)) {

                $stmt->bind_param("ddi", $finalCurrentMemberCharity, $finalTotalMemberCharity, $referralCode);

                $stmt->execute();

                $stmt->close();
            }
            //calls the function again
            $noofTimes = $noofTime + 1;
            updateReferralCashback($originalMemberId, $referralCode, $noofTimes, $amount, $receiptId, $conn);
        } else if ($noofTime == 2) {
            //2% to referrals (2nd upline) in cashback column
            /*****************************INSERT INTO CASHBACK***********************************/
            $inspireAmount = $twoAmount;
            $finalCurrentMemberCashback = $currentMemberCashbackAmount + $inspireAmount;
            $finalTotalMemberCashback = $totalMemberCashbackAmount + $inspireAmount;
            $type = "inspire cashback 2";

            //if he is not a biz rep then go to ist
            if (strcmp($category, "bizrep") != 0) {
                if ($stmt = $conn->prepare($sqlCashback)) {

                    $stmt->bind_param("isdii", $originalMemberId, $type, $inspireAmount, $istId, $receiptId);

                    $stmt->execute();

                    $stmt->close();
                }
                /*****************************UPDATE INTO MEMBER FOR UPDATED CASHBACK AMOUNT***********************************/
                if ($stmt = $conn->prepare($sqlMember)) {

                    $finalCurrentISTCashback = $currentISTCashbackAmount + $inspireAmount;
                    $finalTotalISTCashback = $totalISTCashbackAmount + $inspireAmount;

                    $stmt->bind_param("ddi", $finalCurrentISTCashback, $finalTotalISTCashback, $istId);

                    $stmt->execute();

                    $stmt->close();
                }
            } else {
                //to the biz rep
                if ($stmt = $conn->prepare($sqlCashback)) {

                    $stmt->bind_param("isdii", $originalMemberId, $type, $inspireAmount, $referralCode, $receiptId);

                    $stmt->execute();

                    $stmt->close();
                }
                /*****************************UPDATE INTO MEMBER FOR UPDATED CASHBACK AMOUNT***********************************/
                if ($stmt = $conn->prepare($sqlMember)) {

                    $stmt->bind_param("ddi", $finalCurrentMemberCashback, $finalTotalMemberCashback, $referralCode);

                    $stmt->execute();

                    $stmt->close();
                }
            }

            //calls the function again
            $noofTimes = $noofTime + 1;
            updateReferralCashback($originalMemberId, $referralCode, $noofTimes, $amount, $receiptId, $conn);
        } else if ($noofTime == 3) {
            //4% to referrals (3rd upline) in cashback column
            /*****************************INSERT INTO CASHBACK***********************************/
            $inspireAmount = $fourAmount;
            $finalCurrentMemberCashback = $currentMemberCashbackAmount + $inspireAmount;
            $finalTotalMemberCashback = $totalMemberCashbackAmount + $inspireAmount;
            $type = "inspire cashback 3";

            //if he is not a biz rep then go to ist
            if (strcmp($category, "bizrep") != 0) {
                if ($stmt = $conn->prepare($sqlCashback)) {

                    $stmt->bind_param("isdii", $originalMemberId, $type, $inspireAmount, $istId, $receiptId);

                    $stmt->execute();

                    $stmt->close();
                }
                /*****************************UPDATE INTO MEMBER FOR UPDATED CASHBACK AMOUNT***********************************/
                if ($stmt = $conn->prepare($sqlMember)) {

                    $finalCurrentISTCashback = $currentISTCashbackAmount + $inspireAmount;
                    $finalTotalISTCashback = $totalISTCashbackAmount + $inspireAmount;

                    $stmt->bind_param("ddi", $finalCurrentISTCashback, $finalTotalISTCashback, $istId);

                    $stmt->execute();

                    $stmt->close();
                }
            } else {
                //to the biz rep
                if ($stmt = $conn->prepare($sqlCashback)) {

                    $stmt->bind_param("isdii", $originalMemberId, $type, $inspireAmount, $referralCode, $receiptId);

                    $stmt->execute();

                    $stmt->close();
                }
                /*****************************UPDATE INTO MEMBER FOR UPDATED CASHBACK AMOUNT***********************************/
                if ($stmt = $conn->prepare($sqlMember)) {

                    $stmt->bind_param("ddi", $finalCurrentMemberCashback, $finalTotalMemberCashback, $referralCode);

                    $stmt->execute();

                    $stmt->close();
                }
            }

            //calls the function again
            $noofTimes = $noofTime + 1;
            updateReferralCashback($originalMemberId, $referralCode, $noofTimes, $amount, $receiptId, $conn);
        } else if ($noofTime == 4) {
            //8% to referrals (4th upline) in cashback column
            /*****************************INSERT INTO CASHBACK***********************************/
            $inspireAmount = $eightAmount;
            $finalCurrentMemberCashback = $currentMemberCashbackAmount + $inspireAmount;
            $finalTotalMemberCashback = $totalMemberCashbackAmount + $inspireAmount;
            $type = "inspire cashback 4";

            //if he is not a biz rep then go to ist
            if (strcmp($category, "bizrep") != 0) {
                if ($stmt = $conn->prepare($sqlCashback)) {

                    $stmt->bind_param("isdii", $originalMemberId, $type, $inspireAmount, $istId, $receiptId);

                    $stmt->execute();

                    $stmt->close();
                }
                /*****************************UPDATE INTO MEMBER FOR UPDATED CASHBACK AMOUNT***********************************/
                if ($stmt = $conn->prepare($sqlMember)) {

                    $finalCurrentISTCashback = $currentISTCashbackAmount + $inspireAmount;
                    $finalTotalISTCashback = $totalISTCashbackAmount + $inspireAmount;

                    $stmt->bind_param("ddi", $finalCurrentISTCashback, $finalTotalISTCashback, $istId);

                    $stmt->execute();

                    $stmt->close();
                }
            } else {
                //to the biz rep
                if ($stmt = $conn->prepare($sqlCashback)) {

                    $stmt->bind_param("isdii", $originalMemberId, $type, $inspireAmount, $referralCode, $receiptId);

                    $stmt->execute();

                    $stmt->close();
                }
                /*****************************UPDATE INTO MEMBER FOR UPDATED CASHBACK AMOUNT***********************************/
                if ($stmt = $conn->prepare($sqlMember)) {

                    $stmt->bind_param("ddi", $finalCurrentMemberCashback, $finalTotalMemberCashback, $referralCode);

                    $stmt->execute();

                    $stmt->close();
                }
            }

            //ends the recursive function
            return "complete";
        } else {
            return "fail";
        }
    }
}//function updateReferralCashback END

function getMemberCashbackAmount($id, $conn)
{
    $sql = 'SELECT current_cashback_amount, current_charity_amount, total_cashback_amount, total_charity_amount FROM `member` WHERE id = ?';

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $id);

        $stmt->execute();

        $stmt->bind_result($currentCashbackAmount, $currentCharityAmount, $totalCashbackAmount, $totalCharityAmount);

        $stmt->fetch();

        $stmt->close();

    }

    return array('current-cashback-amount' => $currentCashbackAmount,
        'current-charity-amount' => $currentCharityAmount,
        'total-cashback-amount' => $totalCashbackAmount,
        'total-charity-amount' => $totalCharityAmount);
}

function getAdminCashbackAmount($category, $conn)
{
    $sql = 'SELECT id, current_cashback_amount, current_charity_amount, total_cashback_amount, total_charity_amount FROM `member` WHERE category = ?';

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("s", $category);

        $stmt->execute();

        $stmt->bind_result($id, $currentCashbackAmount, $currentCharityAmount, $totalCashbackAmount, $totalCharityAmount);

        $stmt->fetch();

        $stmt->close();

    }

    return array('id' => $id,
        'current-cashback-amount' => $currentCashbackAmount,
        'current-charity-amount' => $currentCharityAmount,
        'total-cashback-amount' => $totalCashbackAmount,
        'total-charity-amount' => $totalCharityAmount);
}
?>