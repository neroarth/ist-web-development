<?php
	session_start();
	ob_start();
	require_once "php-files/usefulFunction.php";
	require_once "php-files/conDb.php";

	if(!isLogin()){
		header("Location: login");
		exit();
	}else if(!isMerchantLogin()){
        header("Location: profile");
        exit();
    }

	$conn->close();
	ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<link href="images/favicon.png" rel="icon" type="image/png"/>
		<meta name="description" content="ISpendTribute">
		<meta name="author" content="Spending tribute">

		<title>Merchant Registration Part 3</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
		<script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	h1{
	color:#fff;
	padding-top:50px;
	}
	.register-text{
		color:#9c77b4;
	}
	@media (max-width: 736px) {
	.footer {
		height:18%;
	}

}
 .caption{
	 padding-top: 10px;
 }
.block{
	width:100%;
	height:20%;
	border:30px solid  #f291bb;
	padding-bottom:20px;
}
option{
	border:0;
}
#bank-type {
	width:200px;

}
	</style>
  </head>
  <?php include 'nav.php' ?>
  <body class="body-color">
        <div class="container cancelpaddingtop">
        <h1 class="paddingtop25">Create Your Merchant Account</h1><div class="heartimgdiv"><img src="images/heart3.jpg" class="heartimg heartimg2no"></div><br>
        <p class="profilep">Merchant Logo</p>
		<div class="form-wrap nobgcolor">
				<div class="col-md-5 col-md-offset-3 editmargin">
					<img src="http://placehold.it/750x450" id="profilePicPreview" name="profilePicPreview" class="img-responsive whiteborder" />
				</div>
				<form action="uploadMerchantLogo" class="form-horizontal border-bottom nopaddingleft" method="post" enctype="multipart/form-data" id="merchantRegistrationPart3">
			<input name="uploadLogo[]" type="file" accept="image/*" onchange="previewProfilePic(this);" class="uploadphoto" />
                     <div id="progress-wrp nopaddingleft"><div class="progress-bar"></div ><div class="status none">0%</div></div>
			   <div id="output"><!-- error or success results --></div>
	  
					<input id = "submit" name="submit" type="submit" class="btn btn-default btn-min-register floatleftsubmit" value="Upload Logo" />
					<input id = "skip" name="skip" type="submit" class="btn btn-default btn-min-register skiplink" value="Skip" onClick="skipDetected();" />
				</form> </div>
			  
    </div><!-- /.container -->
	<div class="block"></div>
	<?php include 'foot.php' ?>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<script type="text/javascript">
		//preview profile pic
		var isSkipped = false;

		function previewProfilePic(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					$('#profilePicPreview')
						.attr('src', e.target.result);
						//.width(150)
						//.height(200);
				};

				reader.readAsDataURL(input.files[0]);
			}
		}

		function skipDetected(){
			isSkipped = true;
		}

		//configuration
		var max_file_size 			= 10485760; //allowed file size. (1 MB = 1048576)
		var allowed_file_types 		= ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg']; //allowed file types
		var result_output 			= '#output'; //ID of an element for response output
		var my_form_id 				= '#merchantRegistrationPart3'; //ID of an element for response output
		var progress_bar_id 		= '#progress-wrp'; //ID of an element for response output
		var total_files_allowed 	= 1; //Number files allowed to upload

		//on form submit
		$(my_form_id).on( "submit", function(event) {
			//IF SKIP BUTTON IS PRESSED WONT EXECUTE THE FOLLOWING LINE
			if(!isSkipped){
				event.preventDefault();

				var proceed = true; //set proceed flag
				var error = [];	//errors
				var total_files_size = 0;

				//reset progressbar
				$(progress_bar_id +" .progress-bar").css("width", "0%");
				$(progress_bar_id + " .status").text("0%");

				if(!window.File && window.FileReader && window.FileList && window.Blob){ //if browser doesn't supports File API
					error.push("Your browser does not support new File API! Please upgrade."); //push error text
				}else{
					var total_selected_files = this.elements['uploadLogo[]'].files.length; //number of files

					//limit number of files allowed
					if(total_selected_files > total_files_allowed){
						error.push( "You have selected "+total_selected_files+" file(s), " + total_files_allowed +" is maximum!"); //push error text
						proceed = false; //set proceed flag to false
					}
					 //iterate files in file input field
					$(this.elements['uploadLogo[]'].files).each(function(i, ifile){
						if(ifile.value !== ""){ //continue only if file(s) are selected
							if(allowed_file_types.indexOf(ifile.type) === -1){ //check unsupported file
								//error.push( "<b>"+ ifile.name + "</b> is unsupported file type!"); //push error text
								error.push( "Sorry, please upload images with png, jpg or gif file extensions"); //push error text
								proceed = false; //set proceed flag to false
							}

							total_files_size = total_files_size + ifile.size; //add file size to total size
						}
					});

					//if total file size is greater than max file size
					if(total_files_size > max_file_size){
						error.push( "You have "+total_selected_files+" file(s) with total size "+total_files_size+", Allowed size is " + max_file_size +", Try smaller file!"); //push error text
						proceed = false; //set proceed flag to false
					}

					var submit_btn  = $(this).find("input[type=submit]"); //form submit button

					//if everything looks good, proceed with jQuery Ajax
					if(proceed){
						//submit_btn.val("Please Wait...").prop( "disabled", true); //disable submit button
						var form_data = new FormData(this); //Creates new FormData object
						var post_url = $(this).attr("action"); //get action URL of form

						//jQuery Ajax to Post form data
						$.ajax({
							url : post_url,
							type: "POST",
							data : form_data,
							contentType: false,
							success: function(data) {
								if(data.length > 3000){
									window.location = 'profile';
								}
							 },
							cache: false,
							processData:false,
							xhr: function(){
								//upload Progress
								var xhr = $.ajaxSettings.xhr();
								if (xhr.upload) {
									xhr.upload.addEventListener('progress', function(event) {
										var percent = 0;
										var position = event.loaded || event.position;
										var total = event.total;
										if (event.lengthComputable) {
											percent = Math.ceil(position / total * 100);
										}
										//update progressbar
										$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
										$(progress_bar_id + " .status").text(percent +"%");
									}, true);
								}
								return xhr;
							},
							mimeType:"multipart/form-data"
						}).done(function(res){ //
							$(my_form_id)[0].reset(); //reset form
							$(result_output).html(res); //output response from server
							//submit_btn.val("Upload Profile Picture").prop( "disabled", false); //enable submit button once ajax is done
							submit_btn.prop( "disabled", false);
							//window.location = 'profile';
						});
					}
				}

				$(result_output).html(""); //reset output
				$(error).each(function(i){ //output any error to output element
					$(result_output).append('<div class="error">'+error[i]+"</div>");
				});
			}
		});
	</script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
