<?php
session_start();
require_once "php-files/usefulFunction.php";
if (isset($_GET["logout"])) {
    logout();
}
if (isLogin()) {
    require_once "php-files/conDb.php";
    $sessionId = $_SESSION["id"];

    $merchantRows = getUserRegisteredMerchant($conn, $sessionId);
    $ngoRows = getUserRegisteredNgo($conn, $sessionId);

    $sql = "SELECT email, phone_no, full_name, profile_picture_link, current_cashback_amount, current_charity_amount, pin_no  
            FROM member 
            WHERE id = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $sessionId);

        $stmt->execute();

        $stmt->bind_result($rsEmail, $rsPhoneNo, $rsFullName, $rsProfilePictureLink, $rsCurrentCashbackAmount, $rsCurrentCharityAmount, $rsPinNo);

        $stmt->fetch();

        $stmt->close();
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["applyRep"])) {
            $stmt = $conn->prepare("UPDATE member SET biz_rep_status = 'pending' WHERE id = ?");
            $stmt->bind_param("i", $sessionId);
            $stmt->execute();
            $stmt->close();
            echo '<script type="text/javascript">'
            , 'alert("Thank you for applying as a Business Rep! We will get back to you shortly.");'
            , '</script>';
        }else if (isset($_POST["change-pin-no-btn"])) {
            $newPinNo = rewrite($_POST['new-pin-no']);
            if (isCorrectPinFormat($newPinNo)) {
                if ($stmt = $conn->prepare("UPDATE member SET pin_no = ? WHERE id = ?")) {
                    $stmt->bind_param("ii", $newPinNo, $sessionId);
                    $stmt->execute();
                    $stmt->close();
                    echo '<script type="text/javascript">'
                    , 'alert("New pin number updated successfully.");'
                    , '</script>';
                }
            } else {
                echo '<script type="text/javascript">'
                , 'alert("Please create a pin number with 6 number digits");'
                , '</script>';
            }
        }
    }
} else {
    header("Location: login");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <title>IST-Profile</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="main-style.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!-- [if lt IE 9]> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="assets/js/ie8-responsive-file-warning.js"></script>
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale = 1.0,
maximum-scale=1.0, user-scalable=no"/>
</head>
<style>
    .footer {
        border-top: 1px solid #d3d3d3;
    }

    .modal-header {
        background: #9c77b4;
        color: #fff;
    }

    select {
        border: 0;
        outline: 0;

    }


</style>

<body>
<?php include 'navProfile.php'; ?>
<div class="container overwrite-container">
    <div class="row">
        <div class="col-lg-3 col-md-4 centerise">
            <div class="text-center marginleftminus cancelmarginleft2">
                <?php
                if ($rsProfilePictureLink != null) {
                    echo '<img src="/' . $rsProfilePictureLink . '" class="img-thumbnail" alt="profile-image">';
                } else {
                    echo '<img src="./images/folio.png" class="img-thumbnail" alt="profile-image">';
                }
                ?>

                <h4><?php echo $rsFullName; ?></h4>
                <div class="text-center">
                    <p class="text-muted">(+60)
                        <?php echo $rsPhoneNo; ?>
                    </p>
                    <p class="text-muted">
                        <?php echo $rsEmail; ?>
                    </p>
                    <p class="text-muted">Your Referral Code: <?php echo $sessionId; ?>
                    </p>
                    <p class="text-muted">
                        <?php
                        echo 'Your Referral Link: <a href=" http://ispendtribute.com/memberRegistration?code='
                            . $sessionId
                            . '">http://ispendtribute.com/memberRegistration?code='
                            . $sessionId
                            . '</a>';
                        ?>
                    </p>
                    <?php
                    if (isLogin() || isAdminLogin() || isCashierLogin()) {
                        echo '<div class="btn-nav logina"><a class="btn btn-small navbar-btn pink" href="index.php?logout=true">Logout</a></div>';
                    } else {
//                            echo '<div class="btn-nav col-xs-1 logina2"><li><a class="btn btn-small navbar-btn pink-two" href="memberRegistration.php"><span style="color:#ffffff">Register</span></a></li></div>';
                        header("Location: login");
                        exit();
                    }
                    ?>
                </div>
            </div>
            <!-- end card-box -->
            <!-- <div class="panel panel-default">
            <h4 class="m-t-0 m-b-20 header-title">Skills</h4>
            <div class="p-b-10">

                <div class="progress progress-sm m-b-0">
                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                    </div>
                </div>
                  <p>Progression of account</p>
            </div>

        </div> -->
        </div>
        <!-- end col -->
        <div class="col-md-9 col-lg-5 text-center m10 cancelmarginleft2 addmleft">
            <div class="col-md-4 col-lg-4 col-md-push-2 col-xs-4 col-xs-push-1 text-center width-size cashback-size over-write-purple">
                <a href="charity">
                    <button class="btn purple select buttomwidth w3 same1" data-target="#cashback">Get Cashback
                    </button>
                </a>
                <div class="modal fade" id="cashback" tabindex="-1" role="dialog" aria-labelledby="title">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <a href="charity.php">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                </a>
                                <h4 class="modal-title" id="title">Increase donation rate from cashback</h4>
                            </div>
                            <div class="modal-body">
                                <p>Coming soon</p>
                            </div>
                        </div>
                    </div>
                </div>
                <p style="font-size:1.5em;" class="select normallineheight minheight sc-text"> Cashback Account </p>
                <div class="group-value">
                    <span class="text-price-two select resizeRM cash-text sc ">SC <?php echo $rsCurrentCashbackAmount; ?></span>
                    <!-- <button type="button" class="btn btn-circle editcircle cash-text ca-button ex0" data-toggle="modal"
                             data-target="#contribution">
                         <span class="glyphicon glyphicon-plus ca-button ex1"></span>
                     </button>-->
                </div>
            </div>
            <div class="verticalLine col-md-2 col-xs-1 col-xs-push-2 specialline overwrite-vertical"></div>
            <div class="col-md-4 col-md-push-3 col-xs-4 col-xs-push-3 cashback-size sm-margin overwrite-sm">
                <a href="charity?tab=ngo">
                    <button class="btn pinkthree select buttomwidth pink-overwrite w3 same1" data-toggle="modal"
                            data-target="#contribute">
                        Contribute
                    </button>
                </a>
                <!--                <button class="btn pinkthree select buttomwidth pink-overwrite w3 same1" data-toggle="modal"-->
                <!--                        data-target="#contribute">-->
                <!--                    Contribute-->
                <!--                </button>-->
                <!--                <div class="modal fade" id="contribute" tabindex="-1" role="dialog" aria-labelledby="title">-->
                <!--                    <div class="modal-dialog" role="document">-->
                <!--                        <div class="modal-content">-->
                <!--                            <div class="modal-header">-->
                <!--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span-->
                <!--                                            aria-hidden="true">&times;</span></button>-->
                <!--                                <h4 class="modal-title" id="title">Modal title</h4>-->
                <!--                            </div>-->
                <!--                            <div class="modal-body">-->
                <!--                                coming soon-->
                <!--                            </div>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <p style="font-size:1.5em;" class="select normallineheight minheight"> Contribution Account</p>
                <div class="w100">
                    <span class="text-price text-price-three select resizeRM pinkfont mm ca overwrite-cc">CC <?php echo $rsCurrentCharityAmount; ?></span>
                    <!--   <button type="button" class="btn btn-circle editcircle overwrite-cc-button ex0" data-toggle="modal"
                              data-target="#contribution">
                          <span class="glyphicon glyphicon-plus ex1"></span>
                      </button>--></div>

                <!-- Modal -->
<!--                <div class="modal fade" id="contribution" tabindex="-1" role="dialog"-->
<!--                     aria-labelledby="contributionLabel">-->
<!--                    <div class="modal-dialog" role="document">-->
<!--                        <div class="modal-content">-->
<!--                            <div class="modal-header">-->
<!--                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span-->
<!--                                            aria-hidden="true">&times;</span></button>-->
<!--                                <h4 class="modal-title" id="contributionLabel">Contribution</h4>-->
<!--                            </div>-->
<!--                            <div class="modal-body">-->
<!--                                Coming soon-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again1 v1 m-mobile"
                 style="z-index: 1;">
                <a href="merchantRegistration.php" class="btn purple-merchant select purplemerchant reg1 test2">Register As
                    Merchant</a>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again3 v2"
                 style="z-index: 0;">
                <a href="ngoRegistration.php" class="btn purple-merchant select purplemerchant mobilengo reg1">Register As NGO</a>
            </div>
            <?php
            if ($merchantRows != null) {
                echo '<div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-login overwrite-again4 v4 ta1">
                            <button class="btn purple select buttomwidth reg1" data-toggle="modal"
                                    data-target="#listOfMerchantModal">Merchant Profile
                            </button>
                      </div>';
            }
            ?>
            <?php
            if ($ngoRows != null) {
                echo ' <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-login overwrite-again4 v5">
                            <button class="btn purple select buttomwidth reg1 test t-overwrite" data-toggle="modal"
                                    data-target="#listOfNgoModal">NGO Profile
                            </button>
                        </div>';
            }
            ?>

            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-login overwrite-again4 v6 ta2">
                <button class="btn purple select buttomwidth reg1 test t-overwrite2" data-toggle="modal"
                        data-target="#changePinModal">Change PIN Number</button>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 spaceup rep overwrite-again2 v7">
                <!--    <form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="applyRepForm"> <input id="applyRep" name="applyRep" type="submit" class="btn btn-default btn-min-register reg1"
                           value="Apply as Business Rep"
                           style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"/>    </form>-->
                <a href="applyrep.php" class="applyrep-a"><input id="applyRep"
                                                                 class="btn btn-default btn-min-register reg1 ta3"
                                                                 style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"
                                                                 value="Apply as Business Rep"/></a>
            </div>
            <div class="modal fade" id="listOfMerchantModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Select the Merchant Profile</h4>
                        </div>
                        <div class="modal-body">
                            <?php
                            if ($merchantRows == null) {
                                echo '<tr>
                                            <td>Sorry, you haven\'t registered with us as a merchant</td>
                                       </tr>';
                            } else {
                                echo '<form action="merchantProfile" class="form-horizontal" method="post" enctype="multipart/form-data">';

                                foreach ($merchantRows as &$value) {
                                    echo '<button type="submit" class="btn btn-default btn-min-block" onClick="changeMerchant('
                                        . $value['id'] . ')">'
                                        . $value['shop-name'] .
                                        '</button>';
                                }

                                echo '<input type="hidden" id = "merchant-id" name="merchant-id" value="0">';

                                echo '</form>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="listOfNgoModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Login as NGO</h4>
                        </div>
                        <div class="modal-body">
                            <?php


                            if ($ngoRows == null) {
                                echo '<tr>
                                            <td>Sorry, you haven\'t registered with us as a ngo</td>
                                          </tr>';
                            } else {
                                echo '<form action="ngoProfile" class="form-horizontal" method="post" enctype="multipart/form-data">';

                                foreach ($ngoRows as &$value) {
                                    echo '<button type="submit" class="btn btn-default btn-min-block" onClick="changeNgo('
                                        . $value['id'] . ')">'
                                        . $value['name'] .
                                        '</button>';
                                }

                                echo '<input type="hidden" id = "ngo-id" name="ngo-id" value="0">';

                                echo '</form>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="changePinModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Change your pin number</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="change-pin-form">
                                <input type="password" class="form-controls" id="new-pin-no"
                                       name="new-pin-no" placeholder="Enter new pin number">
                                <div class="center-block">
                                <input id="change-pin-no-btn" name="change-pin-no-btn " type="submit" class="btn btn-default btn-min-register reg1"
                                       value="Submit" style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="receiptDetailModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Receipt Detail</h4>
                        </div>
                        <div class="modal-body" id="receiptDetailDiv" name ="receiptDetailDiv">

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-8 col-lg-9 col-xs-12 divsize d40">
            <h1 class="trans"> Transaction History</h1>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:#2ab574;" class="select"> &#x25cf; Approved</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p class="select"> &#x25cf; Pending</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:red" class="select"> &#x25cf; Rejected</p>
            </div>
            <div class="sortby">
                <div style="display:inline-block;">
                    <p class="select">Sort by</p>
                </div>
                <div class="col-md-5 col-lg-2 col-md-push-8  col-xs-3 col-xs-push-9 latest" name="sort-by-cb"
                     id="sort-by-cb">
                    <select>
                        <option>Newest</option>
                        <option>Oldest</option>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <table class="table">

                        <?php

//                        $perpage = 10;
//
//                        if (isset($_GET["page"])) {
//                            $page = intval($_GET["page"]);
//                        } else {
//                            $page = 1;
//                        }
//                        //last row of record
//                        $end = $perpage * $page;
//                        //first row of record
//                        $offset = $end - $perpage;
//                        $no = $offset + 1;

                        ?>

                        <tbody id="search-results">
                        </tbody>
                    </table>

                    <table class="table" id="page-table" name="page-table">
                        <tr>
                            <td id = "page-results">
                                <?php
//                                if (isset($page)) {
//
//                                    $table = "receipt";
//                                    $whereColumn = "member_id";
//                                    $countRows = getRowCount($conn, $sessionId, $table, $whereColumn);
//
//                                    foreach ($countRows as &$value) {
//                                        //rounding up
//                                        $totalPages = ceil($value['count'] / $perpage);
//
//                                        //PREVIOUS
//                                        if ($page <= 1) {
//                                            echo "<span class = 'current-page'><b>Previous</b></span>";
//                                        } else {
//                                            $pageNo = $page - 1;
//                                            echo "<span><a class = 'link-page' href='profile?page=$pageNo'><b>< Previous</b></a></span>";
//                                        }
//
//                                        //BETWEEN
//                                        for ($x = 1; $x <= $totalPages; $x++) {
//                                            if ($x <> $page) {
//                                                echo "<span><a class = 'link-page' href='profile?page=$x'>$x</a></span>";
//                                            } else {
//                                                echo "<span class ='current-page' style='font-weight: bold;'>$x</span>";
//                                            }
//                                        }
//
//                                        //NEXT
//                                        if ($page == $totalPages) {
//                                            echo "<span class ='current-page'>Next</span>";
//                                        } else {
//                                            $pageNo = $page + 1;
//                                            echo "<span><a class ='link-page' href='profile?page=$pageNo'>Next ></a></span>";
//                                        }
//                                    }
//                                }
                                $conn->close();
                                ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
</div>
<!--Began another section -->
<script src="dist/js/bootstrap.min.js"></script>
<script type="text/javascript">

    jQuery(document).ready(function () {
        getMemberReport();
        getPageResults();
        $("page-table").hide();
    });

    $("#sort-by-cb").change(function () {
        getMemberReport();
        getPageResults();
        return false;
    });

    function showReceiptDetails(receiptId){
        var where1 = "full";
        $.post('getCashbackReport', {
            receiptId: receiptId,
            where1: where1
        }, function (data) {
            $("#receiptDetailDiv").html(data);
        });

        $('#receiptDetailModal').modal('show');
    }

    <?php //from here https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js ?>
    var getUrlParameter = function getUrlParameter(param) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === param) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var getMemberReport = function getMerchantReport() {
        var sortBy = $('#sort-by-cb').find(":selected").text();
        var perpage = 10;
        var currentPage = null;
        var where1 = "member";
        if (getUrlParameter('page') !== null) {
            currentPage = getUrlParameter('page');
        }else{
            currentPage = 1;
        }

        $.post('getCashbackReport', {
            sortBy: sortBy,
            perpage: perpage,
            currentPage: currentPage,
            where1: where1
        }, function (data) {
            $("#search-results").html(data);
        });

        $("page-table").show();
    };

    var getPageResults = function getPageResults() {
        var sortBy = $('#sort-by-cb').find(":selected").text();
        var perpage = 10;
        var currentPage = null;
        var where1 = "member";

        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (getUrlParameter('page') !== null) {
            currentPage = getUrlParameter('page');
        }

        $.post('getPageResults', {
            sortBy: sortBy,
            perpage: perpage,
            currentPage: currentPage,
            where1: where1,
            day: day,
            month: month,
            year: year
        }, function (data) {
            $("#page-results").html(data);
        });
    };

    function changeMerchant(value) {
        document.getElementById('merchant-id').value = value;
    }

    function changeNgo(value) {
        document.getElementById('ngo-id').value = value;
    }
</script>
<?php include 'foot.php'; ?>
</body>

</html>
