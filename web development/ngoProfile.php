<?php
session_start();
require_once "php-files/usefulFunction.php";
if (isLogin()){
    require_once "php-files/conDb.php";
    $sessionId = $_SESSION["id"];
    $ngoId = $_SESSION["ngo-id"];

    if($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['ngo-id'])){

        if(validateReallyThisNgo($conn,$sessionId,$_POST['ngo-id'])){
            $_SESSION["ngo-id"] = $_POST['ngo-id'];
            $ngoId = $_SESSION["ngo-id"];
        }else{
            header("Location: profile");
            $conn->close();
            exit();
        }
    }

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if (isset($_POST["change-pin-no-btn"])) {
            $newPinNo = rewrite($_POST['new-pin-no']);
            if (isCorrectPinFormat($newPinNo)) {
                if ($stmt = $conn->prepare("UPDATE ngo SET pin_no = ? WHERE id = ?")) {
                    $stmt->bind_param("ii", $newPinNo, $ngoId);
                    $stmt->execute();
                    $stmt->close();
                    echo '<script type="text/javascript">'
                    , 'alert("New pin number updated successfully.");'
                    , '</script>';
                }
            } else {
                echo '<script type="text/javascript">'
                , 'alert("Please create a pin number with 6 number digits");'
                , '</script>';
            }
        }
    }

    $sql = "SELECT logo_link, name, email, phone_number, approval_status, pin_no  FROM ngo WHERE id = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $ngoId);

        $stmt->execute();

        $stmt->bind_result($rsLogoLink, $rsName, $rsEmail, $rsPhoneNo, $rsApprovalStatus, $rsPinNo);

        $stmt->fetch();

        $stmt->close();
    }

    $sql = "SELECT SUM(amount) FROM donation WHERE ngo_id = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $ngoId);

        $stmt->execute();

        $stmt->bind_result($rsTotalDonation);

        $stmt->fetch();

        $stmt->close();
    }
    $conn->close();
} else {
    header("Location: login");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <title>NGO Profile</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="main-style.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!-- [if lt IE 9]> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="assets/js/ie8-responsive-file-warning.js"></script>
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
</head>
<style>
    .footer {
        border-top: 1px solid #d3d3d3;
    }

    .modal-header {
        background: #9c77b4;
        color: #fff;
    }

    select {
        border: 0;
        outline: 0;
        font-family: 'MyWebFont', 'vagrounded-regular', sans-serif;
    }

    .select {
        font-family: 'MyWebFont', 'vagrounded-regular', sans-serif;
    }
</style>

<body>
<?php include 'navProfile.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 centerise">
            <div class="text-center marginleftminus">
                <?php
                if ($rsLogoLink != null) {
                    echo '<img src="/' . $rsLogoLink . '" class="img-thumbnail" alt="profile-image">';
                } else {
                    echo '<img src="./images/folio.png" class="img-thumbnail" alt="profile-image">';
                }
                ?>

                <h4><?php echo $rsName ?></h4>
                <div class="text-center">
                    <p class="text-muted">(+60)
                        <?php echo $rsPhoneNo; ?>
                    </p>
                    <p class="text-muted">
                        <?php echo $rsEmail; ?>
                    </p>
                    <p class="text-muted">
                        Approval Status: <?php echo $rsApprovalStatus; ?>
                    </p>
                </div>
            </div>
        </div>
        <!-- end col -->
        <div class="col-md-9 col-lg-5 text-center m10">
            <div class="col-md-4 col-lg-4 col-md-push-2 col-xs-4 col-xs-push-1 text-center width-size cashback-size">
                <p style="font-size:1.5em;" class="select normallineheight "> Total Contribution Received</p>
                <p class="text-price text-price-three select resizeRM pinkfont mm ca">RM<?php echo $rsTotalDonation;?>
                </p>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again1" style="z-index: 1;">
                <a href="merchantRegistration" class="btn purple-merchant select purplemerchant">Register As
                    Merchant</a>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again3" style="z-index: 0;">
                <a href="ngoRegistration" class="btn purple-merchant select purplemerchant mobilengo"style="margin-top: 50px">Register As NGO</a>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-login overwrite-again4 v4">
                <button class="btn purple select buttomwidth reg1" data-toggle="modal"
                        data-target="#changePinModal">Change PIN Number</button>
            </div>

            <div class="modal fade" id="changePinModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Change your pin number</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="change-pin-form">
                                <input type="password" class="form-controls" id="new-pin-no"
                                       name="new-pin-no" placeholder="Enter new pin number">
                                <input id="change-pin-no-btn" name="change-pin-no-btn" type="submit" class="btn btn-default btn-min-register reg1"
                                       value="Submit" style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-lg-9 col-xs-12 divsize">
            <h1 class="trans"> Transaction History</h1>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:#2ab574;" class="select"> &#x25cf; Approved</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p class="select"> &#x25cf; Pending</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:red" class="select"> &#x25cf; Rejected</p>
            </div>
            <div class="sortby">
                <div style="display:inline-block;">
                    <p class="select">Sort by</p>
                </div>
                <div class="col-md-5 col-lg-2 col-md-push-8  col-xs-3 col-xs-push-9 latest" name = "sort-by-cb" id ="sort-by-cb">
                    <select>
                        <option>Newest</option>
                        <option>Oldest</option>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="approveForm">

                        <table class="table">

                            <?php

                                //besides changing this php $perpage, need change the jquery ajax also below >>> var perpage = 10;
                                $perpage = 100;

                                if(isset($_GET["page"])){
                                    $page = intval($_GET["page"]);
                                }
                                else {
                                    $page = 1;
                                }
                                //last row of record
                                $end = $perpage * $page;
                                //first row of record
                                $offset = $end - $perpage;
                                $no = $offset + 1;

                            ?>

                            <tbody id ="search-results">

                                    <?php
//                                        $where = "";
//                                        $sortBy = "DESC";
//                                        $rows = getCashbackReport($conn, $merchantId, $offset, $perpage, $where, $sortBy);
//
//                                        if($rows == null){
//                                            echo '<tr>
//                                                       <td>Sorry, no records found</td>
//                                                  </tr>';
//                                        }else{
//                                            foreach ($rows as &$value) {
//                                                echo '<tr>';
//                                                echo '<td>' . $value['timestamp'] . '</td>';
//                                                echo '<td> Cashback for ' . $value['member-name'] . '</td>';
//                                                echo '<td> Created by ' . $value['cashier-name'] . '</td>';
//                                                echo '<td>' . $value['cashback-amount'] . '</td>';
//                                                echo '<td>' . $value['merchant-approval-status'] . '</td>';
//                                                if(!strcmp($value['merchant-approval-status'], "approved") == 0){
//                                                    echo '<td><input type="checkbox" name="approveCB[]" value="' . $value['id'] . '">Approve</td>';
//                                                }
//                                                echo '</tr>';
//                                            }
//                                        }
                                    ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div>
</div>

<script type="text/javascript">

    jQuery(document).ready(function(){
        getNgoReport();
        $("page-table").hide();
    });

    $("#sort-by-cb").change(function() {
        getNgoReport();
        return false;
    });

    <?php //from here https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js ?>
    var getUrlParameter = function getUrlParameter(param) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === param) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var getNgoReport = function getNgoReport() {
        var sortBy = $('#sort-by-cb').find(":selected").text();
        var perpage = 100;
        var currentPage = null;
        if(getUrlParameter('page') !== null){
            currentPage = getUrlParameter('page');
        }

        $.post('getDonationReport',{sortBy:sortBy, perpage: perpage, currentPage: currentPage}, function(data){
            $("#search-results").html(data);
        });

        $("page-table").show();
    };

</script>

<!--Began another section -->
<script src="dist/js/bootstrap.min.js"></script>
<?php include 'foot.php'; ?>
</body>

</html>
