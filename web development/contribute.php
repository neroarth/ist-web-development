<!DOCTYPE html>
<html lang="en">

<head>
  <title>Save</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
  <meta name="description" content="ISpendTribute">
  <meta name="author" content="Spending tribute">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css" />
  <link href="main-style.css" rel="stylesheet">
  <script src="dist/js/bootstrap.min.js"></script>
  <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  <style>
    .text-line {
      background-color: transparent;
      color: black;
      outline: none;
      outline-style: none;
      outline-offset: 0;
      border-top: none;
      border-left: none;
      border-right: none;
      border-bottom: 1px solid #f3f3f3;
      padding: 3px 10px;
    }

    .form-controls {
      box-shadow: none;
      border: 0;
      border-color: transparent;
      background-color: transparent;
    }

    .form-control {
      border-radius: 0!important;
    }

    .form-controls::-webkit-input-placeholder {
      color: black;
    }

    .form-controls:-moz-placeholder {
      color: black;
    }

    .form-controls:-ms-input-placeholder {
      color: black;
    }

    .form-control {
      border-bottom: 1px solid black;
    }

    .datepicker {
      background-color: #fff;
      color: #333;
    }

    .btn-group {
      width: 180px;
      padding-right: 0;
      padding-left: 0;
    }

    .btn-default {
      width: 35px;
      border-radius: 0;
    }
    li ,a{list-style-type: none;
        color:#f291bb;
        }
        .btn-default{
          color:#f291bb;
        }
  </style>
</head>

<body>
  <?php include 'navProfile.php'; ?>
  <div class="container">
    <div class="col-lg-3 col-md-9 col-xs-8">
      Filter by
      <a href="#"><img src="./images/filter_1.png" width="20px" height="20px"></a>
      <a href="#"><img src="./images/filter_2.png" width="20px" height="20px"></a>
      <div class="text-justify">
        <li> <h3> Category</h3> </li>
        <li><a href="#">Accesories</a></li>
        <li><a href="#">Clothing</a></li>
        <li><a href="#">Electrical</a></li>
        <li><a href="#">Entertainment</a></li>
      </div>
      <li> <h4> Letters</h4> </li>
        <div class="col-md-3 col-xs-2 btn-group">
          <button class="btn btn-default">A</button>
          <button class="btn btn-default">B</button>
          <button class="btn btn-default">C</button>
          <button class="btn btn-default">D</button>
          <button class="btn btn-default">E</button>
          <button class="btn btn-default">F</button>
          <button class="btn btn-default">G</button>
          <button class="btn btn-default">H</button>
          <button class="btn btn-default">I</button>
          <button class="btn btn-default">J</button>
          <button class="btn btn-default">K</button>
          <button class="btn btn-default">L</button>
          <button class="btn btn-default">M</button>
          <button class="btn btn-default">N</button>
          <button class="btn btn-default">O</button>
          <button class="btn btn-default">P</button>
          <button class="btn btn-default">Q</button>
          <button class="btn btn-default">R</button>
          <button class="btn btn-default">S</button>
          <button class="btn btn-default">T</button>
          <button class="btn btn-default">U</button>
          <button class="btn btn-default">V</button>
          <button class="btn btn-default">W</button>
          <button class="btn btn-default">X</button>
          <button class="btn btn-default">Y</button>
          <button class="btn btn-default">Z</button>
        </div>
      </div>
    <div class="row">
      <div class="col-lg-8">
        <table class="table">
            <!--Table body-->
            <tbody>
                <!--First row-->
                <tr>
                    <td>
                      <div class="col-md-3 col-xs-3">
                        <img src="./images/rounded.png" class="img-responsive">
                      </div>
                      Blue
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-primary" >Donate</button>
                    </td>
                </tr>
                <!--/First row-->

                <!--Second row-->
                <tr>
                    <td>
                      <div class="col-md-3 col-xs-3">
                        <img src="./images/rounded.png" class="img-responsive">
                      </div>
                      Blue
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-primary" >Donate</button>
                    </td>
                </tr>
                <!--/Second row-->

                <!--Third row-->

                <!--Third row-->

                <!--Fourth row-->
            </tbody>
            <!--/Table body-->
        </table>
    </div>
    <!--/Shopping Cart table-->
      </div>
    </div>
  </div>
  <div tabindex="-1" class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button class="close" type="button" data-dismiss="modal">×</button>
          <h3 class="modal-title">Heading</h3>
        </div>
        <div class="modal-body"></div>
        <div class="modal-footer">
          <button class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <?php include 'foot.php';?>
  <script>
  <script>
      $(document).ready(function() {
          $('.thumbnail').click(function() {
              $('.modal-body').empty();
              var title = $(this).parent('a').attr("title");
              $('.modal-title').html(title);
              $($(this).parents('div').html()).appendTo('.modal-body');
              $('#myModal').modal({
                  show: true
              });
            });
            var date_input=$('input[name="date"]'); //our date input has the name "date"
            var options={
              format: 'dd/mm/yyyy',
              todayHighlight: true,
              autoclose: true,
            };
            date_input.datepicker(options);

      });
  </script>
</body>

</html>
