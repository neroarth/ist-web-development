<?php
session_start();
require_once "php-files/usefulFunction.php";
if (isset($_GET["logout"])) {
    logout();
}
if (isLogin() && isMerchantLogin()) {
    require_once "php-files/conDb.php";
    $sessionId = $_SESSION["id"];
    $merchantId = $_SESSION["merchant-id"];

    $cashierRow = getCashierList($conn, $merchantId);

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['merchant-id'])) {

        if (validateReallyThisMerchant($conn, $sessionId, $_POST['merchant-id'])) {
            $_SESSION["merchant-id"] = $_POST['merchant-id'];
            $merchantId = $_SESSION["merchant-id"];
        } else {
            header("Location: profile");
            $conn->close();
            exit();
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['pinConfirmBtn'])) {
        if (!empty($_POST['approveCB'])) {
            $pinNo = $_POST['pin-no'];

            /*****************GET MERCHANT ID & PIN NUMBER FROM CASHIER********************/
            $sql = "SELECT  pin_no FROM merchant WHERE id = ?";

            if ($stmt = $conn->prepare($sql)) {

                $stmt->bind_param("i", $_SESSION['merchant-id']);

                $stmt->execute();

                $stmt->bind_result($rsPinNo);

                $stmt->fetch();

                $stmt->close();

            }

            if (!strcmp($pinNo, $rsPinNo) == 0) {
                echo '<script type="text/javascript">'
                , 'alert("Incorrect pin number!");'
                , '</script>';
            } else {
                $approveStr = "approved";
                $hasFailed = false;

//                /*****************GET CASHBACK RATE FROM MERCHANT BASED ON THE MERCHANT ID********************/
//                $sql = "SELECT rate FROM `cashback_rate` WHERE merchant_id = ? ORDER BY id DESC LIMIT 1";
//
//                if ($stmt = $conn->prepare($sql)) {
//
//                    $stmt->bind_param("i", $merchantId);
//
//                    $stmt->execute();
//
//                    $stmt->bind_result($cashbackRate);
//
//                    $stmt->fetch();
//
//                    $stmt->close();
//
//                }

                $isEnoughMerchantCredit = true;

                $totalMerchantCredit = 0;
                $currentMerchantCredit = 0;
                $usedMerchantCredit = 0;

                /*****************GET MERCHANT CREDIT TO CHECK ENOUGH BO (ADD)********************/
                $sql = "SELECT SUM(amount) FROM `merchant_credit` WHERE merchant_id = ? AND type = 'add'";

                if ($stmt = $conn->prepare($sql)) {

                    $stmt->bind_param("i", $merchantId);

                    $stmt->execute();

                    $stmt->bind_result($currentMerchantCredit);

                    $stmt->fetch();

                    $stmt->close();
                }

                /*****************GET MERCHANT USED CREDIT TO CHECK ENOUGH BO (SUBTRACT)********************/
                $sql = "SELECT SUM(amount) FROM `merchant_credit` WHERE merchant_id = ? AND type = 'sub'";

                if ($stmt = $conn->prepare($sql)) {

                    $stmt->bind_param("i", $merchantId);

                    $stmt->execute();

                    $stmt->bind_result($usedMerchantCredit);

                    $stmt->fetch();

                    $stmt->close();
                }

                $currentMerchantCredit -= $usedMerchantCredit;

                foreach ($_POST['approveCB'] as $receiptId) {
                    /*****************GET EACH CASHBACK AMOUNT FROM THE RECEIPT********************/
                    $sql = "SELECT cashback_amount FROM receipt WHERE id = ?";

                    if ($stmt = $conn->prepare($sql)) {

                        $stmt->bind_param("i", $receiptId);

                        $stmt->execute();

                        $stmt->bind_result($thisCashbackAmount);

                        $stmt->fetch();

                        $stmt->close();
                    }

                    //please search for this word "change management fee" if want to change their management fee value
                    $totalMerchantCredit += ($thisCashbackAmount + ($thisCashbackAmount * (12 / 100) ) );
                }

                if($currentMerchantCredit < $totalMerchantCredit){
                    $isEnoughMerchantCredit = false;
                    $calculatedMerchantCredit = $totalMerchantCredit - $currentMerchantCredit;
                    $merchantCreditError = "Not enough merchant credit, RM$calculatedMerchantCredit is needed!\n
                                            Please reload by contacting IST's staff";
                }

                if($isEnoughMerchantCredit){
                    //here hacker can fake the numbers so in future need do some checking first before approving and updating cashback
                    foreach ($_POST['approveCB'] as $receiptId) {
                        $sql = "UPDATE receipt SET merchant_approval_status = ? WHERE id = ?";

                        if ($stmt = $conn->prepare($sql)) {
                            /******************************UPDATE RECEIPT TO APPROVE*******************************************/
                            $stmt->bind_param("si", $approveStr, $receiptId);

                            $stmt->execute();

                            $stmt->close();

                            /******************************START CASHBACK CALCULATION AND UPDATING TO DATABASE***************************************/

                            /*******************************GET RECEIPT DETAILS************************************************/

                            $memberId = null;
                            $amountSpent = 0;

                            $rows = getReceiptDetail($conn, $receiptId);
                            foreach ($rows as &$value) {
                                $memberId = $value['member-id'];
                                $amountSpent = $value['amount-spent'];
                                $payWithCredit = $value['pay-with-credit'];
                                $cashbackAmount = $value['cashback-amount'];
                            }

                            /*****************CALCULATING CASHBACK AMOUNTS************************/
//                            $totalAmount = $amountSpent * ($cashbackRate / 100);//old
                            $totalAmount = $cashbackAmount;//new

                            $personalAmount = $totalAmount * (90 / 100);

                            $charityAmount = $totalAmount * (10 / 100);

                            $previousCashbackAmounts = getMemberCashbackAmount($memberId, $conn);

                            $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $personalAmount;
                            $currentCharityAmount = $previousCashbackAmounts['current-charity-amount'] + $charityAmount;
                            $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $personalAmount;
                            $totalCharityAmount = $previousCashbackAmounts['total-charity-amount'] + $charityAmount;

                            /**************************INSERT ALL CASHBACK AMOUNTS INTO MEMBER TABLE 1ST******************************/
                            $sql = 'UPDATE member SET current_cashback_amount = ?, current_charity_amount = ?, total_cashback_amount = ?, total_charity_amount = ? WHERE id = ?';

                            if ($stmt = $conn->prepare($sql)) {

                                $stmt->bind_param("ddddi", $currentCashbackAmount, $currentCharityAmount, $totalCashbackAmount, $totalCharityAmount, $memberId);

                                $stmt->execute();

                                $stmt->close();
                            }

                            /**************************NOW INSERT DATA INTO CASHBACK TABLE******************************/
                            //PERSONAL CASHBACK
                            $sql = "INSERT INTO cashback (type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?)";
                            $type = "normal cashback";

                            if ($stmt = $conn->prepare($sql)) {

                                $stmt->bind_param("sdii", $type, $personalAmount, $memberId, $receiptId);

                                $stmt->execute();

                                $stmt->close();
                            }

                            //CHARITY CASHBACK
                            $sql = "INSERT INTO cashback (type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?)";
                            $type = "charity cashback";

                            if ($stmt = $conn->prepare($sql)) {

                                $stmt->bind_param("sdii", $type, $charityAmount, $memberId, $receiptId);

                                $stmt->execute();

                                $stmt->close();
                            }

                            //IST MANAGEMENT FEE CASHBACK
                            //please search for this word "change management fee" if want to change their management fee value
                            $managementFees = $totalAmount * (12 / 100);

                            $previousCashbackAmounts = getAdminCashbackAmount("IST", $conn);
                            $istId = $previousCashbackAmounts['id'];
                            $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $managementFees;
                            $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $managementFees;

                            /*******************************NOW INSERT INTO MERCHANT CREDIT TO SUBTRACT HIS CREDIT****************************/
                            $sql = "INSERT INTO merchant_credit (amount, type, merchant_id, receipt_id) VALUES (? ,? ,?, ?)";

                            if($stmt = $conn->prepare($sql)){
                                $tempMerchantCredit = $managementFees + $totalAmount;
                                $type = "sub";
                                $stmt->bind_param("dsii", $tempMerchantCredit, $type, $merchantId, $receiptId);

                                $stmt->execute();

                                $stmt->close();
                            }

                            //insert ist management fee into cashback
                            $sql = "INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?, ?)";
                            $type = "management fee";

                            if ($stmt = $conn->prepare($sql)) {

                                $stmt->bind_param("isdii", $memberId, $type, $managementFees, $istId, $receiptId);

                                $stmt->execute();

                                $stmt->close();
                            }

                            //insert ist's updated cashback amount into member
                            $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE category = "IST"';

                            if ($stmt = $conn->prepare($sql)) {

                                $stmt->bind_param("dd", $currentCashbackAmount, $totalCashbackAmount);

                                $stmt->execute();

                                $stmt->close();
                            }

                            /**************************HERE AWARDS THE 1% TO CONNECT*************************************************************************/
                            $sql = 'SELECT connect_id FROM merchant WHERE id = ?';

                            if ($stmt = $conn->prepare($sql)) {

                                $stmt->bind_param("i", $merchantId);

                                $stmt->execute();

                                $stmt->bind_result($connectId);

                                $stmt->fetch();

                                $stmt->close();

                            }

                            $connectAmount = $totalAmount * (1 / 100);
                            $type = "connect";

                            //NOW set up sql 1st and calculate the cashback/earning amount 1st
                            if ($connectId == NULL) {
                                //go to ist
                                $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE category = "IST"';
                                $previousCashbackAmounts = getAdminCashbackAmount("IST", $conn);
                                $istId = $previousCashbackAmounts['id'];
                                $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $connectAmount;
                                $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $connectAmount;
                                $sqlCashback = 'INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ' . $istId . ', ?)';
                            } else {
                                //go to member
                                $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE id = ' . $connectId;
                                $previousCashbackAmounts = getMemberCashbackAmount($connectId, $conn);
                                $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $connectAmount;
                                $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $connectAmount;
                                $sqlCashback = 'INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ' . $connectId . ', ?)';
                            }

                            //Now update into member table

                            if ($stmt = $conn->prepare($sql)) {

                                $stmt->bind_param("dd", $currentCashbackAmount, $totalCashbackAmount);

                                $stmt->execute();

                                $stmt->close();
                            }

                            //Now insert into cashback table
                            if ($stmt = $conn->prepare($sqlCashback)) {

                                $stmt->bind_param("isdi", $memberId, $type, $connectAmount, $receiptId);

                                $stmt->execute();

                                $stmt->close();
                            }

                            /**************************HERE CALLS THE RECURSIVE FUNCTION TO RECORD ALL REFERRAL AND MM CASHBACK DATA******************************/
                            $noofTimes = 1;
                            $result = updateReferralCashback($memberId, $memberId, $noofTimes, $totalAmount, $receiptId, $conn);

                            if (strcmp($result, "fail") == 0) {
                                $hasFailed = true;
                            }

                            /**********************************FINISH********************************************/
                            if (isset($_GET['page'])) {
                                header("Location: merchantProfile?page=" . $_GET['page']);
                            }
                        }

                    }

                    //refresh the page and shows the message
                    if ($hasFailed) {
                        header("Location: merchantProfile?msg=fail");
                    } else {
                        header("Location: merchantProfile?msg=success");
                    }
                }
            }
        } else {
            echo '<script type="text/javascript">'
            , 'alert("You didn\'t select anything");'
            , '</script>';
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST["change-pin-no-btn"])) {
            $newPinNo = rewrite($_POST['new-pin-no']);
            if (isCorrectPinFormat($newPinNo)) {
                if ($stmt = $conn->prepare("UPDATE merchant SET pin_no = ? WHERE id = ?")) {
                    $stmt->bind_param("ii", $newPinNo, $merchantId);
                    $stmt->execute();
                    $stmt->close();
                    echo '<script type="text/javascript">'
                    , 'alert("New pin number updated successfully.");'
                    , '</script>';
                }
            } else {
                echo '<script type="text/javascript">'
                , 'alert("Please create a pin number with 6 number digits");'
                , '</script>';
            }
        } else if (isset($_POST["change-cashback-rate-btn"])) {
            $cashbackRate = rewrite($_POST['cashback-rate']);

            $sql = "INSERT INTO cashback_rate (merchant_id, rate) VALUES (?, ?)";

            if ($stmt = $conn->prepare($sql)) {

                $stmt->bind_param("id", $merchantId, $cashbackRate);

                $stmt->execute();

                $stmt->close();

                echo '<script type="text/javascript">'
                , 'alert("Cashback rate changed successfully");'
                , '</script>';
            } else {
                echo '<script type="text/javascript">'
                , 'alert("Please enter values from 0% to 100% for cashback rate");'
                , '</script>';
            }
        } else if (isset($_POST['deactivate-cashier-btn'])) {
            if (!empty($_POST['deactivateCashierCB'])) {
                $cashierFailed = false;
                $deactivateSql = "UPDATE cashier SET active_status = 0 WHERE username = ?";
                foreach ($_POST['deactivateCashierCB'] as $cashierId) {
                    if ($deactivateStmt = $conn->prepare($deactivateSql)) {
                        $deactivateStmt->bind_param("s", $cashierId);

                        $deactivateStmt->execute();

                        $deactivateStmt->close();
                    } else {
                        $cashierFailed = true;
                    }
                }

                if ($cashierFailed) {
                    header("Location: merchantProfile?cashierMsg=fail");
                } else {
                    header("Location: merchantProfile?cashierMsg=success");
                }
            } else {
                echo '<script type="text/javascript">'
                , 'alert("You didn\'t select anything");'
                , '</script>';
            }
        }
    }

    $rows = calculateCashbacks($conn, "merchant_id", $merchantId);
    $merchantTotalAmountEarned = 0;
    $merchantTotalCashbackGiven = 0;
    foreach ($rows as &$value) {
        $merchantTotalAmountEarned += $value['total-amount-spent'];
        $merchantTotalCashbackGiven += $value['total-cashback-amount'];
    }

    $sql = "SELECT logo_link, shop_name, email, phone_number, approval_status, pin_no  FROM merchant WHERE id = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $merchantId);

        $stmt->execute();

        $stmt->bind_result($rsLogoLink, $rsShopName, $rsEmail, $rsPhoneNo, $rsApprovalStatus, $rsPinNo);

        $stmt->fetch();

        $stmt->close();
    }

    if (isset($_GET['msg'])) {
        if (strcmp($_GET['msg'], "fail") == 0) {
            $msg = "Failed to approve certain receipts";
        } else {
            $msg = "Approved all receipts successfully";
        }

        echo '<script type="text/javascript">';
        echo 'alert("' . $msg . '");';
        echo '</script>';
    }

    if (isset($_GET['cashierMsg'])) {
        if (strcmp($_GET['cashierMsg'], "fail") == 0) {
            $cashierMsg = "Failed to deactivate certain cashier account";
        } else {
            $cashierMsg = "Deactivated selected cashier accounts successfully";
        }

        echo '<script type="text/javascript">';
        echo 'alert("' . $cashierMsg . '");';
        echo '</script>';
    }

} else {
    header("Location: login");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <title>Merchant Profile</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="main-style.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!-- [if lt IE 9]> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="assets/js/ie8-responsive-file-warning.js"></script>
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
</head>
<style>
    .footer {
        border-top: 1px solid #d3d3d3;
    }

    .modal-header {
        background: #9c77b4;
        color: #fff;
    }

    select {
        border: 0;
        outline: 0;

    }

    textarea.purple-line {
        color: #9c77b4 !important;

    }

    .purple-line::placeholder {
        color: #9c77b4 !important;

    }

    ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #9c77b4 !important;
    }

    ::-moz-placeholder { /* Firefox 19+ */
        color: #9c77b4 !important;
    }

    :-ms-input-placeholder { /* IE 10+ */
        color: #9c77b4 !important;
    }

    :-moz-placeholder { /* Firefox 18- */
        color: #9c77b4 !important;
    }

    .block {
        width: 100%;
        height: 20%;
        border: 30px solid #fff;
        padding-bottom: 20px;
    }
</style>

<body>
<?php include 'navProfile.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-lg-3 col-md-4 centerise">
            <div class="text-center marginleftminus cancelmarginleft2">
                <?php
                if ($rsLogoLink != null) {
                    echo '<img src="/' . $rsLogoLink . '" class="img-thumbnail" alt="profile-image">';
                } else {
                    echo '<img src="./images/folio.png" class="img-thumbnail" alt="profile-image">';
                }
                ?>

                <h4><?php echo $rsShopName ?></h4>
                <div class="text-center">
                    <p class="text-muted">(+60)
                        <?php echo $rsPhoneNo; ?>
                    </p>
                    <p class="text-muted">
                        <?php echo $rsEmail; ?>
                    </p>
                    <p class="text-muted">
                        Approval Status: <?php echo $rsApprovalStatus; ?>
                    </p>
                </div>
                <?php
                if (isLogin() || isAdminLogin() || isCashierLogin()) {
                    echo '<div class="btn-nav logina"><a class="btn btn-small navbar-btn pink" href="index.php?logout=true">Logout</a></div>';
                } else {
//                            echo '<div class="btn-nav col-xs-1 logina2"><li><a class="btn btn-small navbar-btn pink-two" href="memberRegistration.php"><span style="color:#ffffff">Register</span></a></li></div>';
                    header("Location: login");
                    exit();
                }
                ?>
            </div>
        </div>
        <!-- end card-box -->
        <!-- <div class="panel panel-default">
        <h4 class="m-t-0 m-b-20 header-title">Skills</h4>
        <div class="p-b-10">

            <div class="progress progress-sm m-b-0">
                <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                </div>
            </div>
              <p>Progression of account</p>
        </div>

    </div> -->

        <!-- end col -->
        <div class="col-md-9 col-lg-5 text-center m10cancelmarginleft2 addmleft">
            <div class="col-md-4 col-lg-4 col-md-push-2 col-xs-4 col-xs-push-1 text-center width-size cashback-size">
                <p style="font-size:1.5em;" class="select normallineheight minheight sc-text linea1"> Total Amount Spent by
                    Members </p>
                <div class="group-value">
                    <p class="text-price-two select resizeRM cash-text sc ">
                        RM<?php echo $merchantTotalAmountEarned ?> </p>
                </div>

                <p style="font-size:1.5em;" class="select normallineheight minheight sc-text"> Management Fee </p>
                <div class="group-value">
                    <p class="text-price-two select resizeRM cash-text sc " id = "management-fee-result">
                    </p>
                </div>
            </div>
            <div class="verticalLine col-md-2 col-xs-1 col-xs-push-2 specialline overwrite-vertical new-line"></div>
            <div class="col-md-4 col-md-push-3 col-xs-4 col-xs-push-3 cashback-size sm-margin">
                <?php
                //                    <button class="btn pinkthree select buttomwidth" data-toggle="modal" data-target="#contribute">
                //                        Change Cashback Value
                //                    </button>
                ?>
                <div class="modal fade" id="contribute" tabindex="-1" role="dialog" aria-labelledby="title">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="title">Cashback value</h4>
                            </div>

                        </div>
                    </div>
                </div>
                <p style="font-size:1.5em;" class="select normallineheight linea1"> Total Cashback Amount Given</p>
                <p class="text-price text-price-three select resizeRM pinkfont mm ca ca2">
                    RM<?php echo $merchantTotalCashbackGiven ?>
                </p>
            </div>

            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again3 u3"
                 style="z-index: 0;">
                <a href="profile.php" class="btn purple-merchant select purplemerchant mobilengo mp4 m-force mobilewidth button01"
                   style="margin-top: 50px">User Profile</a>
            </div>
            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-again1 v3"
                 style="z-index: 1;">
                <a href="cashierRegistration.php" class="btn purple-merchant select purplemerchant reg1">Register As
                    Cashier</a>
            </div>

            <?php
            if ($cashierRow != null) {
                echo '<div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-login overwrite-again4 v4 ex3">
                                <button class="btn purple select buttomwidth reg1" data-toggle="modal"
                                        data-target="#deactivateCashier">Deactivate Cashier</button>
                          </div>';
            }
            ?>

            <div class="modal fade" id="deactivateCashier" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Deactivate Cashier</h4>
                        </div>
                        <div class="modal-body">
                            <?php
                            if ($cashierRow != null) {
                                echo '<form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="deactivate-cashier-form">';
                                echo '<div class="table-responsive">';
                                echo '<table class="table table-bordered">';
                                echo '<tbody>';
                                echo '<thead>';
                                echo '<tr>';
                                echo '<div class="text-center">';
                                echo '<th class="text-center">Username</th>';
                                echo '<th class="text-center">Name</th>';
                                echo '</div>';
                                echo '</tr>';
                                echo '</thead>';
                                $tableData = "";
                                foreach ($cashierRow as &$value) {
                                    $tableData .= '<tr>';
                                    $tableData .= '<td>' . $value['username'] . '</td>';
                                    $tableData .= '<td>' . $value['name'] . '</td>';
                                    $tableData .= '<td>' . '<input type="checkbox" name="deactivateCashierCB[]"  class="checkbox-overwrite"  value="' . $value['username'] . '">Deactivate</td>';
                                    $tableData .= '</tr>';
                                }
                                echo $tableData;
                                echo '</tbody>';
                                echo '</table>';
                                echo '</div>';
                                echo '<input id="deactivate-cashier-btn" name="deactivate-cashier-btn" type="submit" class="btn btn-default btn-min-register reg1"
                                             value="Deactivate" style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"/>';
                                echo '</form>';
                            } else {
                                echo '<tr>
                                            <td>Sorry, you haven\'t registered any single cashier yet</td>
                                          </tr>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant ex1">
                <button class="btn purple select buttomwidth reg1" data-toggle="modal"
                        data-target="#changePinModal">Change PIN Number
                </button>
            </div>

            <div class="modal fade" id="changePinModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Change your pin number</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data"
                                  id="change-pin-form">
                                <input type="password" class="form-controls border-pin" id="new-pin-no"
                                       name="new-pin-no" placeholder="Enter new pin number">
                                <input id="change-pin-no-btn" name="change-pin-no-btn" type="submit"
                                       class="btn btn-default btn-min-register reg1"
                                       value="Submit"
                                       style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-md-push-5 col-xs-4 col-xs-push-2 clearing merchant overwrite-login overwrite-again4 v6 ex2">
                <button class="btn purple select buttomwidth reg1 reg1a" data-toggle="modal"
                        data-target="#changeCashbackRateModal">Change Cashback Rate
                </button>
            </div>

            <div class="modal fade" id="changeCashbackRateModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Change your cashback rate</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data"
                                  id="change-cashback-rate-form">
                                <input type="number" step="0.01" min="0" max="100"
                                       oninvalid="setCustomValidity('Please enter values from 0% to 100% for cashback rate');"
                                       class="form-control form-controls under-line" oninput="setCustomValidity('');"
                                       id="cashback-rate" name="cashback-rate" placeholder="Cashback Rate"
                                       required>
                                <span class="input-group-addon form-controls text-line" id="basic-addon1">%</span>
                                <input id="change-cashback-rate-btn" name="change-cashback-rate-btn" type="submit"
                                       class="btn btn-default btn-min-register reg1"
                                       value="Submit"
                                       style="font-size:13px; padding:0px !important; padding-bottom:3px !important; padding-top:3px !important;"/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="receiptDetailModal" tabindex="-1" role="dialog" aria-labelledby="title">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="title reg1">Receipt Detail</h4>
                        </div>
                        <div class="modal-body" id="receiptDetailDiv" name="receiptDetailDiv">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-lg-9 col-xs-12 divsize">
            <h1 class="trans"> Transaction History</h1>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:#2ab574;" class="select"> &#x25cf; Approved</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p class="select"> &#x25cf; Pending</p>
            </div>
            <div class="col-md-2 col-lg-2 col-xs-4 p0">
                <p style="color:red" class="select"> &#x25cf; Rejected</p>
            </div>
            <div class="sortby">
                <div style="display:inline-block;margin-left: 36%;">
                    <p class="select">Sort by</p>
                </div>
                <div class="col-md-5 col-lg-2 col-md-push-8  col-xs-3 col-xs-push-9 latest" name="sort-by-cb"
                     id="sort-by-cb">
                    <input type="date" name="reportDate" id="reportDate">
                    <select>
                        <option>Newest</option>
                        <option>Oldest</option>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <form action="" class="form-horizontal" method="post" enctype="multipart/form-data"
                          id="approveForm">

                        <div class="modal fade" id="pinConfirmation" tabindex="-1" role="dialog" aria-labelledby="title"
                             style="border-radius:0 !important;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="title">Please enter your 6-digit pin number</h4>
                                </div>
                                <div class="modal-content" style="border-radius:0 !important;">
                                    <div class="modal-body"><p class="security-p">Security</p>
                                        <input type="number" min="100000" max="999999"
                                               style="border-bottom: 1px solid #9c77b4; color:#9c77b4 !important;"
                                               oninvalid="setCustomValidity('Please do not enter a valid 6-digit pin number');"
                                               class="form-control form-controls text-line line1 pline"
                                               oninput="setCustomValidity('');"
                                               id="pin-no" name="pin-no" placeholder="Pin Number"
                                               required>

                                        <p>
                                            <button type="submit" name="pinConfirmBtn" id="pinConfirmBtn"
                                                    class="btn btn-default btn-min-register confirm2-overwrite">Confirm
                                            </button>
                                        </p>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <table class="table">

                            <?php

                            //besides changing this php $perpage, need change the jquery ajax also below >>> var perpage = 10;
//                            $perpage = 10;
//
//                            if (isset($_GET["page"])) {
//                                $page = intval($_GET["page"]);
//                            } else {
//                                $page = 1;
//                            }
//                            //last row of record
//                            $end = $perpage * $page;
//                            //first row of record
//                            $offset = $end - $perpage;
//                            $no = $offset + 1;

                            ?>

                            <tbody id="search-results">

                            <?php
                            //                                        $where = "";
                            //                                        $sortBy = "DESC";
                            //                                        $rows = getCashbackReport($conn, $merchantId, $offset, $perpage, $where, $sortBy);
                            //
                            //                                        if($rows == null){
                            //                                            echo '<tr>
                            //                                                       <td>Sorry, no records found</td>
                            //                                                  </tr>';
                            //                                        }else{
                            //                                            foreach ($rows as &$value) {
                            //                                                echo '<tr>';
                            //                                                echo '<td>' . $value['timestamp'] . '</td>';
                            //                                                echo '<td> Cashback for ' . $value['member-name'] . '</td>';
                            //                                                echo '<td> Created by ' . $value['cashier-name'] . '</td>';
                            //                                                echo '<td>' . $value['cashback-amount'] . '</td>';
                            //                                                echo '<td>' . $value['merchant-approval-status'] . '</td>';
                            //                                                if(!strcmp($value['merchant-approval-status'], "approved") == 0){
                            //                                                    echo '<td><input type="checkbox" class="checkbox-overwrite" name="approveCB[]" value="' . $value['id'] . '">Approve</td>';
                            //                                                }
                            //                                                echo '</tr>';
                            //                                            }
                            //                                        }
                            ?>
                            </tbody>
                        </table>
                    </form>
                </div>


                <table class="table" id="page-table" name="page-table">
                    <tr>
                        <td id="page-results">
                            <?php
//                            if (isset($page)) {
//
//                                $table = "receipt";
//                                $whereColumn = "merchant_id";
//                                $countRows = getRowCount($conn, $sessionId, $table, $whereColumn);
//
//                                foreach ($countRows as &$value) {
//                                    //rounding up
//                                    $totalPages = ceil($value['count'] / $perpage);
//
//                                    //PREVIOUS
//                                    if ($page <= 1) {
//                                        echo "<span class = 'current-page'>Previous</span>";
//                                    } else {
//                                        $pageNo = $page - 1;
//                                        echo "<span><a class = 'link-page' href='merchantProfile?page=$pageNo'>< Previous</a></span>";
//                                    }
//
//                                    //BETWEEN
//                                    for ($x = 1; $x <= $totalPages; $x++) {
//                                        if ($x <> $page) {
//                                            echo "<span><a class = 'link-page' href='merchantProfile?page=$x' >$x</a></span>";
//                                        } else {
//                                            echo "<span class ='current-page' style='font-weight: bold;'>$x</span>";
//                                        }
//                                    }
//
//                                    //NEXT
//                                    if ($page == $totalPages) {
//                                        echo "<span class ='current-page'>Next</span>";
//                                    } else {
//                                        $pageNo = $page + 1;
//                                        echo "<span><a class ='link-page' href='merchantProfile?page=$pageNo'>Next ></a></span>";
//                                    }
//                                }
//                            }
                            $conn->close();
                            ?>
                        </td>
                    </tr>
                </table>
                <button id="approve-button" name="approve-button"
                        class="btn btn-default btn-min-register  confirm1-overwrite"
                        data-toggle="modal" data-target="#pinConfirmation">Confirm Receipt
                </button>

                <div><?php echo $merchantCreditError; ?></div>
            </div>
        </div>
        <!-- end row -->
    </div>
</div>
</div>
</div>
<script type="text/javascript">

    Date.prototype.toDateInputValue = (function() {
        var local = new Date(this);
        local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
        return local.toJSON().slice(0,10);
    });

    jQuery(document).ready(function () {
        if (getUrlParameter('date') != null) {
            document.getElementById("reportDate").defaultValue = getUrlParameter('date');
            //yyyy-MM-dd
//        document.getElementById("reportDate").defaultValue = "2014-02-09";
        }else{
            $('#reportDate').val(new Date().toDateInputValue());
        }
        getMerchantReport();
        getPageResults();
        getManagementFee();
        $("page-table").hide();
    });

    $("#sort-by-cb").change(function () {
        getMerchantReport();
        getPageResults();
        getManagementFee();
        return false;
    });

    $("#reportDate").change(function () {
        getMerchantReport();
        getPageResults();
        getManagementFee();
        return false;
    });

    function showReceiptDetails(receiptId) {
        var where1 = "full";
        $.post('getCashbackReport', {
            receiptId: receiptId,
            where1: where1
        }, function (data) {
            $("#receiptDetailDiv").html(data);
        });

        $('#receiptDetailModal').modal('show');
    }

    <?php //from here https://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js ?>
    var getUrlParameter = function getUrlParameter(param) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === param) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var getMerchantReport = function getMerchantReport() {
        var sortBy = $('#sort-by-cb').find(":selected").text();
        var perpage = 10;
        var currentPage = null;
        var where1 = "merchant";

        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (getUrlParameter('page') !== null) {
            currentPage = getUrlParameter('page');
        }else{
            currentPage = 1;
        }

        $.post('getCashbackReport', {
            sortBy: sortBy,
            perpage: perpage,
            currentPage: currentPage,
            where1: where1,
            day: day,
            month: month,
            year: year
        }, function (data) {
            $("#search-results").html(data);
        });

        $("page-table").show();
    };

    var getManagementFee = function getManagementFee() {
        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        $.post('getDailyManagementFee', {
            day: day,
            month: month,
            year: year
        }, function (data) {
            $("#management-fee-result").html(data);
        });
    };

    var getPageResults = function getPageResults() {
        var sortBy = $('#sort-by-cb').find(":selected").text();
        var perpage = 10;
        var currentPage = null;
        var where1 = "merchant";

        var date = new Date($('#reportDate').val());
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        if (getUrlParameter('page') !== null) {
            currentPage = getUrlParameter('page');
        }

        $.post('getPageResults', {
            sortBy: sortBy,
            perpage: perpage,
            currentPage: currentPage,
            where1: where1,
            day: day,
            month: month,
            year: year
        }, function (data) {
            $("#page-results").html(data);
        });
    };

</script>

<!--Began another section -->
<script src="dist/js/bootstrap.min.js"></script>
<?php include 'foot.php'; ?>
</body>

</html>
