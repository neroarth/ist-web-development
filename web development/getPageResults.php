<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";

        $perpage = rewrite($_POST['perpage']);
        $currentPage = rewrite($_POST['currentPage']);

        if($currentPage == 0){
            $currentPage = 1;
        }else if($currentPage == null){
            $currentPage = 1;
        }

        $day = rewrite($_POST['day']);
        $month = rewrite($_POST['month']);
        $year = rewrite($_POST['year']);
        //yyyy-MM-dd (must have 0 in it if lesser than 10 in date and month for example: 2017-8-8 is wrong must be 2017-08-08)
        if($month < 10){
            $tempMonth = "0" . $month;
        }else{
            $tempMonth = $month;
        }

        if($day < 10){
            $tempDay = "0" . $day;
        }else{
            $tempDay = $day;
        }
        $date = $year . "-" . $tempMonth . "-" . $tempDay;

        //last row of record
        $end = $perpage * $currentPage;
        //first row of record
        $offset = $end - $perpage;
        $no = $offset + 1;

        $table = "receipt";
        if(strcmp($_POST['where1'], "merchant") == 0){
            if(isAdminLogin()){
                $whereId = $_POST['merchantId'];
            }else if(isMerchantLogin()){
                $whereId = $_SESSION["merchant-id"];
            }else{
                $whereId = 0;
            }
            $whereColumn = "merchant_id";
        }else if(strcmp($_POST['where1'], "member") == 0){
            $whereColumn = "member_id";
            if(isLogin()){
                $whereId = $_SESSION["id"];
            }
        }

        $countRows = getRowCount($conn, $whereId, $table, $whereColumn, $day, $month, $year);

        $tableData = "";

        foreach ($countRows as &$value) {
            //rounding up
            $totalPages = ceil($value['count'] / $perpage);

            //PREVIOUS
            if ($currentPage <= 1) {
                $tableData.= "<span class = 'current-page'>Previous</span>";
            } else {
                $pageNo = $currentPage - 1;
                if(strcmp($_POST['where1'], "merchant") == 0){
                    if(isAdminLogin()){
                        $tableData .= "<span><a class = 'link-page' href='adminProfile?page=$pageNo&date=$date&merchantId=$whereId'>< Previous</a></span>";
                    }else{
                        $tableData .= "<span><a class = 'link-page' href='merchantProfile?page=$pageNo&date=$date'>< Previous</a></span>";
                    }

                }else if(strcmp($_POST['where1'], "member") == 0){
                    $tableData .= "<span><a class = 'link-page' href='profile?page=$pageNo&date=$date'>< Previous</a></span>";
                }
            }

            //BETWEEN
            for ($x = 1; $x <= $totalPages; $x++) {
                if ($x <> $currentPage) {
                    if(strcmp($_POST['where1'], "merchant") == 0){
                        if(isAdminLogin()){
                            $tableData .= "<span><a class = 'link-page' href='adminProfile?page=$x&date=$date&merchantId=$whereId' >$x</a></span>";
                        }else{
                            $tableData .= "<span><a class = 'link-page' href='merchantProfile?page=$x&date=$date' >$x</a></span>";
                        }
                    }else if(strcmp($_POST['where1'], "member") == 0){
                        $tableData .= "<span><a class = 'link-page' href='profile?page=$x&date=$date' >$x</a></span>";
                    }
                } else {
                    $tableData .= "<span class ='current-page' style='font-weight: bold;'>$x</span>";
                }
            }

            //NEXT
            if ($currentPage == $totalPages) {
                $tableData .= "<span class ='current-page'>Next</span>";
            } else {
                $pageNo = $currentPage + 1;
                if(strcmp($_POST['where1'], "merchant") == 0){
                    if(isAdminLogin()){
                        $tableData .= "<span><a class ='link-page' href='adminProfile?page=$pageNo&date=$date&merchantId=$whereId'>Next ></a></span>";
                    }else{
                        $tableData .= "<span><a class ='link-page' href='merchantProfile?page=$pageNo&date=$date'>Next ></a></span>";
                    }
                }else if(strcmp($_POST['where1'], "member") == 0){
                    $tableData .= "<span><a class ='link-page' href='profile?page=$pageNo&date=$date'>Next ></a></span>";
                }
            }
        }

        echo $tableData;

		$conn->close();
	}
?>