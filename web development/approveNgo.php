<?php
	session_start();
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		ob_start();
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		$complete = true;
		
		$ngoId = $_POST['ngoList'];
		
		$remarkNgo = rewrite($_POST["remarkNgo"]);
		$remarkNgo = stripSpaces($remarkNgo);
		if(strlen($remarkNgo) > 300){
			$remarkNgoError = "Remark should not be more than 300 characters";
		    $complete = false;
		}
		
		$sql = "UPDATE ngo SET approval_status = ?, remark = ?, admin_id = ? WHERE id = ?";
		
		if (isset($_POST['approveNgo'])) {
			$approvalStatus = "approved";
		}else if(isset($_POST['rejectNgo'])) {
			$approvalStatus = "rejected";
		}
		else{
			header("Location: adminMerchantDetails");
			exit();
		}
		
		if($complete){
			$stmt = $conn->prepare($sql);
			$stmt->bind_param("sssi", $approvalStatus, $remarkNgo, $_SESSION['admin-id'], $ngoId);
	
			$stmt->execute();
			
			header("Location: adminMerchantDetails");
		}else{
			header("Location: adminMerchantDetails?remarkNgoError=$remarkNgoError&remarkNgo=$remarkNgo");
		}
		
		
		
		$conn->close();
		ob_end_clean();
	}
?>