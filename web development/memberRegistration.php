<?php
session_start();
ob_start();
require_once "php-files/conDb.php";
require_once "php-files/usefulFunction.php";

if (isLogin()) {
    header("Location: profile.php");
    exit();
}

$codeFromLink = $_GET['code'];

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    require_once "php-files/recaptchaSecret.php";//recaptcha_secret is in this php file
    $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptcha_secret . "&response=" . $_POST['g-recaptcha-response']);
    $response = json_decode($response, true);

    if ($response["success"] === true) {

        $complete = true;
        $memberComplete = true;
        $referralComplete = true;
        $phoneNoError = null;
        $emailError = null;
        $referralCodeError = null;

        $sql = "SELECT phone_no
						  FROM member WHERE phone_no = ?";

        if ($stmt = $conn->prepare($sql)) {

            /* bind parameters for the ? variable */
            $stmt->bind_param("d", rewrite($_POST['phone-no']));

            /* execute query */
            $stmt->execute();

            /* bind result variables */
            $stmt->bind_result($checkPhoneNo);

            /* fetch value */
            $stmt->fetch();

            /* close statement */
            $stmt->close();
        }

        $sql = "SELECT email
						  FROM member WHERE email = ?";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("s", filter_var(rewrite($_POST['email']), FILTER_SANITIZE_EMAIL));

            /* execute query */
            $stmt->execute();

            /* bind result variables */
            $stmt->bind_result($checkEmail);

            /* fetch value */
            $stmt->fetch();

            /* close statement */
            $stmt->close();
        }
        if (rewrite($_POST["referral-code"]) == NULL) {
            $checkReferralCode = "user bo type any code";
        } else {
            $sql = "SELECT phone_no
						  FROM member WHERE id = ?";

            if ($stmt = $conn->prepare($sql)) {

                $stmt->bind_param("i", rewrite($_POST['referral-code']));

                /* execute query */
                $stmt->execute();

                /* bind result variables */
                $stmt->bind_result($checkReferralCode);

                /* fetch value */
                $stmt->fetch();

                /* close statement */
                $stmt->close();
            }
        }

        if ($checkPhoneNo == NULL && $checkEmail == NULL && $checkReferralCode != NULL) {
            $stmt = $conn->prepare("INSERT INTO member (email, phone_no, full_name, password, 
                                          salt, email_verification_code, date_joined, pin_no, pin_salt) 
                                          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("sisssssss",
                $email, $phoneNo, $fullName,
                $finalPassword, $salt, $emailVerificationCode,
                $dateJoined, $pinNo, $pinSalt);

            /*i - integer
              d - double
              s - string
              b - BLOB*/

            /*********************************************************************EMAIL***************************************************************/
            $email = rewrite($_POST["email"]);
            $email = stripAllWhiteSpaces($email);
            //cleans the email
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            //check whether the email is correct or not
            if (strlen($email) == 0) {
                $emailError = "Your email should not be empty!";
                $complete = false;
            } else if (strlen($email) > 100) {
                $emailError = "Your email can't be more than 100 characters";
                $complete = false;
            } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $emailError = "Incorrect email format!";
                $complete = false;
            }

            /*********************************************************************PHONE NUMBER***************************************************************/
            $phoneNo = rewrite($_POST['phone-no']);
            if (strlen($phoneNo) < 9 || strlen($phoneNo) > 11) {
                $phoneNoError = "Incorrect phone number!";
                $complete = false;
            }
            /*********************************************************************FULL NAME***************************************************************/
            $fullName = rewrite($_POST["full-name"]);
            $fullName = stripSpaces($fullName);
            if (strlen($fullName) == 0) {
                $fullNameError = "Your name should not be empty!";
                $complete = false;
            } else if (strlen($fullName) > 100) {
                $fullNameError = "Your name can't be more than 100 characters";
                $complete = false;
            }

            /*********************************************************************PASSWORD***************************************************************/
            $password = $_POST["password"];
            if (!preg_match("/^[a-zA-Z0-9]+$/", $password)) {
                $passwordError = "Only letters and numbers allowed (no spaces too)";
                $complete = false;
            } else if (strlen($password) < 8) {
                $passwordError = "Password length needs to be at least 8";
                $complete = false;
            } else {
                $password = hash('sha256', $password);
                //adds a random salt with random number from 0 to 100
                $salt = substr(sha1(mt_rand()), 0, 100);
                $finalPassword = hash('sha256', $salt . $password);
            }

            /*********************************************************************CONFIRM PASSWORD***************************************************************/
            if (strcmp($_POST["password"], $_POST["confirmation-password"]) != 0) {
                $confirmPasswordError = "Password is not the same";
                $complete = false;
            }

            /*********************************************************************REFERRAL CODE***************************************************************/
            $referralCode = rewrite($_POST["referral-code"]);
            if ($referralCode == NULL) {
                $hasReferralCode = false;
            } else {
                $hasReferralCode = true;
            }
            /*********************************************************************EMAIL VERIFICATION CODE***************************************************************/
            $emailVerificationCode = substr(md5(microtime()), rand(0, 26), 6);

            /*********************************************************************DATE JOINED***************************************************************/
            date_default_timezone_set('Asia/Kuala_Lumpur');
            $dateJoined = date('Y-m-d H:i:s');


            if ($complete) {
                $pinNo = mt_rand(100000,999999);

//                //to encrypt the pin number
//                $randomNumber = mt_rand(100000,999999);
//                $pinNo = hash('sha256',$randomNumber);
//                //adds a random salt with random number from 0 to 100
//                $pinSalt = substr(sha1(mt_rand()), 0, 100);
//                $finalPinNo = hash('sha256', $pinSalt.$pinNo);

                if ($stmt->execute()) {
                    $memberComplete = true;
                } else {
                    $memberComplete = false;
                }

                $returnedId = $stmt->insert_id;

                if ($hasReferralCode) {
                    $referralSql = "SELECT level, top_referrer FROM referral WHERE member_id = ?";

                    if ($stmt = $conn->prepare($referralSql)) {

                        /* bind parameters for the ? variable */
                        $stmt->bind_param("i", $referralCode);

                        /* execute query */
                        $stmt->execute();

                        /* bind result variables */
                        $stmt->bind_result($referralLevel, $referralTopReferrer);

                        /* fetch value */
                        $stmt->fetch();

                        /* close statement */
                        $stmt->close();
                    }

                    $stmtReferral = $conn->prepare("INSERT INTO referral (member_id, referral_code, level, top_referrer) VALUES (?, ?, ?, ?)");

                    if ($referralTopReferrer == null) {
                        $totalLevel = 1;
                        $stmtReferral->bind_param("iiii", $returnedId, $referralCode, $totalLevel, $referralCode);
                    } else {
                        $totalLevel = $referralLevel + 1;
                        $stmtReferral->bind_param("iiii", $returnedId, $referralCode, $totalLevel, $referralTopReferrer);
                    }

                    if ($stmtReferral->execute()) {
                        $referralComplete = true;
                    } else {
                        $referralComplete = false;
                    }
                    $stmtReferral->close();
                } else {
                    $stmt->close();
                }

                /***********************************EMAIL WELCOME MESSAGE**************************************/
                //$to =$email;
                //$subject = "Welcome to ISpendTribute";
                //$htmlContent = file_get_contents("mail.html");
                //$headers = "MIME-Version: 1.0" . "\r\n";
                //$headers .= "Content-type:text/html;  charset=iso-8859-1" . "\r\n";
                //$headers .= 'From:ISpendTribute<willteh.ist@gmail.com>' . "\r\n";
                ////header("Location: login.php");
                //// Send email
                //if(mail($to,$subject,$htmlContent,$headers)):
                //$successMsg=  'Email Sent successfully';
                //else:
                //  $errorMsg = 'Email sending fail.';
                //endif;

                $to = $email;
                $subject = "Your ISpendTribute Account Verification Code";
                $htmlContent = "Hi, Thank You for registering with us!
									<br/>Below is your account verification code
									<br/>Verification Code: " . "$emailVerificationCode" .
                    "<br/>Please go to this <a href = \"http://ispendtribute.com/login.php\">link</a> to enter your verification code<br/>";
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;  charset=iso-8859-1" . "\r\n";
                $headers .= 'From:ISpendTribute<willteh.ist@gmail.com>' . "\r\n";
                //header("Location: login.php");
                // Send email
                if (mail($to, $subject, $htmlContent, $headers)):
                    $emailMsg = "The verification code has been sent to $email successfully!";
                else:
                    $emailMsg = 'Failed to send email';
                endif;

                $_SESSION["id"] = $returnedId;
                header("Location: memberRegistrationPart2.php");
            }

        } else {
            if ($checkPhoneNo != NULL) {
                $phoneNoError = "This phone number already exists";
            }
            if ($checkEmail != NULL) {
                $emailError = "This email already exists";
            }
            if ($checkReferralCode == NULL) {
                $referralCodeError = "No such referral code found in the database";
            }
        }
    } else {
        $captchaError = "Captcha not ticked or expired!";
    }
}

ob_end_clean();
$conn->close();

if ($complete && $phoneNoError == null && $emailError == null && $referralCodeError == null) {
    echo '<script language="javascript">';
    echo 'alert("Account registered successfully")';
    echo '</script>';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link href="images/favicon.png" rel="icon" type="image/png"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">

    <title>Registration</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        h1 {
            color: #fff;
            padding-top: 50px;
        }

        .register-text {
            color: #9c77b4;
        }

        @media (max-width: 736px) {
            .footer {
                height: 18.50%;
            }

        }

        .caption {
            padding-top: 10px;
        }

        .block {
            width: 100%;
            height: 20%;
            border: 30px solid #f291bb;
            padding-bottom: 20px;
        }
    </style>
    <!--<script>
         function changeNav1Active() {
        console.log("1");
        clearActive();
        $("#navItem1").addClass("active");
      }

      function changeNav2Active() {
        console.log("2");
        clearActive();
        $("#navItem2").addClass("active");
      }

      function changeNav3Active() {
        console.log("3");
        clearActive();
        $("#navItem3").addClass("active");
      }

      function clearActive() {
        $(".navItem").removeClass('active');
      }

    </script>-->
</head>
<?php include 'nav.php' ?>
<body class="body-color">
<div class="container cancelpaddingtop">
    <h1 class="paddingtop25">Create Your Account</h1>
    <img src="images/heart1.jpg" class="heartimg"><br>
    <form id="registerFormPart1" class="form-horizontal" role="form" method="post" action=""
          onsubmit="return checkCaptcha();">
        <div class="form-group">
            <div class="col-sm-4" for="email">
                <input type="email" class="form-control form-controls text-line" id="email" name="email"
                       placeholder="Enter email" value="<?php echo $_POST["email"]; ?>" required>
                <span class="error"><?php echo $emailError; ?></span>
                <span class="glyphicon glyphicon-info-sign form-control-feedback" data-trigger="hover"
                      data-toggle="popover" data-placement="bottom"
                      title="You will receive a verification email to activate your account"></span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4" for="name">
                <input type="text" class="form-control form-controls text-line" id="full-name" name="full-name"
                       placeholder="Enter your full name"
                       value="<?php echo $_POST["full-name"]; ?>" required>
                <span class="error"><?php echo $fullNameError; ?></span>
                <span class="glyphicon glyphicon-info-sign form-control-feedback" rel="popover" data-trigger="hover"
                      data-toggle="popover" data-placement="bottom"
                      title="Please enter your Full name as shown in your NRIC" data-content="Content"></span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon form-controls text-line" id="basic-addon1">+60</span>
                    <input type="number" min="100000000" max="9999999999"
                           oninvalid="setCustomValidity('Enter 9 to 10 digits of numbers please');"
                           class="form-control form-controls text-line" oninput="setCustomValidity('');"
                           id="phone-no" name="phone-no" placeholder="Enter phone number"
                           value="<?php echo $_POST["phone-no"]; ?>" required>
                    <span class="error"><?php echo $phoneNoError; ?></span>
                </div>
                <span class="glyphicon glyphicon-info-sign form-control-feedback" rel="popover" data-trigger="hover"
                      data-toggle="popover" data-placement="bottom" title="Your phone number without " -" (dash)"
                data-content="Content" ></span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-4">
                <input type="password" pattern=".{8,}"
                       oninvalid="setCustomValidity('Password length needs to be at least 8');"
                       class="form-control form-controls text-line" oninput="setCustomValidity('');"
                       id="password" name="password" placeholder="Enter password" required>
                <span class="glyphicon glyphicon-info-sign form-control-feedback" rel="popover" data-trigger="hover"
                      data-toggle="popover" data-placement="bottom"
                      title="Your password must be at least 8 characters long and contains alphanumeric"
                      data-content="Content" aria-hidden="true"></span>
                <span class="error"><?php echo $passwordError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="password" class="form-control form-controls text-line" id="confirmation-password"
                       name="confirmation-password" placeholder="Enter your password again" required>
                <span class="error"><?php echo $confirmPasswordError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="number" class="form-control form-controls text-line" id="referral-code"
                       name="referral-code" placeholder="Enter your referral code if you have one" min="0"
                       value="<?php
                                    if($codeFromLink != null && $_POST["referral-code"] == null){
                                        echo $codeFromLink;
                                    }else{
                                        echo $_POST["referral-code"];
                                    }
                               ?>">
                <span class="glyphicon glyphicon-info-sign form-control-feedback" rel="popover"
                      title="The Phone Number of your Introducer" data-trigger="hover" data-toggle="popover"
                      data-placement="bottom" data-content="Content" aria-hidden="true"></span>
                <span class="error"><?php echo $referralCodeError; ?></span>
            </div>
        </div>

        <div id="html_element" class="g-recaptcha" data-sitekey="6LeaNw4UAAAAACk-lb6MHBWfKMhnmAdU2_Qh1Voa"
             style="transform:scale(0.77);transform-origin:0 0"></div>
        <span class="error"><?php echo $captchaError; ?></span>
        <div class="form-group caption">
            <div class="col-sm-4  register-text">
                <!--<div class="col-sm-offset-4 col-sm-10">-->
                <button type="submit" class="btn btn-default btn-min-register">Next</button>
                <?php echo $emailMsg; ?>
            </div>
        </div>
        <!--</div>-->
</div>
</form>
</div><!-- /.container -->
<div class="block"></div>
<?php include 'foot.php' ?>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">

    function checkCaptcha() {
        var googleResponse = jQuery('#g-recaptcha-response').val();
        if (!googleResponse) {
            $('<p style="color:red !important" class="error-captcha"><span class="glyphicon glyphicon-remove " ></span> Please fill up the captcha.</p>" ').insertAfter("#html_element");
            return false;
        } else {

            return true;
        }
    }
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });
</script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
