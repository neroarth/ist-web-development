<nav class="nav navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <img class="img-responsive logo" alt="Ist" src="images/brand.png">
            </a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <!--target-active-->
            <ul class="nav navbar-nav navbar-right text nav-margin">
                <li><a href="index.php">Home</a></li>
                <?php

                if (isLogin() || isAdminLogin() || isCashierLogin()) {

                    echo '<li><div class="btn-nav"><a class="btn btn-small navbar-btn pink" href = "index.php?logout=true">Logout</a></div></li>';
                    echo '<li><div class="btn-nav col-xs-1"><a class="btn btn-small navbar-btn pink-two" href="profile.php"><span style="color:#ffffff" class="register-button">Your Profile</span></a></div></li>';
                } else {
                    echo '<li><div class="btn-nav"><a class="btn btn-small navbar-btn pink" href="login.php">Log In</a></div></li>';
                    echo '<li><div class="btn-nav col-xs-1"><a class="btn btn-small navbar-btn pink-two" href="memberRegistration.php"><span style="color:#ffffff" class="register-button">Register</span></a></div></li>';
                }
                ?>
                <!--id="navItem1" onclick="changeNav1Active()" class="active navItem"-->

            </ul>
        </div>
    </div><!--/.nav-collapse -->
</nav>
<style>
    @media all and (max-width: 366px) {
        img.logo {
            width: 150px;
            height: auto;
            margin-top: 5px;
        }

        .navham {
            margin-top: 12px;
        }

        .navbar-default {
            min-height: 50px;
        }
    }
</style>