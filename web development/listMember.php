<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		$id = $_POST['memberId'];
		
		$sql = "SELECT  full_name, email, phone_no 
						FROM member WHERE id = ?";
		
		if ($stmt = $conn->prepare($sql)) {
	
		  $stmt->bind_param("i", $id);
	
		  $stmt->execute();
	
		  $stmt->bind_result($fullName,$email, $phoneNumber);
	
		  $stmt->fetch();
	
		  $stmt->close();
		}
		
		if($fullName == null){
			$msg = "No such member found!";
		}
		
		if($phoneNumber == 0){
			$phoneNumber = null;
		}
		
		$memberDetails = array(
			'full-name' => $fullName,
			'email' => $email,
			'phone-number' => $phoneNumber,
			'msg' => $msg
		);
		header('Content-Type: application/json');
	    echo json_encode($memberDetails);
		
		$conn->close();
	}
?>