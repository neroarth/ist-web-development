<?php
	session_start();
	
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		ob_start();
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		$complete = true;
		
		$repId = $_POST['repList'];
		
		$remarkRep = rewrite($_POST["remarkRep"]);
		$remarkRep = stripSpaces($remarkRep);
		if(strlen($remarkRep) > 300){
			$remarkRepError = "Remark should not be more than 300 characters";
		    $complete = false;
		}
		
		$sql = "UPDATE member SET category = ?, remark = ?, biz_rep_status = ?, admin_id = ? WHERE id = ?";
		
		if (isset($_POST['approveRep'])) {
			$approvalStatus = "approved";
			$category = "bizrep";
		}else if(isset($_POST['rejectRep'])) {
			$approvalStatus = "rejected";
			$category = "member";
		}
		else{
			header("Location: adminMerchantDetails");
			exit();
		}
		
		if($complete){
			$stmt = $conn->prepare($sql);
			$stmt->bind_param("ssssi", $category, $remarkRep, $approvalStatus, $_SESSION['admin-id'], $repId);
	
			$stmt->execute();
			
			header("Location: adminMerchantDetails");
		}else{
			header("Location: adminMerchantDetails?remarkRepError=$remarkRepError&remarkRep=$remarkRep");
		}
		
		
		
		$conn->close();
		ob_end_clean();
	}
?>