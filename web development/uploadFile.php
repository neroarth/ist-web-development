<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		for($i=0;$i<count($_FILES["uploadSsm"]["name"]);$i++){
			$ok = true;

			$imagePath = "uploads/merchants/".$_SESSION['id']."/";
		
			$fileName = basename($_FILES["uploadSsm"]["name"][$i]);
			
			//$fullPath = $imagePath . $fileName;
			$fullPath = $imagePath . "ssm_$i." . pathinfo($fileName,PATHINFO_EXTENSION);
			
			$fileType = pathinfo($fullPath,PATHINFO_EXTENSION);
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])){
				$check = getimagesize($_FILES["uploadSsm"]["tmp_name"][$i]);
				if($check !== false){
					$ok = true;
				} else {
					$msg = " The chosen file is not an image!\n";
					//header("Location: index?&msg=$msg");
					$ok = false;
				}
			}
			
			if($fileType != "jpg" && $fileType != "jpeg" && $fileType != "png"
			&& $fileType != "JPG" && $fileType != "JPEG" && $fileType != "PNG"
			&& $fileType != "gif" && $fileType != "GIF"){
					$msg = "Only jpg, jpeg, gif and png file types are allowed\n";
					//header("Location: index?&msg=$msg");
					$ok = false;
					
			}
			
			//10mb = 10 * 1024 * 1024 bytes
			if($_FILES["uploadSsm"]["size"][$i] > 10485760){
					$msg = " Uploaded picture cannot exceed 10mb\n";
					//header("Location: index?&msg=$msg");
					$ok = false;
			}
	
			if($ok == false){
				//$msg = $msg . "Sorry some error occured and your file was not uploaded.";
				
				//header("Location: index?&msg=$msg");
			}
			else{
				if(!file_exists("$imagePath")){
					mkdir("$imagePath",0755,true);
				}
					
				if(file_exists($fullPath)){
					if(unlink($fullPath)==1){
						$replaced = true;
					}
				}
				if(move_uploaded_file($_FILES["uploadSsm"]["tmp_name"][$i], $fullPath)){
					if($replaced == false){
						//include "conDb.php";
						//$sql = "INSERT INTO image (imagePath,imageTitle,postId)
						//		VALUES('$fullPath','$fileName',$postId)";
						//		
						////execute the query
						//mysqli_query($conn,$sql) or die(mysqli_error());
						////close database connection
						//mysqli_close($conn);
					}
							//$stmt->execute();
								//execute the query
								//mysqli_query($conn,$sql) or die(mysqli_error($conn));
								//echo '<script type="text/javascript">'
								//		, 'alert("Data saved successfully!");'
								//	, '</script>';
								$msg = "$fileName has been uploaded successfully!";
								//header("Location: index.php?&msg=$msg");
				}
				else{
								$msg = "Sorry, there was an error uploading your file.";
								//header("Location: index?&msg=$msg");
				}
			}
			echo "<div>$msg</div>";
		}
		$conn->close();
	}
?>