<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";

        $perpage = rewrite($_POST['perpage']);
        $currentPage = rewrite($_POST['currentPage']);

        if($currentPage == 0){
            $currentPage = 1;
        }else if($currentPage == null){
            $currentPage = 1;
        }

        //last row of record
        $end = $perpage * $currentPage;
        //first row of record
        $offset = $end - $perpage;
        $no = $offset + 1;

        $countRows = getCreditRowCount($conn);

        $tableData = "";

        foreach ($countRows as &$value) {
            //rounding up
            $totalPages = ceil($value['count'] / $perpage);
        }

        //PREVIOUS
        if ($currentPage <= 1) {
            $tableData.= "<span class = 'current-page'>Previous</span>";
        } else {
            $pageNo = $currentPage - 1;
            $tableData .= "<span><a class = 'link-page' href='adminProfile?creditPage=$pageNo'>< Previous</a></span>";
        }

        //BETWEEN
        for ($x = 1; $x <= $totalPages; $x++) {
            if ($x <> $currentPage) {
                $tableData .= "<span><a class = 'link-page' href='adminProfile?creditPage=$x' >$x</a></span>";
            } else {
                $tableData .= "<span class ='current-page' style='font-weight: bold;'>$x</span>";
            }
        }

        //NEXT
        if ($currentPage == $totalPages) {
            $tableData .= "<span class ='current-page'>Next</span>";
        } else {
            $pageNo = $currentPage + 1;
            $tableData .= "<span><a class ='link-page' href='adminProfile?creditPage=$pageNo'>Next ></a></span>";
        }

        echo $tableData;

		$conn->close();
	}
?>