<?php
session_start();
require_once "php-files/usefulFunction.php";
require_once "php-files/conDb.php";

if (isset($_GET['id'])) {
    $id = rewrite($_GET['id']);
    $rows = getNgoDetails($conn, $id);

    foreach ($rows as &$value) {
        $logoLink = $value['logo-link'];
        $name = $value['name'];
        $description = $value['description'];
        $operatingHours = $value['operating-hours'];
        $email = $value['email'];
        $phoneNumber = $value['phone-number'];
        $address = $value['lot-number'] . ' ' . $value['street-name'] . ' ' . $value['postcode'] . ' ' . $value['city'] . ' ' . $value['state'];
        $website = $value['website'];
        $socialMedia = $value['social-media'];
        $category = $value['category'];
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(!isLogin()){
        $conn->close();
        header("Location: ngoDetails?id=$id&donation-amount-error=Please login as a member first");
        exit();
    }

    $donationAmount = rewrite($_POST['donation-amount']);

    if ($donationAmount >= 0 && $donationAmount < 100000000000 && validateTwoDecimals($donationAmount)) {

    } else {
        $numAfterDecimalPoints = getNumberAfterDecimalPoint($donationAmount);
        if ($numAfterDecimalPoints[1] != 0 && $numAfterDecimalPoints[2] != 0) {
            $conn->close();
            header("Location: ngoDetails?id=$id&donation-amount-error=Please do not enter amount that is less than 0");
            exit();
        }
    }

    if(!checkEnoughContributionCredit($conn,$_SESSION['id'],$donationAmount)){
        $conn->close();
        header("Location: ngoDetails?id=$id&donation-amount-error=Not enough contribution credit");
        exit();
    }else{
        $previousCashbackAmounts = getMemberCashbackAmount($_SESSION['id'], $conn);
        $currentCharityAmount = $previousCashbackAmounts['current-charity-amount'] - $donationAmount;

        $sql = "INSERT INTO donation (amount, ngo_id, member_id) VALUES (?, ?, ?)";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("dii", $donationAmount, $id, $_SESSION['id']);

            $stmt->execute();

            $stmt->close();
        }

        $sql = 'UPDATE member SET current_charity_amount = ? WHERE id = ?';

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("di", $currentCharityAmount, $_SESSION['id']);

            $stmt->execute();

            $stmt->close();
        }

        echo '<script language="javascript">';
        echo 'alert("Thank you for contributing on behalf of ' . $name . '")';
        echo '</script>';
    }
}

$conn->close();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>iSpendtribute</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
    <link href="main-style.css" rel="stylesheet">
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>

</head>

<body>
<?php include 'navProfile.php'; ?>

<div class="bigdiv">
    <div class="left-div">
        <div class="shop-image">
            <?php
            if($logoLink == null){
                echo '<img class="shop-img" src="images/fromagerie.jpg">';
            }else{
                echo '<img class="shop-img" src="' . $logoLink .'">';
            }
            ?>
        </div>
    </div>
    <div class="right-div">
        <h1 class="shopname" id="shopName"><b><?php echo $name; ?></b></h1>
        <p class="description1"><?php echo $description; ?></p>
        <p class="description"><b>Operating hours: </b><?php echo $operatingHours; ?></p>
        <p class="description"><b>Email: </b><?php echo $email; ?></p>
        <p class="description"><b>Contact: </b><?php echo $phoneNumber; ?></p>
        <p class="description"><b>Address: </b><?php echo $address; ?></p>
        <p class="description"><b>Website: </b><a href="<?php echo $website; ?>"
                                                  class="shop-name"><?php echo $website; ?></a></p>
        <p class="description"><b>Facebook Page: </b><a href="<?php echo $socialMedia; ?>" class="shop-name">Follow
                us</a></p>
        <p class="description"><b>Category: </b><?php echo $category; ?></p>
        <p class="description d2"><b>Let's Contribute</b></p>

        <form id="contributionForm" class="form-horizontal" role="form" method="post" action="">
            <span class="contribute-credit"><b>Contribute Credits :  </b></span>
            <input type="number" step="0.01" min="0"
                   oninvalid="setCustomValidity('Please do not enter any amount less than 0');"
                   oninput="setCustomValidity('')"
                   class="contributecredit" id="donation-amount" name="donation-amount">
            <!-- Modal -->
    <button type="button" class="contribute-div" data-toggle="modal" data-target="#myModal" style="padding:5px;">Contribute</button>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Do you want to contribute?</h4>
                </div>
                <div class="modal-footer">
                    <div class="contribute-div">
                        <input type="submit" class="confirm-a " value="contribute">
                    </div>
                    <br><p class="error-msg-p"><?php echo $_GET["donation-amount-error"]; ?></p><br>
                </div>
            </div>

        </div>
    </div>
</div>
        </form>
    </div>

</div>

<?php include 'foot.php'; ?>
<style>
.error-msg-p{
text-align: left !important;
    clear: both !important;	
	margin-bottom:0;
}
input.confirm-a {
    -webkit-appearance: inherit !important;
    background-color: transparent !important;
    border: 0px !important;
    padding: 6px !important;
}
</style>
</body>

</html>

