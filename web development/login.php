<?php
	session_start();
	ob_start();
	require_once "php-files/usefulFunction.php";
	require_once "php-files/conDb.php";

	if(isLogin()){
		$sql = "SELECT verification_status FROM member WHERE id = ?";

		if ($stmt = $conn->prepare($sql)) {

		  $stmt->bind_param("i", $_SESSION['id']);

		  $stmt->execute();

		  $stmt->bind_result($isVerified);

		  $stmt->fetch();

		  $stmt->close();

		}
		if($isVerified == 0){
			header("Location: verification.php");
		}else{
			header("Location: profile.php");
		}
		exit();
	}

	if(isCashierLogin()){
        header("Location: enterReceipt.php");
    }

	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if(isset($_POST['email'])&&isset($_POST['password'])){

			$password = $_POST['password'];
			$password = rewrite($password);
			$password = hash('sha256',$password);
			$email= rewrite($_POST['email']);
			$email = stripAllWhiteSpaces($email);

			if (isset($_POST['memberCashierSwitch'])){
				$sql = "SELECT id,password,salt,verification_status FROM member WHERE email = ?";

				if ($stmt = $conn->prepare($sql)) {

				  $stmt->bind_param("s", $email);

				  $stmt->execute();

				  $stmt->bind_result($rsId,$rsPassword,$rsSalt,$isVerified);

				  $stmt->fetch();

				  $stmt->close();
				}

				$password = $rsSalt . $password;
				$finalPassword = hash('sha256',$password);

				if($finalPassword == $rsPassword){
					$_SESSION["id"]=$rsId;

					$sql = "SELECT id FROM merchant WHERE member_id = ?";

					if ($stmt = $conn->prepare($sql)) {

						/* bind parameters for the ? variable */
						$stmt->bind_param("i", $_SESSION['id']);

						/* execute query */
						$stmt->execute();

						/* bind result variables */
						$stmt->bind_result($rsMerchantId);

						/* fetch value */
						$stmt->fetch();

						/* close statement */
						$stmt->close();
					}

					if($rsMerchantId != null){
						$_SESSION["merchant-id"]=$rsMerchantId;
					}

					if($isVerified == 0){
						  header("Location: verification.php");
					}else{
						  header("Location: profile.php");
					}
				}
				else{
					$error = "Wrong email or password!";
				}
			}else{
				$sql = "SELECT username,password,salt,active_status FROM cashier WHERE username = ?";

				if ($stmt = $conn->prepare($sql)) {

				  $stmt->bind_param("s", $email);

				  $stmt->execute();

				  $stmt->bind_result($rsId,$rsPassword,$rsSalt,$rsActiveStatus);

				  $stmt->fetch();

				  $stmt->close();
				}

				$password = $rsSalt . $password;
				$finalPassword = hash('sha256',$password);

				if($finalPassword == $rsPassword){
				    if($rsActiveStatus == 1){
                        $_SESSION["cashier-id"]=$rsId;
                        header("Location: enterReceipt.php");
                    }else{
                        $error = "This account has been deactivated!";
                    }

				}
				else{
				  $error = "Wrong username or password for cashier account!";
				}
			}
		}
	}
	ob_end_clean();
	//$stmt->close();
	$conn->close();
?>

	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<link href="images/favicon.png" rel="icon" type="image/png">
		<meta name="description" content="ISpendTribute">
		<meta name="author" content="Spending tribute">
		<title>Login</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

		<!-- Custom styles for this template -->
		<link href="main-style.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="assets/js/ie-emulation-modes-warning.js"></script>
		<script src="assets/js/navigation.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
		<style>
			/**using  this css to overcome this bootstrap fixed top navbar problem**/

			@media (max-width: 736px) {
				.footer {
					height: 22%;
				}
				.image {
					width: 35%;
					height: 35%;
					padding-top: 2%;
					padding-bottom: -13%;
				}
			}

			.image {
				padding-top: 2%;
				padding-bottom: -13%;
			}

			body {
				background-color: #9c77b4;
			}

			h1 {
				color: #fff;
				margin-top: 15%;
				padding-left: 10%;
			}

			.form-group {
				padding-left: 10%;
			}

			form {
				padding-top: 2%;
			}

			.white {
				background-color: #fff;
			}
		</style>
		<!--<script>
	  $('#login').click(function(e) {

        e.preventDefault();
	  });

	  function changeNav1Active() {
		console.log("1");
		clearActive();
        $("#navItem1").addClass("active");
      }

	  function changeNav2Active() {
		console.log("2");
		clearActive();
        $("#navItem2").addClass("active");
      }

	  function changeNav3Active() {
		console.log("3");
		clearActive();
		$("#navItem3").addClass("active");
      }

	  function clearActive() {
		$(".navItem").removeClass('active');
      }
	</script>-->

	</head>

	<body>
		<?php include 'nav.php' ?>
         <div class="heartmin">
		<div class="container cancelpaddingtop cancelmarginleft pleft15">
			<h1 class="paddingtopabc">Log In</h1>
			<form id="loginForm" class="form-horizontal" role="form" method="post" action="">
				<div class="form-group">
					<div class="col-sm-4">
						<input type="text" class="form-control form-controls text-line" id="email" name="email" placeholder="Enter your email address or username" value="<?php echo $_POST[" email "];?>" required>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<input type="password" class="form-control form-controls text-line" id="password" name="password" placeholder="Enter your password" required>
						<span class="error"><?php echo $error;?></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<div class="memberCashierSwitch">
							<input type="checkbox" name="memberCashierSwitch" class="memberCashierSwitch-checkbox" id="memberCashierSwitch" checked>
							<label class="memberCashierSwitch-label" for="memberCashierSwitch">
					  <span class="memberCashierSwitch-inner"></span>
					  <span class="memberCashierSwitch-switch"></span>
				  </label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-4">
						<button type="submit" class="btn btn-default btn-min-block">Log In</button>
					</div>
				</div>
			</form>
		</div>
		<!-- /.container -->
		<img alt="heart" src="./images/log-in-logo.png" class="img-responsive pull-left image" width="200px" height="200px"></div>
		<?php include 'foot.php' ?>
		<!-- Bootstrap core JavaScript
	 ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
		<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<script src='https://www.google.com/recaptcha/api.js'></script>

		<script type="text/javascript">
			function checkCaptcha() {
				var googleResponse = jQuery('#g-recaptcha-response').val();
				if (!googleResponse) {
					$('<p style="color:red !important" class="error-captcha"><span class="glyphicon glyphicon-remove" ></span> Please fill up the captcha.</p>" ').insertAfter("#html_element");
					return false;
				} else {
					return true;
				}
			}
		</script>
		<script>
			window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')
		</script>
		<script src="dist/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>

	</html>
