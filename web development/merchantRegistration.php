<?php
session_start();
ob_start();
require_once "php-files/usefulFunction.php";
require_once "php-files/conDb.php";
if (!isLogin()) {
    header("Location: login.php");
    exit();
}

//now changed to can register as multiple merchant so commented this code
//	$sql = "SELECT id FROM merchant WHERE member_id = ?";
//
//		if ($stmt = $conn->prepare($sql)) {
//
//			/* bind parameters for the ? variable */
//			$stmt->bind_param("i", $_SESSION['id']);
//
//			/* execute query */
//			$stmt->execute();
//
//			/* bind result variables */
//			$stmt->bind_result($checkAlreadyMerchant);
//
//			/* fetch value */
//			$stmt->fetch();
//
//			/* close statement */
//			$stmt->close();
//		}
//
//	if($checkAlreadyMerchant != null){
//		header("Location: profile.php");
//		exit();
//	}

if ($_SERVER["REQUEST_METHOD"] == "POST") {


    $complete = true;

    $shopName = rewrite($_POST['shop-name']);
    $shopName = stripSpaces($shopName);

    $sql = "SELECT shop_name FROM merchant WHERE shop_name = ?";

    if ($stmt = $conn->prepare($sql)) {

        /* bind parameters for the ? variable */
        $stmt->bind_param("s", $shopName);

        /* execute query */
        $stmt->execute();

        /* bind result variables */
        $stmt->bind_result($checkShopName);

        /* fetch value */
        $stmt->fetch();

        /* close statement */
        $stmt->close();
    }

//		if($checkShopName == null && $checkAlreadyMerchant == null){
    if ($checkShopName == null) {
        if (strlen($shopName) <= 0) {
            $shopNameError = "Shop name name can't be empty";
            ////$complete = false;
        } else if (strlen($shopName) > 200) {
            $shopNameError = "Shop name name can't be more than 200 characters";
            $complete = false;
        }

        $description = rewrite($_POST['description']);
        if (strlen($description) > 10000) {
            $descriptionError = "Description can't be more than 10,000 characters";
            $complete = false;
        }

        $operatingHours = rewrite($_POST['operating-hours']);
        if (strlen($operatingHours) > 500) {
            $operatingHoursError = "Operating hours can't be more than 500 characters";
            $complete = false;
        }

        $email = rewrite($_POST["email"]);
        $email = stripAllWhiteSpaces($email);
        //cleans the email
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        //check whether the email is correct or not
        if (strlen($email) > 100) {
            $emailError = "Your email can't be more than 100 characters";
            $complete = false;
        } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $emailError = "Incorrect email format!";
            $complete = false;
        }

        $phoneNo = rewrite($_POST['phone-no']);
        if (strlen($phoneNo) < 8 || strlen($phoneNo) > 10) {
            $phoneNoError = "Incorrect phone number!";
            $complete = false;
        }

        $lotNumber = rewrite($_POST['lot-number']);
        $lotNumber = stripSpaces($lotNumber);
        if (strlen($lotNumber) > 60) {
            $lotNumberError = "Lot numbers can't be more than 60 characters";
            $complete = false;
        }

        $streetName = rewrite($_POST['street-name']);
        $streetName = stripSpaces($streetName);
        if (strlen($streetName) <= 0) {
            $streetNameError = "Street name can't be empty";
            $complete = false;
        } else if (strlen($streetName) > 200) {
            $streetNameError = "Street name can't be more than 200 characters";
            $complete = false;
        }

        $postcode = rewrite($_POST['postcode']);
        $postcode = stripAllWhiteSpaces($postcode);
        if (strlen($postcode) <= 0) {
            $postcodeError = "Post code can't be empty";
            $complete = false;
        } else if (strlen($postcode) > 30) {
            $postcodeError = "Post code can't be more than 30 characters";
            $complete = false;
        }

        $city = rewrite($_POST['city']);
        $city = stripSpaces($city);
        if (strlen($city) <= 0) {
            $cityError = "City can't be empty";
            $complete = false;
        } else if (strlen($city) > 100) {
            $cityError = "City can't be more than 100 characters";
            $complete = false;
        }

        $state = rewrite($_POST['state']);

        $website = rewrite($_POST['website']);
        if (strlen($website) > 150) {
            $websiteError = "Website can't be more than 150 characters";
            $complete = false;
        }

        $socialMedia = rewrite($_POST['social-media']);
        if (strlen($socialMedia) > 500) {
            $socialMediaError = "Social media links can't be more than 500 characters";
            $complete = false;
        }

        $cashbackRate = rewrite($_POST['cashback-rate']);
        if ($cashbackRate >= 0 && $cashbackRate <= 100 && validateTwoDecimals($cashbackRate)) {

        } else {
            $numAfterDecimalPoints = getNumberAfterDecimalPoint($cashbackRate);
            if ($numAfterDecimalPoints[1] != 0 && $numAfterDecimalPoints[2] != 0) {
                $cashbackRateError = "Please enter the cashback rate at the range of 0% to 100% with only 2 decimal points";
                $complete = false;
            }
        }

        $bankType = rewrite($_POST['bank-type']);

        $accountHolderName = rewrite($_POST['account-holder-name']);
        $accountHolderName = stripSpaces($accountHolderName);
        if (strlen($accountHolderName) <= 0) {
            $accountHolderNameError = "Account holder name can't be empty";
            $complete = false;
        } else if (strlen($accountHolderName) > 100) {
            $accountHolderNameError = "Account holder name can't be more than 100 characters";
            $complete = false;
        }

        $accountNumber = rewrite($_POST['account-number']);
        $accountNumber = stripAllWhiteSpaces($accountNumber);

        if (strlen($accountNumber) <= 0) {
            $accountNumberError = "Account number can't be empty";
            $complete = false;
        } else if (strlen($accountNumber) >= 18) {
            $accountNumberError = "Invalid account number length";
            $complete = false;
        }

        $latitude = rewrite($_POST['latitude']);
        if($latitude != null && !is_numeric($latitude)){
            $latitudeError = "Please enter numbers only";
            $complete = false;
        }
        else if($latitude != null && !($latitude >= -90 && $latitude <= 90) ){
            $latitudeError = "Please enter values between -90 to 90 for latitude";
            $complete = false;
        }

        $longitude = rewrite($_POST['longitude']);
        if($longitude != null && !is_numeric($longitude)){
            $longitudeError = "Please enter numbers only";
            $complete = false;
        }
        else if($longitude != null && !($longitude >= -180 && $longitude <= 180) ){
            $longitudeError = "Please enter values between -180 to 180 for longitude";
            $complete = false;
        }

        $category = rewrite($_POST['category']);

        if ($complete) {
            $pinNo = mt_rand(100000,999999);

            $stmt = $conn->prepare("INSERT INTO merchant (
									   shop_name, description, operating_hours, email, phone_number, lot_number,
									   street_name, postcode, city, state, website,
									   social_media, bank_type, bank_account_number, account_holder_name, member_id,
									   latitude, longitude, category, pin_no, pin_salt)
									   VALUES (
									   ?, ?, ?, ?, ?, ?,
									   ?, ?, ?, ?, ?,
									   ?, ?, ?, ?, ?,
									   ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssissssssssssiddsss", $shopName, $description, $operatingHours, $email, $phoneNo, $lotNumber,
                $streetName, $postcode, $city, $state, $website,
                $socialMedia, $bankType, $accountNumber, $accountHolderName, $_SESSION['id'],
                $latitude, $longitude, $category, $pinNo, $pinSalt);

            if ($stmt->execute()) {
                if ($cashbackRate != null) {
                    $returnedMerchantId = $stmt->insert_id;

                    $stmtRate = $conn->prepare("INSERT INTO cashback_rate (
								   merchant_id, rate) VALUES (?, ?)");
                    $stmtRate->bind_param("id", $returnedMerchantId, $cashbackRate);

                    $stmtRate->execute();
                    $stmtRate->close();
                    header("Location: merchantRegistrationPart2");
                } else {
                    header("Location: profile");
                }
                $returnedId = $stmt->insert_id;
                $_SESSION["merchant-id"] = $returnedId;
            }
            $stmt->close();
        }
    } else {
        $shopNameError = "This shop name already exists, please enter a different one";
    }

}

ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link href="images/favicon.png" rel="icon" type="image/png"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">

    <title>Merchant Registration</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        h1 {
            color: #fff;
            padding-top: 50px;
        }

        .register-text {
            color: #9c77b4;
        }

        @media (max-width: 736px) {
            .footer {
                height: 18%;
            }

        }

        .caption {
            padding-top: 10px;
        }

        .block {
            width: 100%;
            height: 20%;
            border: 30px solid #f291bb;
            padding-bottom: 20px;
        }
    </style>
</head>
<?php include 'nav.php' ?>
<body class="body-color">
<div class="container cancelpaddingtop">
    <h1 class="paddingtop25">Create Your Merchant Account</h1>
    <img src="images/heart1.jpg" class="heartimg"><br>
    <form id="merchantRegisterForm" class="form-horizontal" role="form" method="post" action="">

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="shop-name" name="shop-name"
                       placeholder="Shop Name"
                       value="<?php echo $_POST["shop-name"]; ?>" required>
                <span class="error"><?php echo $shopNameError; ?></span>
            </div>
        </div>

        <div class="form-group desdiv">
            <textarea class="form-control transparentarea" rows="5" id="description" name="description"
                      placeholder="Description"><?php echo $_POST["description"]; ?></textarea>
            <span class="error"><?php echo $descriptionError; ?></span>
        </div>

        <div class="form-group desdiv">
            <label for="category"></label>
            <select class="form-control" id="category" name="category">
                <?php

                $rows = getListOfCategories($conn);
                foreach ($rows as &$value) {
                    if($_POST["category"] == $value['name']){
                        echo '<option value="' . $value['name'] . '" selected >' . $value['name'] . '</option>';
                    }else{
                        echo '<option value="' . $value['name'] . '">' . $value['name'] . '</option>';
                    }
                }

                ?>
            </select>
        </div>

        <div class="form-group desdiv">
            <textarea class="form-control transparentarea" rows="3" id="operating-hours" name="operating-hours"
                      placeholder="Operating Hours"><?php echo $_POST["operating-hours"]; ?></textarea>
            <span class="error"><?php echo $operatingHoursError; ?></span>
        </div>

        <div class="form-group">
            <div class="col-sm-4" for="email">
                <input type="email" class="form-control form-controls text-line" id="email" name="email"
                       placeholder="Email" value="<?php echo $_POST["email"]; ?>" required>
                <span class="error"><?php echo $emailError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <div class="input-group">
                    <span class="input-group-addon form-controls text-line" id="basic-addon1">+60</span>
                    <input type="number" min="10000000" max="9999999999"
                           oninvalid="setCustomValidity('Enter 8 to 10 digits of numbers please');"
                           class="form-control form-controls text-line" oninput="setCustomValidity('');"
                           id="phone-no" name="phone-no" placeholder="Phone Number"
                           value="<?php echo $_POST["phone-no"]; ?>">
                    <span class="error"><?php echo $phoneNoError; ?></span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="lot-number" name="lot-number"
                       placeholder="Lot Number"
                       value="<?php echo $_POST["lot-number"]; ?>">
                <span class="error"><?php echo $lotNumberError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="street-name" name="street-name"
                       placeholder="Street Name"
                       value="<?php echo $_POST["street-name"]; ?>" required>
                <span class="error"><?php echo $streetNameError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="postcode" name="postcode"
                       placeholder="Post Code"
                       value="<?php echo $_POST["postcode"]; ?>" required>
                <span class="error"><?php echo $postcodeError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="city" name="city" placeholder="City"
                       value="<?php echo $_POST["city"]; ?>" required>
                <span class="error"><?php echo $cityError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="number" step="0.000000000001" min="-90" max="90"
                       oninvalid="setCustomValidity('Please enter values between -90 to 90 for latitude');"
                       oninput="setCustomValidity('');"
                       class="form-control form-controls text-line" id="latitude" name="latitude" placeholder="Latitude"
                       value="<?php echo $_POST["latitude"]; ?>">
                <span class="error"><?php echo $latitudeError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="number" step="0.000000000001" min="-180" max="180"
                       oninvalid="setCustomValidity('Please enter values between -180 to 180 for latitude');"
                       oninput="setCustomValidity('');"
                       class="form-control form-controls text-line" id="longitude" name="longitude" placeholder="Longitude"
                       value="<?php echo $_POST["longitude"]; ?>">
                <span class="error"><?php echo $longitudeError; ?></span>
            </div>
        </div>

        <div class="form-group desdiv">
            <label for="state"></label>
            <select class="form-control" id="state" name="state">
                <option value="Johor" <?php if($_POST["state"] == "Johor"){echo 'selected="selected"';} ?> >Johor</option>
                <option value="Kedah" <?php if($_POST["state"] == "Kedah"){echo 'selected="selected"';} ?> >Kedah</option>
                <option value="Kelantan" <?php if($_POST["state"] == "Kelantan"){echo 'selected="selected"';} ?> >Kelantan</option>
                <option value="Kuala Lumpur" <?php if($_POST["state"] == "Kuala Lumpur"){echo 'selected="selected"';} ?> >Kuala Lumpur</option>
                <option value="Labuan" <?php if($_POST["state"] == "Labuan"){echo 'selected="selected"';} ?> >Labuan</option>
                <option value="Melaka" <?php if($_POST["state"] == "Melaka"){echo 'selected="selected"';} ?> >Melaka</option>
                <option value="Negeri Sembilan" <?php if($_POST["state"] == "Negeri Sembilan"){echo 'selected="selected"';} ?> >Negeri Sembilan</option>
                <option value="Pahang" <?php if($_POST["state"] == "Pahang"){echo 'selected="selected"';} ?> >Pahang</option>
                <option value="Perak" <?php if($_POST["state"] == "Perak"){echo 'selected="selected"';} ?> >Perak</option>
                <option value="Perlis" <?php if($_POST["state"] == "Perlis"){echo 'selected="selected"';} ?> >Perlis</option>
                <option value="Pulau Pinang" <?php if($_POST["state"] == "Pulau Pinang"){echo 'selected="selected"';} ?>>Pulau Pinang</option>
                <option value="Putrajaya" <?php if($_POST["state"] == "Putrajaya"){echo 'selected="selected"';} ?> >Putrajaya</option>
                <option value="Sabah" <?php if($_POST["state"] == "Sabah"){echo 'selected="selected"';} ?> >Sabah</option>
                <option value="Sarawak" <?php if($_POST["state"] == "Sarawak"){echo 'selected="selected"';} ?> >Sarawak</option>
                <option value="Selangor" <?php if($_POST["state"] == "Selangor"){echo 'selected="selected"';} ?> >Selangor</option>
                <option value="Terengganu" <?php if($_POST["state"] == "Terengganu"){echo 'selected="selected"';} ?>>Terengganu</option>
            </select>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="website" name="website"
                       placeholder="Website Link"
                       value="<?php echo $_POST["website"]; ?>">
                <span class="error"><?php echo $websiteError; ?></span>
            </div>
        </div>

        <div class="form-group desdiv">
            <textarea class="form-control transparentarea" rows="3" id="social-media" name="social-media"
                      placeholder="Social Media Links"><?php echo $_POST["social-media"]; ?></textarea>
            <span class="error"><?php echo $socialMediaError; ?></span>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <div class="input-group">
                    <input type="number" step="0.01" min="0" max="100"
                           oninvalid="setCustomValidity('Please enter values from 0% to 100% for cashback rate');"
                           class="form-control form-controls text-line" oninput="setCustomValidity('');"
                           id="cashback-rate" name="cashback-rate" placeholder="Cashback Rate"
                           value="<?php echo $_POST["cashback-rate"]; ?>" required>
                    <span class="input-group-addon form-controls text-line" id="basic-addon1">%</span>
                    <span class="error"><?php echo $cashbackRateError; ?></span>
                </div>
            </div>
        </div>

        <div class="form-group desdiv">
            <label for="bank-type"></label>
            <select class="form-control" id="bank-type" name="bank-type">
                <?php

                $rows = getListOfBanks($conn);
                foreach ($rows as &$value) {
                    if($_POST["bank-type"] == $value['name']){
                        echo '<option value="' . $value['name'] . '"  selected >' . $value['name'] . '</option>';
                    }else{
                        echo '<option value="' . $value['name'] . '"  >' . $value['name'] . '</option>';
                    }
                }
                $conn->close();
                ?>
            </select>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="account-holder-name"
                       name="account-holder-name" placeholder="Account holder name"
                       value="<?php echo $_POST["account-holder-name"]; ?>" required>
                <span class="error"><?php echo $accountHolderNameError; ?></span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-4">
                <input type="text" class="form-control form-controls text-line" id="account-number"
                       name="account-number" placeholder="Account number"
                       value="<?php echo $_POST["account-number"]; ?>" required>
                <span class="error"><?php echo $accountNumberError; ?></span>
            </div>
        </div>

        <div class="form-group caption">
            <div class="col-sm-4  register-text">
                <button type="submit" class="btn btn-default btn-min-register">Register as Merchant</button>
            </div>
        </div>
    </form>
</div><!-- /.container -->
<div class="block"></div>
<?php include 'foot.php' ?>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
<style>
    ::-webkit-input-placeholder {
        color: white !important;
    }

    ::-moz-placeholder {
        color: white !important;
    }

    /* firefox 19+ */
    :-ms-input-placeholder {
        color: white !important;
    }

    /* ie */
    input:-moz-placeholder {
        color: white !important;
    }
</style>
</body>
</html>
