<?php
	session_start();
	ob_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";
		
		$merchantId = rewrite($_POST['shop-name']);
		
		$amountSpent = rewrite($_POST['amount-spent']);
		if($amountSpent >= 0 && $amountSpent < 100000000000 && validateTwoDecimals($amountSpent)){
						
		}
		else{
			$numAfterDecimalPoints = getNumberAfterDecimalPoint($amountSpent);
			if($numAfterDecimalPoints[1] != 0 && $numAfterDecimalPoints[2] != 0){
				$cashbackRateError = "Please do not enter amount that is less than 0";
				header("Location: uploadReceiptHtml.php?msg=$cashbackRateError");
				exit();
			}
		}
		
		for($i=0;$i<count($_FILES["uploadReceipt"]["name"]);$i++){
			$ok = true;
			
			$sql = "SELECT count(*) FROM receipt WHERE member_id = ? AND merchant_id = ?";

			if ($stmt = $conn->prepare($sql)) {
	
				/* bind parameters for the ? variable */
				$stmt->bind_param("ii", $_SESSION['id'], $merchantId);
	
				/* execute query */
				$stmt->execute();
	
				/* bind result variables */
				$stmt->bind_result($receiptCount);
	
				/* fetch value */
				$stmt->fetch();
	
				/* close statement */
				$stmt->close();
			}

			$imagePath = "uploads/receipts/".$_SESSION['id']."/".$merchantId."/";
		
			$receiptCount++;
			$fileName = basename($_FILES["uploadReceipt"]["name"][$i]);
			
			//$fullPath = $imagePath . $fileName;
			$fullPath = $imagePath . "receipt_$receiptCount." . pathinfo($fileName,PATHINFO_EXTENSION);
			
			$fileType = pathinfo($fullPath,PATHINFO_EXTENSION);
			// Check if image file is a actual image or fake image
			if(isset($_POST["submit"])){
				$check = getimagesize($_FILES["uploadSsm"]["tmp_name"][$i]);
				if($check !== false){
					$ok = true;
				} else {
					$msg = " The chosen file is not an image!\n";
					//header("Location: uploadReceiptHtml.php?&msg=$msg");
					$ok = false;
				}
			}
			
			if($fileType != "jpg" && $fileType != "jpeg" && $fileType != "png"
			&& $fileType != "JPG" && $fileType != "JPEG" && $fileType != "PNG"
			&& $fileType != "gif" && $fileType != "GIF"){
					$msg = "Only jpg, jpeg, gif and png file types are allowed\n";
					//header("Location: uploadReceiptHtml.php?&msg=$msg");
					$ok = false;
			}
			
			//10mb = 10 * 1024 * 1024 bytes
			if($_FILES["uploadSsm"]["size"][$i] > 10485760){
					$msg = " Uploaded picture cannot exceed 10mb\n";
					//header("Location: index?&msg=$msg");
					$ok = false;
			}
	
			if($ok == false){
				//$msg = $msg . "Sorry some error occured and your file was not uploaded.";
				
				//header("Location: index?&msg=$msg");
			}
			else{
				if(!file_exists("$imagePath")){
					mkdir("$imagePath",0755,true);
				}
					
				if(file_exists($fullPath)){
					if(unlink($fullPath)==1){
						$replaced = true;
					}
				}
				if(move_uploaded_file($_FILES["uploadSsm"]["tmp_name"][$i], $fullPath)){
					if($replaced == false){
						//include "conDb.php";
						//$sql = "INSERT INTO image (imagePath,imageTitle,postId)
						//		VALUES('$fullPath','$fileName',$postId)";
						//		
						////execute the query
						//mysqli_query($conn,$sql) or die(mysqli_error());
						////close database connection
						//mysqli_close($conn);
					}
							//$stmt->execute();
								//execute the query
								//mysqli_query($conn,$sql) or die(mysqli_error($conn));
								//echo '<script type="text/javascript">'
								//		, 'alert("Data saved successfully!");'
								//	, '</script>';
								$msg = "$fileName has been uploaded successfully!";
								//header("Location: index.php?&msg=$msg");
				}
				else{
								$msg = "Sorry, there was an error uploading your file.";
								//header("Location: index?&msg=$msg");
				}
			}
			echo "<div>$msg</div>";
		}
		$conn->close();
	}
	ob_end_clean();
?>