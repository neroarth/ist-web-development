<?php
	session_start();
	ob_start();
	require_once "php-files/usefulFunction.php";
	require_once "php-files/conDb.php";
	if(!isLogin()){
		header("Location: login.php");
		$conn->close();
		exit();
	}
	
	ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<link href="images/favicon.png" rel="icon" type="image/png"/>
	<meta name="description" content="ISpendTribute">
	<meta name="author" content="Spending tribute">

	<title>Merchant Registration</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
	<script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <?php include 'nav.php' ?>
  <body class="body-color">
    <div class="container">
        <div class="container">
			<div class="row">
				<form action="uploadReceipt.php"  class="form-horizontal" method="post" enctype="multipart/form-data" id="uploadSsmForm">
					<div class="col-md-5">
						<h1> Receipt Photo </h1>
						<img src="http://placehold.it/750x450" class="img-responsive" id="receiptPhoto" name="receiptPhoto" />
					 </div>
					<div class="col-md-9">
						<div class="form-group">
							<label for="bank-type"></label>
							<select class="form-control" id="shop-name" name="shop-name">
								<?php
									$rows = getListOfMerchants($conn);
									foreach ($rows as &$value) {
										echo '<option value="'.$value['id'].'">'.$value['shop_name'].'</option>';
									}
									$conn->close();
								?>
							</select>
						</div>
	
						<div class="form-group">
							<div class="col-md-3">
								<input type="number" step="0.01" min = "0" oninvalid="setCustomValidity('Please do not enter any amount less than 0');"
															 class="form-control form-controls text-line" oninput="setCustomValidity('');"
															 id="amount-spent" name ="amount-spent" placeholder="Amount Spent"
															 value="<?php echo $_POST["amount-spent"];?>" required>
								<span class="input-group-addon form-controls text-line" id="basic-addon1">%</span>
								<span class = "error"><?php echo $amountSpentError;?></span>
							</div>
						</div>
						
						<p> Date of Purchase </p>
						
						<div class="checkbox">
							<label>
								<input type="checkbox"> Check me out
							</label>
						</div>
						
					</div>
					
					<input name="uploadReceipt" type="file" accept="image/*" />
				</form>
			</div>
        </div>
    </div><!-- /.container -->
	<div class="block"></div>
	<?php include 'foot.php' ?>

	<!--The progress bar section-->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	
	<script type="text/javascript">    
		//configuration
		var max_file_size 			= 10485760; //allowed file size. (1 MB = 1048576)
		var allowed_file_types 		= ['image/png', 'image/gif', 'image/jpeg', 'image/pjpeg']; //allowed file types
		var result_output 			= '#output'; //ID of an element for response output
		var my_form_id 				= '#uploadSsmForm'; //ID of an element for response output
		var progress_bar_id 		= '#progress-wrp'; //ID of an element for response output
		var total_files_allowed 	= 3; //Number files allowed to upload
		
		
		
		//on form submit
		$(my_form_id).on( "submit", function(event) { 
			event.preventDefault();
			var proceed = true; //set proceed flag
			var error = [];	//errors
			var total_files_size = 0;
			
			//reset progressbar
			$(progress_bar_id +" .progress-bar").css("width", "0%");
			$(progress_bar_id + " .status").text("0%");
									
			if(!window.File && window.FileReader && window.FileList && window.Blob){ //if browser doesn't supports File API
				error.push("Your browser does not support new File API! Please upgrade."); //push error text
			}else{
				var total_selected_files = this.elements['uploadSsm[]'].files.length; //number of files
				
				//limit number of files allowed
				if(total_selected_files > total_files_allowed){
					error.push( "You have selected "+total_selected_files+" file(s), " + total_files_allowed +" is maximum!"); //push error text
					proceed = false; //set proceed flag to false
				}
				 //iterate files in file input field
				$(this.elements['uploadSsm[]'].files).each(function(i, ifile){
					if(ifile.value !== ""){ //continue only if file(s) are selected
						if(allowed_file_types.indexOf(ifile.type) === -1){ //check unsupported file
							//error.push( "<b>"+ ifile.name + "</b> is unsupported file type!"); //push error text
							error.push( "Sorry, please upload images with png, jpg or gif file extensions"); //push error text
							proceed = false; //set proceed flag to false
						}
		
						total_files_size = total_files_size + ifile.size; //add file size to total size
					}
				});
				
				//if total file size is greater than max file size
				if(total_files_size > max_file_size){ 
					error.push( "You have "+total_selected_files+" file(s) with total size "+total_files_size+", Allowed size is " + max_file_size +", Try smaller file!"); //push error text
					proceed = false; //set proceed flag to false
				}
				
				var submit_btn  = $(this).find("input[type=submit]"); //form submit button	
				
				//if everything looks good, proceed with jQuery Ajax
				if(proceed){
					//submit_btn.val("Please Wait...").prop( "disabled", true); //disable submit button
					var form_data = new FormData(this); //Creates new FormData object
					var post_url = $(this).attr("action"); //get action URL of form
					
					//jQuery Ajax to Post form data
		$.ajax({
			url : post_url,
			type: "POST",
			data : form_data,
			contentType: false,
			cache: false,
			processData:false,
			xhr: function(){
				//upload Progress
				var xhr = $.ajaxSettings.xhr();
				if (xhr.upload) {
					xhr.upload.addEventListener('progress', function(event) {
						var percent = 0;
						var position = event.loaded || event.position;
						var total = event.total;
						if (event.lengthComputable) {
							percent = Math.ceil(position / total * 100);
						}
						//update progressbar
						$(progress_bar_id +" .progress-bar").css("width", + percent +"%");
						$(progress_bar_id + " .status").text(percent +"%");
					}, true);
				}
				return xhr;
			},
			mimeType:"multipart/form-data"
		}).done(function(res){ //
			$(my_form_id)[0].reset(); //reset form
			$(result_output).html(res); //output response from server
			submit_btn.val("Upload").prop( "disabled", false); //enable submit button once ajax is done
		});
					
				}
			}
			
			$(result_output).html(""); //reset output 
			$(error).each(function(i){ //output any error to output element
				$(result_output).append('<div class="error">'+error[i]+"</div>");
			});
				
		});
	</script>

    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
