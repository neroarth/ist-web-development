<?php
	session_start();
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		require_once "php-files/usefulFunction.php";
		require_once "php-files/conDb.php";

		$perpage = $_POST['perpage'];

        if(isset($_POST["currentPage"])){
            $page = intval($_POST["currentPage"]);
        }
        else {
            $page = 1;
        }

        //last row of record
        $end = $perpage * $page;
        //first row of record
        $offset = $end - $perpage;

        $where1 = "receipt.image_url";

        $sortBy = "ASC";

        if(strcmp($_POST['sortBy'], "Newest") == 0){
            $sortBy = "DESC";
        }else if(strcmp($_POST['sortBy'], "Oldest") == 0){
            $sortBy = "ASC";
        }

        $rows = getDonationReport($conn, $_SESSION['ngo-id'], $offset, $perpage, $sortBy);

        if($rows == null){
            echo '<tr>
                       <td>Sorry, no records found</td>
                  </tr>';
        }else{
            $tableData = "";

            foreach ($rows as &$value) {
                $tableData .= '<tr>';
                $tableData .= '<td>' . $value['timestamp'] . '</td>';
                $tableData .= '<td> Donation from ' . $value['member-name'] . '</td>';
                $tableData .= '<td>' . $value['amount'] . '</td>';
                $tableData .= '</tr>';
            }

            echo $tableData;
        }

		$conn->close();
	}
?>