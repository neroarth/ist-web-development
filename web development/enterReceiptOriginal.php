<?php
session_start();
ob_start();
require_once "php-files/usefulFunction.php";
require_once "php-files/conDb.php";
if (!isCashierLogin()) {
    header("Location: login.php");
    $conn->close();
    exit();
}

if (isset($_GET["msg"])) {
    if (strcmp($_GET['msg'], "success") == 0) {
        $msg = "Receipt\'s details recorded successfully!";
    } else {
        $msg = "Sorry, failed to record the receipt\'s details. Please try again!";
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    require_once "php-files/usefulFunction.php";
    require_once "php-files/conDb.php";

    $complete = true;

    $amountSpent = rewrite($_POST['amount-spent']);
    if ($amountSpent >= 0 && $amountSpent < 100000000000 && validateTwoDecimals($amountSpent)) {

    } else {
        $numAfterDecimalPoints = getNumberAfterDecimalPoint($amountSpent);
        if ($numAfterDecimalPoints[1] != 0 && $numAfterDecimalPoints[2] != 0) {
            $amountSpentError = "Please do not enter amount that is less than 0";
            $complete = false;
        }
    }

    $receiptNumber = rewrite($_POST["receipt-number"]);
    $receiptNumber = stripAllWhiteSpaces($receiptNumber);
    if (strlen($receiptNumber) > 100) {
        $receiptNumberError = "Length of receipt number can't be more than 100 characters";
        $complete = false;
    }

    $receiptRemark = rewrite($_POST["receipt-remark"]);
    $receiptRemark = stripSpaces($receiptRemark);
    if (strlen($receiptRemark) > 300) {
        $receiptRemarkError = "Length of remark can't be more than 300 characters";
        $complete = false;
    }

    $memberId = rewrite($_POST["member-id"]);
    $memberId = stripAllWhiteSpaces($memberId);

    $sql = "SELECT  full_name FROM member WHERE id = ?";

    if ($stmt = $conn->prepare($sql)) {

        $stmt->bind_param("i", $memberId);

        $stmt->execute();

        $stmt->bind_result($fullName);

        $stmt->fetch();

        $stmt->close();
    }

    if ($fullName == null) {
        $memberIdError = "No such member found!";
        $complete = false;
    }

    if ($complete) {

        /*****************GET MERCHANT ID FROM CASHIER********************/
        $sql = "SELECT  merchant_id FROM cashier WHERE username = ?";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("s", $_SESSION['cashier-id']);

            $stmt->execute();

            $stmt->bind_result($merchantId);

            $stmt->fetch();

            $stmt->close();

        }

        /*****************GET CASHBACK RATE FROM MERCHANT BASED ON THE ID GET PREVIOUSLY********************/
        $sql = "SELECT rate FROM `cashback_rate` WHERE merchant_id = ? ORDER BY id DESC LIMIT 1";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("i", $merchantId);

            $stmt->execute();

            $stmt->bind_result($cashbackRate);

            $stmt->fetch();

            $stmt->close();

        }

        /*****************CALCULATING CASHBACK AMOUNTS************************/
        $totalAmount = $amountSpent * ($cashbackRate / 100);

        $personalAmount = $totalAmount * (90 / 100);

        $charityAmount = $totalAmount * (10 / 100);

        $previousCashbackAmounts = getMemberCashbackAmount($memberId, $conn);
        $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $personalAmount;
        $currentCharityAmount = $previousCashbackAmounts['current-charity-amount'] + $charityAmount;
        $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $personalAmount;
        $totalCharityAmount = $previousCashbackAmounts['total-charity-amount'] + $charityAmount;

        /**************************INSERT ALL CASHBACK AMOUNTS INTO MEMBER TABLE 1ST******************************/
        $sql = 'UPDATE member SET current_cashback_amount = ?, current_charity_amount = ?, total_cashback_amount = ?, total_charity_amount = ? WHERE id = ?';

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("ddddi", $currentCashbackAmount, $currentCharityAmount, $totalCashbackAmount, $totalCharityAmount, $memberId);

            $stmt->execute();

            $stmt->close();
        }

        /**************************THEN INSERT DATA INTO RECEIPT TABLE******************************/
        $sql = "INSERT INTO receipt (receipt_number, receipt_remark, amount_spent, cashback_amount, member_id, merchant_id, cashier_id) VALUES (?, ?, ?, ?, ?, ?, ?)";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("ssddiis", $receiptNumber, $receiptRemark, $amountSpent, $totalAmount, $memberId, $merchantId, $_SESSION['cashier-id']);

            $stmt->execute();

            $receiptId = $stmt->insert_id;

            $stmt->close();
        }

        /**************************NOW INSERT DATA INTO CASHBACK TABLE******************************/
        //PERSONAL CASHBACK
        $sql = "INSERT INTO cashback (type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?)";
        $type = "normal cashback";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("sdii", $type, $personalAmount, $memberId, $receiptId);

            $stmt->execute();

            $stmt->close();
        }

        //CHARITY CASHBACK
        $sql = "INSERT INTO cashback (type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?)";
        $type = "charity cashback";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("sdii", $type, $charityAmount, $memberId, $receiptId);

            $stmt->execute();

            $stmt->close();
        }

        //IST MANAGEMENT FEE CASHBACK

        $managementFees = $totalAmount * (12 / 100);

        $previousCashbackAmounts = getAdminCashbackAmount("IST", $conn);
        $istId = $previousCashbackAmounts['id'];
        $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $managementFees;
        $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $managementFees;

        //insert ist management fee into cashback
        $sql = "INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ?, ?)";
        $type = "management fee";

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("isdii", $memberId, $type, $managementFees, $istId, $receiptId);

            $stmt->execute();

            $stmt->close();
        }

        //insert ist's updated cashback amount into member
        $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE category = "IST"';

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("dd", $currentCashbackAmount, $totalCashbackAmount);

            $stmt->execute();

            $stmt->close();
        }

        /**************************HERE AWARDS THE 1% TO CONNECT*************************************************************************/
        $sql = 'SELECT connect_id FROM merchant WHERE id = ?';

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("i", $merchantId);

            $stmt->execute();

            $stmt->bind_result($connectId);

            $stmt->fetch();

            $stmt->close();

        }

        $connectAmount = $totalAmount * (1 / 100);
        $type = "connect";

        //NOW set up sql 1st and calculate the cashback/earning amount 1st
        if ($connectId == NULL) {
            //go to ist
            $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE category = "IST"';
            $previousCashbackAmounts = getAdminCashbackAmount("IST", $conn);
            $istId = $previousCashbackAmounts['id'];
            $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $connectAmount;
            $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $connectAmount;
            $sqlCashback = 'INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ' . $istId . ', ?)';
        } else {
            //go to member
            $sql = 'UPDATE member SET current_cashback_amount = ?, total_cashback_amount = ? WHERE id = ' . $connectId;
            $previousCashbackAmounts = getMemberCashbackAmount($connectId, $conn);
            $currentCashbackAmount = $previousCashbackAmounts['current-cashback-amount'] + $connectAmount;
            $totalCashbackAmount = $previousCashbackAmounts['total-cashback-amount'] + $connectAmount;
            $sqlCashback = 'INSERT INTO cashback (downline_referral_code, type, amount, member_id, receipt_id) VALUES (?, ?, ?, ' . $connectId . ', ?)';
        }

        //Now update into member table

        if ($stmt = $conn->prepare($sql)) {

            $stmt->bind_param("dd", $currentCashbackAmount, $totalCashbackAmount);

            $stmt->execute();

            $stmt->close();
        }

        //Now insert into cashback table
        if ($stmt = $conn->prepare($sqlCashback)) {

            $stmt->bind_param("isdi", $memberId, $type, $connectAmount, $receiptId);

            $stmt->execute();

            $stmt->close();
        }

        /**************************HERE CALLS THE RECURSIVE FUNCTION TO RECORD ALL REFERRAL AND MM CASHBACK DATA******************************/
        $noofTimes = 1;
        $result = updateReferralCashback($memberId, $memberId, $noofTimes, $totalAmount, $receiptId, $conn);
        $conn->close();

        //refresh the page and shows the message
        if (strcmp($result, "fail") != 0) {
            header("Location: enterReceipt.php?msg=success");
            exit();
        } else {
            header("Location: enterReceipt.php?msg=fail");
            exit();
        }
    }

}


ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link href="images/favicon.png" rel="icon" type="image/png"/>
    <meta name="description" content="ISpendTribute">
    <meta name="author" content="Spending tribute">

    <title>Enter Receipt</title>

    <!-- Bootstrap core CSS -->
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="main-style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>
    <script src="assets/js/navigation.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php include 'nav.php' ?>
<body class="body-color">
<div class="minpink">
    <div class="container cancelpaddingtop">
        <h1 class="paddingtop25 p15" style="color:white;">Enter Receipt Details</h1>
        <div class="row">
            <form action="" class="form-horizontal" method="post" enctype="multipart/form-data" id="receiptForm">
                <div class="col-md-9 nop">
                    <div class="form-group">
                        <div class="col-md-3">
                            <input type="number" step="0.01" min="0"
                                   oninvalid="setCustomValidity('Please do not enter any amount less than 0');"
                                   class="form-control form-controls text-line line1" oninput="setCustomValidity('');"
                                   id="amount-spent" name="amount-spent" placeholder="Amount Spent"
                                   value="<?php echo $_POST["amount-spent"]; ?>" required>
                            <span class="error"><?php echo $amountSpentError; ?></span>
                        </div>
                    </div>
                </div>

                <div class="form-group c1">
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-controls text-line" id="receipt-number"
                               name="receipt-number" placeholder="Enter the receipt's number"
                               value="<?php echo $_POST["receipt-number"]; ?>">
                        <span class="error"><?php echo $receiptNumberError; ?></span>
                    </div>
                </div>

                <div class="form-group c1">
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-controls text-line" id="receipt-remark"
                               name="receipt-remark" placeholder="Enter the receipt's remark"
                               value="<?php echo $_POST["receipt-remark"]; ?>">
                        <span class="error"><?php echo $receiptRemarkError; ?></span>
                    </div>
                </div>

                <div class="form-group c1">
                    <div class="col-sm-4">
                        <input type="text" class="form-control form-controls text-line" id="member-id" name="member-id"
                               placeholder="Enter member's ID/Inspire Code"
                               value="<?php echo $_POST["member-id"]; ?>" required>
                        <span class="error"><?php echo $memberIdError; ?></span>
                    </div>
                </div>

                <button type="submit" class="btn btn-default btn-min-register">Confirm</button>
                <input id="get-info-button" name="get-info-button" type="button"
                       class="btn btn-default btn-min-register m10a" onclick="getMemberDetails();" value="Get Info"/>
                <div><span class="error"><?php echo $_GET["msg"]; ?></span></div>
                <div id="full-name"></div>
                <div id="email"></div>
                <div id="phone-number"></div>
                <div id="msg"></div>
            </form>
        </div>
    </div><!-- /.container -->
    <div class="block"></div>
</div>
<?php include 'foot.php' ?>

<!--The progress bar section-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!--<script src="https://code.jquery.com/jquery-2.1.1.min.js" type="text/javascript"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

<script type="text/javascript">
    function getMemberDetails() {
        var memberId = $('#member-id').val();
        $.ajax({
            type: 'post',
            url: "listMember.php",
            data: {memberId: memberId},
            cache: false,
            success: function (data) {
                $("#full-name").html(data["full-name"]);
                $("#email").html(data["email"]);
                $("#phone-number").html(data["phone-number"]);
                $("#msg").html(data["msg"]);
            }
        });
    }
</script>

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
